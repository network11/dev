symmetric

.option	noinit

.param	len=.1
+	r11=5
+	l11=2	l12=1
+	c11=.5	c12=.7

.subckt	tl3	p1 p2 p3 n1 n2 n3	len=len
.model	tl3c	ltra	len=len	r=r11	l='l11+l12*2'	c='c11'
.model	tl3d	ltra	len=len	r=r11	l='l11-l12'	c='c11+c12*3'
o1	q1 0 m1 0	tl3c
o2	q2 0 m2 0	tl3d
o3	q3 0 m3 0	tl3d
xp	p1 p2 p3 q1 q2 q3	tl3f
xn	n1 n2 n3 m1 m2 m3	tl3f
.ends
.subckt	tl3f	p1 p2 p3 q1 q2 q3
bp1	0 p1	i='i(bq1)+i(bq2)+i(bq3)'
bp2	0 p2	i='i(bq1)-i(bq2)'
bp3	0 p3	i='i(bq1)-i(bq3)'
bq1	q1 0	v='(v(p1)+v(p2)+v(p3))/3'
bq2	q2 0	v='(v(p1)+v(p2)+v(p3))/3-v(p2)'
bq3	q3 0	v='(v(p1)+v(p2)+v(p3))/3-v(p3)'
.ends

.subckt	tl3s1	p1 p2 p3 n1 n2 n3	len=len
r11	n1 1	'len*r11'
r22	n2 2	'len*r11'
r33	n3 3	'len*r11'
l11	p1 1	'len*l11'
l22	p2 2	'len*l11'
l33	p3 3	'len*l11'
k12	l11 l22	'l12/l11'
k13	l11 l33	'l12/l11'
k23	l22 l33	'l12/l11'
cp11	p1 0	'len*c11/2'
cp22	p2 0	'len*c11/2'
cp33	p3 0	'len*c11/2'
cp12	p1 p2	'len*c12/2'
cp13	p1 p3	'len*c12/2'
cp23	p2 p3	'len*c12/2'
cn11	n1 0	'len*c11/2'
cn22	n2 0	'len*c11/2'
cn33	n3 0	'len*c11/2'
cn12	n1 n2	'len*c12/2'
cn13	n1 n3	'len*c12/2'
cn23	n2 n3	'len*c12/2'
.ends

.subckt	tl3s10	p1 p2 p3 n1 n2 n3	len=len
x0	p1 p2 p3 11 12 13	tl3s1	len='len/10'
x1	11 12 13 21 22 23	tl3s1	len='len/10'
x2	21 22 23 31 32 33	tl3s1	len='len/10'
x3	31 32 33 41 42 43	tl3s1	len='len/10'
x4	41 42 43 51 52 53	tl3s1	len='len/10'
x5	51 52 53 61 62 63	tl3s1	len='len/10'
x6	61 62 63 71 72 73	tl3s1	len='len/10'
x7	71 72 73 81 82 83	tl3s1	len='len/10'
x8	81 82 83 91 92 93	tl3s1	len='len/10'
x9	91 92 93 n1 n2 n3	tl3s1	len='len/10'
.ends

.subckt	ts3	p1 p2 p3 n1 n2 n3
rp1	p1 p	1
rp2	p2 p	2
rp3	p3 p	3
rn1	n1 0	3
rn2	n2 0	2
rn3	n3 0	1
vp	p 0	ac=1	am(0 1 1 1 5)
.ends

xl3a	p1a p2a p3a n1a n2a n3a	tl3
xs3a	p1a p2a p3a n1a n2a n3a	ts3
xl3b	p1b p2b p3b n1b n2b n3b	tl3s10
xs3b	p1b p2b p3b n1b n2b n3b	ts3

.control
tran	1m 1
plot	p1a p1b n1a n1b
plot	p2a p2b n2a n2b
plot	p3a p3b n3a n3b
ac	dec 100 .1 10
plot	vr(p1a) vr(p1b) vr(n1a) vr(n1b)	vi(p1a) vi(p1b) vi(n1a) vi(n1b)
plot	vr(p2a) vr(p2b) vr(n2a) vr(n2b)	vi(p2a) vi(p2b) vi(n2a) vi(n2b)
plot	vr(p3a) vr(p3b) vr(n3a) vr(n3b)	vi(p3a) vi(p3b) vi(n3a) vi(n3b)
.endc

.end
