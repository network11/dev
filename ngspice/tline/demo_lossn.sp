lossless transmission lines

.option	noinit

.param	td=5 z0=2 mp=1 mn=1

t1	p 0 n 0	z0=z0	td=td
vp	vp p	0
vn	n vn	0
hp	ip 0 vp	1
hn	in 0 vn	1
rp	vp 0	r=z0	m=mp
rn	vn 0	r=z0	m=mn
ip	0 vp	pulse(0 '1+mp' 1m 1 1 1m)

.control
foreach	mn	1 1m 1k
foreach	mp	1 1m 1k
	alterparam	mp=$mp
	alterparam	mn=$mn
	reset
	tran	40m 40
	plot	vp ip vn in	ylimit -4 4
end
end
.endc

.end
