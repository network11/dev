bare

.option	noinit

.param	td=.1
+	z11=2	z22=3	z12=1
+	y11=.4	y22=.2	y12=.2

.subckt	tl2	p1 p2 n1 n2	td=td
t11	p1 0  n1 0	td=td z0='1/y11'
t22	p2 0  n2 0	td=td z0='1/y22'
t12	p1 p2 n1 n2	td=td z0='1/y12'
.ends

.subckt	tl2s1	p1 p2 n1 n2	td=td
l11	p1 n1	'td*z11'
l22	p2 n2	'td*z22'
k12	l11 l22	'z12/sqrt(z11*z22)'
cp11	p1 0	'td*y11/2'
cp22	p2 0	'td*y22/2'
cp12	p1 p2	'td*y12/2'
cn11	n1 0	'td*y11/2'
cn22	n2 0	'td*y22/2'
cn12	n1 n2	'td*y12/2'
.ends

.subckt	tl2s10	p1 p2 n1 n2	td=td
x0	p1 p2 11 12	tl2s1	td='td/10'
x1	11 12 21 22	tl2s1	td='td/10'
x2	21 22 31 32	tl2s1	td='td/10'
x3	31 32 41 42	tl2s1	td='td/10'
x4	41 42 51 52	tl2s1	td='td/10'
x5	51 52 61 62	tl2s1	td='td/10'
x6	61 62 71 72	tl2s1	td='td/10'
x7	71 72 81 82	tl2s1	td='td/10'
x8	81 82 91 92	tl2s1	td='td/10'
x9	91 92 n1 n2	tl2s1	td='td/10'
.ends

.subckt	ts2	p1 p2 n1 n2
rp1	p1 p	1
rp2	p2 p	1
rn1	n1 0	1
rn2	n2 0	1
vp	p 0	ac=1	am(0 1 1 1 5)
.ends

xl2a	p1a p2a n1a n2a	tl2
xs2a	p1a p2a n1a n2a	ts2
xl2b	p1b p2b n1b n2b	tl2s10
xs2b	p1b p2b n1b n2b	ts2

.control
tran	1m 1
plot	p1a p1b n1a n1b
plot	p2a p2b n2a n2b
ac	dec 100 .1 10
plot	vr(p1a) vr(p1b) vr(n1a) vr(n1b)	vi(p1a) vi(p1b) vi(n1a) vi(n1b)
plot	vr(p2a) vr(p2b) vr(n2a) vr(n2b)	vi(p2a) vi(p2b) vi(n2a) vi(n2b)
.endc

.end
