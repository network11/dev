bare

.option	noinit

.param	td=.1
+	z11=4/7	z22=5/7	z33=6/7	z12=1/7	z13=2/7	z23=3/7
+	y11=1.4	y22=1.0	y33=0.2	y12=1u	y13=0.7	y23=1.0

.subckt	tl3	p1 p2 p3 n1 n2 n3	td=td
t11	p1 0  n1 0	td=td z0='1/y11'
t22	p2 0  n2 0	td=td z0='1/y22'
t33	p3 0  n3 0	td=td z0='1/y33'
t12	p1 p2 n1 n2	td=td z0='1/y12'
t13	p1 p3 n1 n3	td=td z0='1/y13'
t23	p2 p3 n2 n3	td=td z0='1/y23'
.ends

.subckt	tl3s1	p1 p2 p3 n1 n2 n3	td=td
l11	p1 n1	'td*z11'
l22	p2 n2	'td*z22'
l33	p3 n3	'td*z33'
k12	l11 l22	'z12/sqrt(z11*z22)'
k13	l11 l33	'z13/sqrt(z11*z33)'
k23	l22 l33	'z23/sqrt(z22*z33)'
cp11	p1 0	'td*y11/2'
cp22	p2 0	'td*y22/2'
cp33	p3 0	'td*y33/2'
cp12	p1 p2	'td*y12/2'
cp13	p1 p3	'td*y13/2'
cp23	p2 p3	'td*y23/2'
cn11	n1 0	'td*y11/2'
cn22	n2 0	'td*y22/2'
cn33	n3 0	'td*y33/2'
cn12	n1 n2	'td*y12/2'
cn13	n1 n3	'td*y13/2'
cn23	n2 n3	'td*y23/2'
.ends

.subckt	tl3s10	p1 p2 p3 n1 n2 n3	td=td
x0	p1 p2 p3 11 12 13	tl3s1	td='td/10'
x1	11 12 13 21 22 23	tl3s1	td='td/10'
x2	21 22 23 31 32 33	tl3s1	td='td/10'
x3	31 32 33 41 42 43	tl3s1	td='td/10'
x4	41 42 43 51 52 53	tl3s1	td='td/10'
x5	51 52 53 61 62 63	tl3s1	td='td/10'
x6	61 62 63 71 72 73	tl3s1	td='td/10'
x7	71 72 73 81 82 83	tl3s1	td='td/10'
x8	81 82 83 91 92 93	tl3s1	td='td/10'
x9	91 92 93 n1 n2 n3	tl3s1	td='td/10'
.ends

.subckt	ts3	p1 p2 p3 n1 n2 n3
rp1	p1 p	1
rp2	p2 p	1
rp3	p3 p	1
rn1	n1 0	1
rn2	n2 0	1
rn3	n3 0	1
vp	p 0	ac=1	am(0 1 1 1 5)
.ends

xl3a	p1a p2a p3a n1a n2a n3a	tl3
xs3a	p1a p2a p3a n1a n2a n3a	ts3
xl3b	p1b p2b p3b n1b n2b n3b	tl3s10
xs3b	p1b p2b p3b n1b n2b n3b	ts3

.control
tran	1m 1
plot	p1a p1b n1a n1b
plot	p2a p2b n2a n2b
plot	p3a p3b n3a n3b
ac	dec 100 .1 10
plot	vr(p1a) vr(p1b) vr(n1a) vr(n1b)	vi(p1a) vi(p1b) vi(n1a) vi(n1b)
plot	vr(p2a) vr(p2b) vr(n2a) vr(n2b)	vi(p2a) vi(p2b) vi(n2a) vi(n2b)
plot	vr(p3a) vr(p3b) vr(n3a) vr(n3b)	vi(p3a) vi(p3b) vi(n3a) vi(n3b)
.endc

.end
