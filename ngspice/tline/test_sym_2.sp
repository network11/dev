symmetric

.option	noinit

.param	len=.1
+	r11=5
+	l11=2	l12=1
+	c11=.5	c12=.7

.subckt	tl2	p1 p2 n1 n2	len=len
.model	tl2c	ltra	len=len	r=r11	l='l11+l12'	c='c11'
.model	tl2d	ltra	len=len	r=r11	l='l11-l12'	c='c11+c12*2'
o1	q1 0 m1 0	tl2c
o2	q2 0 m2 0	tl2d
xp	p1 p2 q1 q2	tl2f
xn	n1 n2 m1 m2	tl2f
.ends
.subckt	tl2f	p1 p2 q1 q2
bp1	0 p1	i='i(bq1)+i(bq2)'
bp2	0 p2	i='i(bq1)-i(bq2)'
bq1	q1 0	v='(v(p1)+v(p2))/2'
bq2	q2 0	v='(v(p1)-v(p2))/2'
.ends

.subckt	tl2s1	p1 p2 n1 n2	len=len
r11	n1 1	'len*r11'
r22	n2 2	'len*r11'
l11	p1 1	'len*l11'
l22	p2 2	'len*l11'
k12	l11 l22	'l12/l11'
cp11	p1 0	'len*c11/2'
cp22	p2 0	'len*c11/2'
cp12	p1 p2	'len*c12/2'
cn11	n1 0	'len*c11/2'
cn22	n2 0	'len*c11/2'
cn12	n1 n2	'len*c12/2'
.ends

.subckt	tl2s10	p1 p2 n1 n2	len=len
x0	p1 p2 11 12	tl2s1	len='len/10'
x1	11 12 21 22	tl2s1	len='len/10'
x2	21 22 31 32	tl2s1	len='len/10'
x3	31 32 41 42	tl2s1	len='len/10'
x4	41 42 51 52	tl2s1	len='len/10'
x5	51 52 61 62	tl2s1	len='len/10'
x6	61 62 71 72	tl2s1	len='len/10'
x7	71 72 81 82	tl2s1	len='len/10'
x8	81 82 91 92	tl2s1	len='len/10'
x9	91 92 n1 n2	tl2s1	len='len/10'
.ends

.subckt	ts2	p1 p2 n1 n2
rp1	p1 p	1
rp2	p2 p	2
rn1	n1 0	2
rn2	n2 0	1
vp	p 0	ac=1	am(0 1 1 1 5)
.ends

xl2a	p1a p2a n1a n2a	tl2
xs2a	p1a p2a n1a n2a	ts2
xl2b	p1b p2b n1b n2b	tl2s10
xs2b	p1b p2b n1b n2b	ts2

.control
tran	1m 1
plot	p1a p1b n1a n1b
plot	p2a p2b n2a n2b
ac	dec 100 .1 10
plot	vr(p1a) vr(p1b) vr(n1a) vr(n1b)	vi(p1a) vi(p1b) vi(n1a) vi(n1b)
plot	vr(p2a) vr(p2b) vr(n2a) vr(n2b)	vi(p2a) vi(p2b) vi(n2a) vi(n2b)
.endc

.end
