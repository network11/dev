symmetric

.option	noinit

.param	len=.1
+	r11=5
+	l11=2
+	c11=.5

.subckt	tl1	p1 n1	len=len
.model	tl1c	ltra	len=len	r=r11	l=l11	c=c11
o1	p1 0 n1 0	tl1c
.ends

.subckt	tl1s1	p1 n1	len=len
r11	n1 1	'len*r11'
l11	p1 1	'len*l11'
cp11	p1 0	'len*c11/2'
cn11	n1 0	'len*c11/2'
.ends

.subckt	tl1s10	p1 n1	len=len
x0	p1 11	tl1s1	len='len/10'
x1	11 21	tl1s1	len='len/10'
x2	21 31	tl1s1	len='len/10'
x3	31 41	tl1s1	len='len/10'
x4	41 51	tl1s1	len='len/10'
x5	51 61	tl1s1	len='len/10'
x6	61 71	tl1s1	len='len/10'
x7	71 81	tl1s1	len='len/10'
x8	81 91	tl1s1	len='len/10'
x9	91 n1	tl1s1	len='len/10'
.ends

.subckt	ts1	p1 n1
rp1	p1 p	1
rn1	n1 0	1
vp	p 0	ac=1	am(0 1 1 1 5)
.ends

xl1a	p1a n1a	tl1
xs1a	p1a n1a	ts1
xl1b	p1b n1b	tl1s10
xs1b	p1b n1b	ts1

.control
tran	1m 1
plot	p1a p1b n1a n1b
ac	dec 100 .1 10
plot	vr(p1a) vr(p1b) vr(n1a) vr(n1b)	vi(p1a) vi(p1b) vi(n1a) vi(n1b)
.endc

.end
