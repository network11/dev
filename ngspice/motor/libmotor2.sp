* 2-phase motor model

* nonlin motor
.subckt	mb2	v1 v2 v4 u9 x9	ic=0 l=1 i=1 n=1
x1	v1 0 q1 0	eddt
x2	v2 0 q2 0	eddt
x4	v4 0 q4 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x2),i(x4),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),i(x4),v(x9))'
b4	q4 0	v='q4(i(x1),i(x2),i(x4),v(x9))'
b9	0 u9	i='f9(i(x1),i(x2),i(x4),v(x9))'
.func	q1(i1,i2,i4,x9)='b(i1,i2,i4,x9)*(c(0,x9)*i4+i1)'
.func	q2(i1,i2,i4,x9)='b(i1,i2,i4,x9)*(c(1,x9)*i4+i2)'
.func	q4(i1,i2,i4,x9)='b(i1,i2,i4,x9)*(c(0,x9)*i1+c(1,x9)*i2+i4)'
.func	f9(i1,i2,i4,x9)='b(i1,i2,i4,x9)*(d(0,x9)*i1*i4+d(1,x9)*i2*i4)'
.func	a(i1,i2,i4,x9)='sqrt(c(0,x9)*i1*i4*2+c(1,x9)*i2*i4*2+i4^2+i1^2+i2^2)'
.func	b(i1,i2,i4,x9)='l*i*tanh(a(i1,i2,i4,x9)/i)/a(i1,i2,i4,x9)'
.func	c(m,x)='cos(n*x-pi/2*m)'
.func	d(m,x)='sin(n*x-pi/2*m)*-n'
.ends

* induct motor
.subckt	mk2	v1 v2 v4 u9 x9	ic=0 k=1 n=1
x1	v1 0 q1 0	eddt
x2	v2 0 q2 0	eddt
x4	v4 0 q4 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x2),i(x4),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),i(x4),v(x9))'
b4	q4 0	v='q4(i(x1),i(x2),i(x4),v(x9))'
b9	0 u9	i='f9(i(x1),i(x2),i(x4),v(x9))'
.func	q1(i1,i2,i4,x9)='c(0,x9)*i4'
.func	q2(i1,i2,i4,x9)='c(1,x9)*i4'
.func	q4(i1,i2,i4,x9)='c(0,x9)*i1+c(1,x9)*i2'
.func	f9(i1,i2,i4,x9)='d(0,x9)*i1*i4+d(1,x9)*i2*i4'
.func	c(m,x)='k*cos(n*x-pi/2*m)'
.func	d(m,x)='k*sin(n*x-pi/2*m)*-n'
.ends

* reluct motor
.subckt	ml2	v1 v2 u9 x9	ic=0 l=1 n=2
x1	v1 0 q1 0	eddt
x2	v2 0 q2 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x2),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),v(x9))'
b9	0 u9	i='f9(i(x1),i(x2),v(x9))'
.func	q1(i1,i2,x9)='c(0,x9)*i1+c(1,x9)*i2'
.func	q2(i1,i2,x9)='c(1,x9)*i1+c(2,x9)*i2'
.func	f9(i1,i2,x9)='d(0,x9)*i1^2/2+d(2,x9)*i2^2/2+d(1,x9)*i1*i2'
.func	c(m,x)='l*cos(n*x-pi/2*m)'
.func	d(m,x)='l*sin(n*x-pi/2*m)*-n'
.ends

* magnet motor
.subckt	mq2	v1 v2 u9 x9	ic=0 q=1 n=1
x1	v1 0 q1 0	eddt
x2	v2 0 q2 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x2),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),v(x9))'
b9	0 u9	i='f9(i(x1),i(x2),v(x9))'
.func	q1(i1,i2,x9)='c(0,x9)'
.func	q2(i1,i2,x9)='c(1,x9)'
.func	f9(i1,i2,x9)='d(0,x9)*i1+d(1,x9)*i2'
.func	c(m,x)='q*cos(n*x-pi/2*m)'
.func	d(m,x)='q*sin(n*x-pi/2*m)*-n'
.ends

.subckt	nh2	v1 v2 v9 x9	h=1 n=1
b1	v1 0	v='v1(i(b1),i(b2),v(v9),v(x9))'
b2	v2 0	v='v2(i(b1),i(b2),v(v9),v(x9))'
b9	0 v9	i='i9(i(b1),i(b2),v(v9),v(x9))'
.func	v1(i1,i2,v9,x9)='c(0,x9)*v9'
.func	v2(i1,i2,v9,x9)='c(1,x9)*v9'
.func	i9(i1,i2,v9,x9)='c(0,x9)*i1+c(1,x9)*i2'
.func	c(m,x)='h*cos(n*x-pi/2*m)'
.ends

.subckt	ng2	v1 v2 v9 x9	g=1 n=1
b1	0 v1	i='i1(v(v1),v(v2),i(b9),v(x9))'
b2	0 v2	i='i2(v(v1),v(v2),i(b9),v(x9))'
b9	v9 0	v='v9(v(v1),v(v2),i(b9),v(x9))'
.func	i1(v1,v2,i9,x9)='c(0,x9)*i9'
.func	i2(v1,v2,i9,x9)='c(1,x9)*i9'
.func	v9(v1,v2,i9,x9)='c(0,x9)*v1+c(1,x9)*v2'
.func	c(m,x)='g*cos(n*x-pi/2*m)'
.ends

