* 3-phase motor model

* nonlin motor
.subckt	mb3	v1 v2 v3 vn v4 u9 x9	ic=0 l=1 i=1 n=1
x1	v1 vn q1 0	eddt
x2	v2 vn q2 0	eddt
x3	v3 vn q3 0	eddt
x4	v4 0 q4 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x2),i(x3),i(x4),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),i(x3),i(x4),v(x9))'
b3	q3 0	v='q3(i(x1),i(x2),i(x3),i(x4),v(x9))'
b4	q4 0	v='q4(i(x1),i(x2),i(x3),i(x4),v(x9))'
b9	0 u9	i='f9(i(x1),i(x2),i(x3),i(x4),v(x9))'
.func	q1(i1,i2,i3,i4,x9)='b(i1,i2,i3,i4,x9)*(c(0,x9)*i4+i1-i2/2-i3/2)'
.func	q2(i1,i2,i3,i4,x9)='b(i1,i2,i3,i4,x9)*(c(1,x9)*i4+i2-i1/2-i3/2)'
.func	q3(i1,i2,i3,i4,x9)='b(i1,i2,i3,i4,x9)*(c(2,x9)*i4+i3-i1/2-i2/2)'
.func	q4(i1,i2,i3,i4,x9)='b(i1,i2,i3,i4,x9)*(c(0,x9)*i1+c(1,x9)*i2+c(2,x9)*i3+i4)'
.func	f9(i1,i2,i3,i4,x9)='b(i1,i2,i3,i4,x9)*(d(0,x9)*i1*i4+d(1,x9)*i2*i4+d(2,x9)*i3*i4)'
.func	a(i1,i2,i3,i4,x9)='sqrt(c(0,x9)*i1*i4*2+c(1,x9)*i2*i4*2+c(2,x9)*i3*i4*2+i4^2+i1^2+i2^2+i3^2-i1*i2-i1*i3-i2*i3)'
.func	b(i1,i2,i3,i4,x9)='l*i*tanh(a(i1,i2,i3,i4,x9)/i)/a(i1,i2,i3,i4,x9)'
.func	c(m,x)='cos(n*x-pi*2/3*m)'
.func	d(m,x)='sin(n*x-pi*2/3*m)*-n'
.ends

* induct motor
.subckt	mk3	v1 v2 v3 vn v4 u9 x9	ic=0 k=1 n=1
x1	v1 vn q1 0	eddt
x2	v2 vn q2 0	eddt
x3	v3 vn q3 0	eddt
x4	v4 0 q4 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x2),i(x3),i(x4),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),i(x3),i(x4),v(x9))'
b3	q3 0	v='q3(i(x1),i(x2),i(x3),i(x4),v(x9))'
b4	q4 0	v='q4(i(x1),i(x2),i(x3),i(x4),v(x9))'
b9	0 u9	i='f9(i(x1),i(x2),i(x3),i(x4),v(x9))'
.func	q1(i1,i2,i3,i4,x9)='c(0,x9)*i4'
.func	q2(i1,i2,i3,i4,x9)='c(1,x9)*i4'
.func	q3(i1,i2,i3,i4,x9)='c(2,x9)*i4'
.func	q4(i1,i2,i3,i4,x9)='c(0,x9)*i1+c(1,x9)*i2+c(2,x9)*i3'
.func	f9(i1,i2,i3,i4,x9)='d(0,x9)*i1*i4+d(1,x9)*i2*i4+d(2,x9)*i3*i4'
.func	c(m,x)='k*cos(n*x-pi*2/3*m)'
.func	d(m,x)='k*sin(n*x-pi*2/3*m)*-n'
.ends

* reluct motor
.subckt	ml3	v1 v2 v3 vn u9 x9	ic=0 l=1 n=2
x1	v1 vn q1 0	eddt
x2	v2 vn q2 0	eddt
x3	v3 vn q3 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x2),i(x3),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),i(x3),v(x9))'
b3	q3 0	v='q3(i(x1),i(x2),i(x3),v(x9))'
b9	0 u9	i='f9(i(x1),i(x2),i(x3),v(x9))'
.func	q1(i1,i2,i3,x9)='c(0,x9)*i1+c(1,x9)*i2+c(2,x9)*i3'
.func	q2(i1,i2,i3,x9)='c(1,x9)*i1+c(2,x9)*i2+c(3,x9)*i3'
.func	q3(i1,i2,i3,x9)='c(2,x9)*i1+c(3,x9)*i2+c(4,x9)*i3'
.func	f9(i1,i2,i3,x9)='d(0,x9)*i1^2/2+d(2,x9)*i2^2/2+d(4,x9)*i3^2/2+d(1,x9)*i1*i2+d(2,x9)*i1*i3+d(3,x9)*i2*i3'
.func	c(m,x)='l*cos(n*x-pi*2/3*m)'
.func	d(m,x)='l*sin(n*x-pi*2/3*m)*-n'
.ends

* magnet motor
.subckt	mq3	v1 v2 v3 vn u9 x9	ic=0 q=1 n=1
x1	v1 vn q1 0	eddt
x2	v2 vn q2 0	eddt
x3	v3 vn q3 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x2),i(x3),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),i(x3),v(x9))'
b3	q3 0	v='q3(i(x1),i(x2),i(x3),v(x9))'
b9	0 u9	i='f9(i(x1),i(x2),i(x3),v(x9))'
.func	q1(i1,i2,i3,x9)='c(0,x9)'
.func	q2(i1,i2,i3,x9)='c(1,x9)'
.func	q3(i1,i2,i3,x9)='c(2,x9)'
.func	f9(i1,i2,i3,x9)='d(0,x9)*i1+d(1,x9)*i2+d(2,x9)*i3'
.func	c(m,x)='q*cos(n*x-pi*2/3*m)'
.func	d(m,x)='q*sin(n*x-pi*2/3*m)*-n'
.ends

.subckt	nh3	v1 v2 v3 vn v9 x9	h=1 n=1
b1	v1 vn	v='v1(i(b1),i(b2),i(b3),v(v9),v(x9))'
b2	v2 vn	v='v2(i(b1),i(b2),i(b3),v(v9),v(x9))'
b3	v3 vn	v='v3(i(b1),i(b2),i(b3),v(v9),v(x9))'
b9	0 v9	i='i9(i(b1),i(b2),i(b3),v(v9),v(x9))'
.func	v1(i1,i2,i3,v9,x9)='c(0,x9)*v9'
.func	v2(i1,i2,i3,v9,x9)='c(1,x9)*v9'
.func	v3(i1,i2,i3,v9,x9)='c(2,x9)*v9'
.func	i9(i1,i2,i3,v9,x9)='c(0,x9)*i1+c(1,x9)*i2+c(2,x9)*i3'
.func	c(m,x)='h*cos(n*x-pi*2/3*m)'
.ends

.subckt	ng3	v1 v2 v3 v9 x9	g=1 n=1
b1	0 v1	i='i1(v(v1),v(v2),v(v3),i(b9),v(x9))'
b2	0 v2	i='i2(v(v1),v(v2),v(v3),i(b9),v(x9))'
b3	0 v3	i='i3(v(v1),v(v2),v(v3),i(b9),v(x9))'
b9	v9 0	v='v9(v(v1),v(v2),v(v3),i(b9),v(x9))'
.func	i1(v1,v2,v3,i9,x9)='c(0,x9)*i9'
.func	i2(v1,v2,v3,i9,x9)='c(1,x9)*i9'
.func	i3(v1,v2,v3,i9,x9)='c(2,x9)*i9'
.func	v9(v1,v2,v3,i9,x9)='c(0,x9)*v1+c(1,x9)*v2+c(2,x9)*v3'
.func	c(m,x)='g*cos(n*x-pi*2/3*m)'
.ends

