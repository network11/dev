test eidt eddt

.option	noinit
.inc	libmotor.sp

.func	fi(x)='tanh(x) + ln(cosh(x)) + cos(x) + sin(x)'
.func	fd(x)='cosh(x)^-2 + tanh(x) + -sin(x) + cos(x)'

xi	ai 0 bd 0	eidt	ic='fi(-10)'
xd	ad 0 bi 0	eddt
bi	bi 0	v='fi(time-10)'
bd	bd 0	v='fd(time-10)'

.control
tran	10m 20
plot	ai bi ai-bi
plot	ad bd ad-bd
.endc

.end
