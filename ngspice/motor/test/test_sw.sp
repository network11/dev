test switch

.option	noinit

rd	d 0	'v(d)>0 ? 1 : 10'
rs	s 0	'v(c)>0 ? 1 : 10'
vd	d 0	1
vs	s 0	1
vc	c 0	1

.control
dc	vd -10 10 10m
plot	-i(vd)
dc	vc -10 10 10m	vs -1 2 1
plot	-i(vs)
dc	vs -10 10 10m	vc 0 1 1
plot	-i(vs)
.endc

.end
