demo ml2 step

.option	noinit
.inc	libmotor.sp
.inc	libmotor2.sp

xml	v1 v2 u9 x9	ml2
r9	u9 0	10
c9	u9 0	10m
xng	v1 v2 v7 x8	ng2
i7	0 v7	1
b8	x8 0	v='x*floor(time/2)'

.param	x=0

.control
foreach	x	-1 1 -2 2
	alterparam	x=$x
	reset
	tran	10m 20
	plot	x8 x9
end
.endc

.end
