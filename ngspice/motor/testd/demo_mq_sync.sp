demo mq2 sync

.option	noinit
.inc	libmotor.sp
.inc	libmotor2.sp

xmq	v1 v2 u9 x9	mq2
r9	u9 0	'10/(1-u)'
c9	u9 0	10m
xng	v1 v2 v7 x8	ng2
r7	v7 0	'10/u'
b7	0 v7	i='nint(sin(time/3))/20'
b8	x8 0	v='nint(v(x9))+pi/2'

.param	u=.5

.control
foreach	u	.1 .5 .9
	alterparam	u=$u
	reset
	tran	10m 20
end
plot	tran1.u9 tran2.u9 tran3.u9
.endc

.end
