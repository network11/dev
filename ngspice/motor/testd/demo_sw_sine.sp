demo switch sine

.option	noinit
.inc	libmotor.sp
.inc	libmotor3.sp

xmq	v1 v2 v3 vn u9 x9	mq3
r9	u9 0	100
c9	u9 0	100m
l1	v1 d1	100m
l2	v2 d2	100m
l3	v3 d3	100m
bc1	c1 c	v='-sin(v(x9))'
bc2	c2 c	v='-sin(v(x9)-pi*2/3)'
bc3	c3 c	v='-sin(v(x9)-pi*4/3)'
rp1	d1 d	'v(c1)>0||v(d1,d)>0 ? 1 : 1g'
rp2	d2 d	'v(c2)>0||v(d2,d)>0 ? 1 : 1g'
rp3	d3 d	'v(c3)>0||v(d3,d)>0 ? 1 : 1g'
rn1	d1 0	'v(c1)<0||v(d1,0)<0 ? 1 : 1g'
rn2	d2 0	'v(c2)<0||v(d2,0)<0 ? 1 : 1g'
rn3	d3 0	'v(c3)<0||v(d3,0)<0 ? 1 : 1g'
vd	d 0	pwl(0 0 10m 1)
vc	c 0	pwl(0 -1 25m 1 50m -1) r=0

.control
tran	2m 20
plot	u9
plot	v1-vn v2-vn v3-vn
plot	v1-v2 v2-v3 v3-v1
.endc

.end
