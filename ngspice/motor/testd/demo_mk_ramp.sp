demo mk2 ramp

.option	noinit
.inc	libmotor.sp
.inc	libmotor2.sp

xmk	v1 v2 v4 u9 x9	mk2
r4	v4 4	1
l4	4 0	.1
r9	u9 0	10
c9	u9 0	10m
xng	v1 v2 v7 x8	ng2
i7	0 v7	1
x8	x8 0 u8 0	eidt
b8	u8 0	v='15*nint(sin(time/3))'

.control
tran	10m 20
plot	u8 u9
plot	x8 x9
plot	x8-x9
.endc

.end
