demo mb2 stop

.option	noinit
.inc	libmotor.sp
.inc	libmotor2.sp

xmb	v1 v2 v4 0 x9	mb2
i4	0 v4	1
i1	0 v1	sin(.1 .1 2 0 0 -90)

.control
set	p=( )
let	x=pi/4*vector(5)
foreach	x	$&x
	alterparam	mb2	ic=$x
	reset
	tran	10m 2
	set	p=( $p {$curplot}.v1 )
end
plot	$p
.endc

.end
