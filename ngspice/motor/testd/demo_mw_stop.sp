demo mw1 stop

.option	noinit
.inc	libmotor.sp
.inc	libmotor1.sp

xmw	u9 x9	mw1	w=.2
r9	u9 0	10
c9	u9 0	10m

.control
set	p=( )
let	x=pi/3*vector(7)
foreach	x	$&x
	alterparam	mw1	ic=$x
	reset
	tran	10m 2
	set	p=( $p {$curplot}.x9 )
end
plot	$p
.endc

.end
