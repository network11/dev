* 1-phase motor model

* nonlin motor
.subckt	mb1	v1 v4 u9 x9	ic=0 l=1 i=1 n=1
x1	v1 0 q1 0	eddt
x4	v4 0 q4 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x4),v(x9))'
b4	q4 0	v='q4(i(x1),i(x4),v(x9))'
b9	0 u9	i='f9(i(x1),i(x4),v(x9))'
.func	q1(i1,i4,x9)='b(i1,i4,x9)*(c(x9)*i4+i1)'
.func	q4(i1,i4,x9)='b(i1,i4,x9)*(c(x9)*i1+i4)'
.func	f9(i1,i4,x9)='b(i1,i4,x9)*(d(x9)*i1*i4)'
.func	a(i1,i4,x9)='sqrt(c(x9)*i1*i4*2+i4^2+i1^2)'
.func	b(i1,i4,x9)='l*i*tanh(a(i1,i4,x9)/i)/a(i1,i4,x9)'
.func	c(x)='cos(n*x)'
.func	d(x)='sin(n*x)*-n'
.ends

* induct motor
.subckt	mk1	v1 v4 u9 x9	ic=0 k=1 n=1
x1	v1 0 q1 0	eddt
x4	v4 0 q4 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),i(x4),v(x9))'
b4	q4 0	v='q4(i(x1),i(x4),v(x9))'
b9	0 u9	i='f9(i(x1),i(x4),v(x9))'
.func	q1(i1,i4,x9)='c(x9)*i4'
.func	q4(i1,i4,x9)='c(x9)*i1'
.func	f9(i1,i4,x9)='d(x9)*i1*i4'
.func	c(x)='k*cos(n*x)'
.func	d(x)='k*sin(n*x)*-n'
.ends

* reluct motor
.subckt	ml1	v1 u9 x9	ic=0 l=1 n=2
x1	v1 0 q1 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),v(x9))'
b9	0 u9	i='f9(i(x1),v(x9))'
.func	q1(i1,x9)='c(x9)*i1'
.func	f9(i1,x9)='d(x9)*i1^2/2'
.func	c(x)='l*cos(n*x)'
.func	d(x)='l*sin(n*x)*-n'
.ends

* magnet motor
.subckt	mq1	v1 u9 x9	ic=0 q=1 n=1
x1	v1 0 q1 0	eddt
x9	x9 0 u9 0	eidt	ic=ic
b1	q1 0	v='q1(i(x1),v(x9))'
b9	0 u9	i='f9(i(x1),v(x9))'
.func	q1(i1,x9)='c(x9)'
.func	f9(i1,x9)='d(x9)*i1'
.func	c(x)='q*cos(n*x)'
.func	d(x)='q*sin(n*x)*-n'
.ends

.subckt	mw1	u9 x9	ic=0 w=0 n=2
x9	x9 0 u9 0	eidt	ic=ic
b9	0 u9	i='f9(v(x9))'
.func	f9(x9)='d(x9)'
.func	d(x)='w*sin(n*x)*-n'
.ends

.subckt	nh1	v1 v9 x9	h=1 n=1
b1	v1 0	v='v1(i(b1),v(v9),v(x9))'
b9	0 v9	i='i9(i(b1),v(v9),v(x9))'
.func	v1(i1,v9,x9)='c(x9)*v9'
.func	i9(i1,v9,x9)='c(x9)*i1'
.func	c(x)='h*cos(n*x)'
.ends

