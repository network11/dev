test mq3 mq2

.option	noinit
.inc	libmotor.sp
.inc	libmotor2.sp
.inc	libmotor3.sp

.subckt	tst	1 2 3 4
i1	0 1	sin(.15 .3 .59 0 0 -30)
i2	0 2	sin(.15 .3 .53 0 0 -30)
i3	0 3	sin(.15 .3 .47 0 0 -30)
i4	0 4	sin(.15 .3 .41 0 0 -30)
r1	1 0	10
r2	2 0	10
r3	3 0	10
r4	4 0	10
.ends

.subckt	una	v8 x8 u9 x9
xng	v1 v2 v3 v8 x8	ng3	g='2/3'
xmq	v1 v2 v3 vn u9 x9	mq3	ic=-10 q=.4
rn	vn 0	1g
.ends

.subckt	unb	v8 x8 u9 x9
xng	v1 v2 v8 x8	ng2
xmq	v1 v2 u9 x9	mq2	ic=-10 q=.4
.ends

xua	a1 a2 a3 a4	una
xub	b1 b2 b3 b4	unb
xta	a1 a2 a3 a4	tst
xtb	b1 b2 b3 b4	tst

.control
tran	10m 20
define	check(a,b)	vecmax(abs(a-b))/stddev(b)
foreach	n	1 2 3 4
	plot	a{$n} b{$n} a{$n}-b{$n}
	print	check(a{$n},b{$n})
end
.endc

.end
