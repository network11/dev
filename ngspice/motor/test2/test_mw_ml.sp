test mw1 ml1

.option	noinit
.inc	libmotor.sp
.inc	libmotor1.sp

.subckt	tst	1 2 3 4
i1	0 1	sin(.15 .3 .59 0 0 -30)
i2	0 2	sin(.15 .3 .53 0 0 -30)
i3	0 3	sin(.15 .3 .47 0 0 -30)
i4	0 4	sin(.15 .3 .41 0 0 -30)
r1	1 0	10
r2	2 0	10
r3	3 0	10
r4	4 0	10
.ends

.subckt	una	u9 x9
xmw	u9 x9	mw1	ic=-10 w=.1
.ends

.subckt	unb	u9 x9
xml	v1 u9 x9	ml1	ic=-10 l=.8
i1	0 v1	.5
.ends

xua	a3 a4	una
xub	b3 b4	unb
xta	a1 a2 a3 a4	tst
xtb	b1 b2 b3 b4	tst

.control
tran	10m 20
define	check(a,b)	vecmax(abs(a-b))/stddev(b)
foreach	n	3 4
	plot	a{$n} b{$n} a{$n}-b{$n}
	print	check(a{$n},b{$n})
end
.endc

.end
