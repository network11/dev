test mk2 work

.option	noinit
.inc	libmotor.sp
.inc	libmotor2.sp

.subckt	tst	1 2 3 4
i1	0 1	sin(.15 .3 .59 0 0 -30)
i2	0 2	sin(.15 .3 .53 0 0 -30)
i3	0 3	sin(.15 .3 .47 0 0 -30)
i4	0 4	sin(.15 .3 .41 0 0 -30)
r1	1 0	10
r2	2 0	10
r3	3 0	10
r4	4 0	10
.ends

.subckt	una	i1 i2 i4 i9 w
xmk	v1 v2 v4 u9 x9	mk2	ic=-10 k=2
l1	i1 v1	2
l2	i2 v2	2
l4	i4 v4	2
v9	i9 u9	0
bp	p 0	v='v(v1)*i(l1)+v(v2)*i(l2)+v(v4)*i(l4)+v(u9)*i(v9)'
xw	u w p 0	eidt
bu	u 0	v='(v(xmk.q1)*i(l1)+v(xmk.q2)*i(l2)+v(xmk.q4)*i(l4))/2'
.ends

xua	a1 a2 a3 a4 a	una
xta	a1 a2 a3 a4	tst

.control
tran	10m 20
define	check(a)	vecmax(abs(a))
print	check(a)
.endc

.end
