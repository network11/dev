* motor model

.subckt	eddt	op on ip in
g1	0 1 ip in	1
l1	1 0	1
e1	op on 1 0	1
.ends

.subckt	eidt	op on ip in	ic=0
.ic	v(1)=ic
g1	0 1 ip in	1
c1	1 0	1
e1	op on 1 0	1
.ends

.subckt	__mot	v1 n1 v2 n2 q1 q2 u8 n8 u9 n9 x8 x9
x1	v1 n1 q1 0	eddt
x2	v2 n2 q2 0	eddt
x8	x8 0 u8 n8	eidt
x9	x9 0 u9 n9	eidt
b1	q1 0	v='q1(i(x1),i(x2),v(x8),v(x9))'
b2	q2 0	v='q2(i(x1),i(x2),v(x8),v(x9))'
b8	n8 u8	i='f8(i(x1),i(x2),v(x8),v(x9))'
b9	n9 u9	i='f9(i(x1),i(x2),v(x8),v(x9))'
.func	q1(i1,i2,x8,x9)='w1(i1,i2,x8,x9)'
.func	q2(i1,i2,x8,x9)='w2(i1,i2,x8,x9)'
.func	f8(i1,i2,x8,x9)='w8(i1,i2,x8,x9)'
.func	f9(i1,i2,x8,x9)='w9(i1,i2,x8,x9)'
.ends

.subckt	__net	v1 n1 v2 n2 v8 n8 v9 n9
b1	v1 n1	v='v1(i(b1),i(b2),v(v8,n8),v(v9,n9))'
b2	v2 n2	v='v2(i(b1),i(b2),v(v8,n8),v(v9,n9))'
b8	n8 v8	i='i8(i(b1),i(b2),v(v8,n8),v(v9,n9))'
b9	n9 v9	i='i9(i(b1),i(b2),v(v8,n8),v(v9,n9))'
.func	v1(i1,i2,v8,v9)='h1(i1,i2,v8,v9)'
.func	v2(i1,i2,v8,v9)='h2(i1,i2,v8,v9)'
.func	i8(i1,i2,v8,v9)='h8(i1,i2,v8,v9)'
.func	i9(i1,i2,v8,v9)='h9(i1,i2,v8,v9)'
.ends
