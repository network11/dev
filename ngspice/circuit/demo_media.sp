transmission line with media

.option	noinit

.subckt	tdist	1 2 3 4 5 6 7 8 9 a b	z0=1 td=1
t1	1 0 2 0	z0=z0 td='td/10'
t2	2 0 3 0	z0=z0 td='td/10'
t3	3 0 4 0	z0=z0 td='td/10'
t4	4 0 5 0	z0=z0 td='td/10'
t5	5 0 6 0	z0=z0 td='td/10'
t6	6 0 7 0	z0=z0 td='td/10'
t7	7 0 8 0	z0=z0 td='td/10'
t8	8 0 9 0	z0=z0 td='td/10'
t9	9 0 a 0	z0=z0 td='td/10'
ta	a 0 b 0	z0=z0 td='td/10'
.ends
.subckt	cdist	1 2 3 4 5 6 7 8 9 a b	c=0
c1	1 0	'c/20'
c2	2 0	'c/10'
c3	3 0	'c/10'
c4	4 0	'c/10'
c5	5 0	'c/10'
c6	6 0	'c/10'
c7	7 0	'c/10'
c8	8 0	'c/10'
c9	9 0	'c/10'
ca	a 0	'c/10'
cb	b 0	'c/20'
.ends

.subckt	tc	s 1 2 3 4 5 6 7 8 9 a b	xt=0 xc=0
vs	m 0	pulse(0 2 0 1m 1m)
rs	m s	1
ts	s 0 1 0	z0=1 td=1
xt	1 2 3 4 5 6 7 8 9 a b	tdist	z0='1/sqrt(1+xt)' td='sqrt(1+xt)'
xc	1 2 3 4 5 6 7 8 9 a b	cdist	c='xc'
rb	b 0	'1/sqrt(1+xt+xc)'
.ends

xtr	r0 r1 r2 r3 r4 r5 r6 r7 r8 r9 ra rb	tc
rr	r1 0	'1/(sqrt(1+x)-1)'
xtc	c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 ca cb	tc
cc	c1 0	'x'
xtt	t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb	tc	xt='x'
xts	s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 sa sb	tc	xc='x'

.param	x=3

.control
tran	1m 9.9	uic
plot	r0 r1 r2 r3 r4 r5 r6 r7 r8 r9 ra rb
plot	c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 ca cb
plot	t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb
plot	s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 sa sb
.endc

.end
