ladder network

.option	noinit

.subckt	p1	1 2	l=.159 c=.159
c1	1 0	c='c/2'
c2	2 0	c='c/2'
l1	1 2	l='l'
.ends
.subckt	p2	1 2
x1	1 3	p1
x2	2 3	p1
.ends
.subckt	p4	1 2
x1	1 3	p2
x2	2 3	p2
.ends
.subckt	p8	1 2
x1	1 3	p4
x2	2 3	p4
.ends

v1	0 i1	0	ac=-1
x1	i1 o1	p1
r1	o1 0	1
v2	0 i2	0	ac=-1
x2	i2 o2	p2
r2	o2 0	1
v4	0 i4	0	ac=-1
x4	i4 o4	p4
r4	o4 0	1
v8	0 i8	0	ac=-1
x8	i8 o8	p8
r8	o8 0	1

.control
ac	dec 100 .1 10
plot	mag(i(v1)) mag(i(v2)) mag(i(v4)) mag(i(v8))
plot	mag(v(o1)) mag(v(o2)) mag(v(o4)) mag(v(o8))
plot	ph(i(v1)) ph(i(v2)) ph(i(v4)) ph(i(v8))
plot	ph(v(o1)) ph(v(o2)) ph(v(o4)) ph(v(o8))
.endc

.end
