switch as mosfet

.option	noinit rshunt=1g

.subckt	sw	d s g b	k=1
b1	d s	i='k*uramp(v(g,b))*v(d,s)'
.ends

.subckt	mn1	d s g	k=1
xd	d s g d	sw	k='k/2'
xs	d s g s	sw	k='k/2'
.ends

.subckt	mp1	d s g	k=1
xd	d s d g	sw	k='k/2'
xs	d s s g	sw	k='k/2'
.ends

.subckt	mn10	d s g	k=1
x0	d 1 g	mn1	k='k*10'
x1	1 2 g	mn1	k='k*10'
x2	2 3 g	mn1	k='k*10'
x3	3 4 g	mn1	k='k*10'
x4	4 5 g	mn1	k='k*10'
x5	5 6 g	mn1	k='k*10'
x6	6 7 g	mn1	k='k*10'
x7	7 8 g	mn1	k='k*10'
x8	8 9 g	mn1	k='k*10'
x9	9 s g	mn1	k='k*10'
.ends

.subckt	mp10	d s g	k=1
x0	d 1 g	mp1	k='k*10'
x1	1 2 g	mp1	k='k*10'
x2	2 3 g	mp1	k='k*10'
x3	3 4 g	mp1	k='k*10'
x4	4 5 g	mp1	k='k*10'
x5	5 6 g	mp1	k='k*10'
x6	6 7 g	mp1	k='k*10'
x7	7 8 g	mp1	k='k*10'
x8	8 9 g	mp1	k='k*10'
x9	9 s g	mp1	k='k*10'
.ends

xd0	d0 0 d0 0	sw	k=1m
xs0	s0 0 g0 0	sw	k=1m
vd0	d0 0	0
vs0	s0 0	0
vg0	g0 0	0

xd1	d1 0 d1	mn10	k=1m
xs1	s1 0 g1	mn10	k=1m
vd1	d1 0	0
vs1	s1 0	0
vg1	g1 0	0

xd2	d2 0 d2	mp10	k=1m
xs2	s2 0 g2	mp10	k=1m
vd2	d2 0	0
vs2	s2 0	0
vg2	g2 0	0

.control
dc	vd0 -2 2 10m
plot	-i(vd0)
dc	vg0 -2 2 10m	vs0 -1 1.5 .5
plot	-i(vs0)
dc	vs0 -2 2 10m	vg0 -1 1.5 .5
plot	-i(vs0)
dc	vd1 -2 2 10m
plot	-i(vd1)
dc	vg1 -2 2 10m	vs1 -0 1.5 .5
plot	-i(vs1)
dc	vs1 -0 2 10m	vg1 -1 1.5 .5
plot	-i(vs1)
dc	vd2 -2 2 10m
plot	-i(vd2)
dc	vg2 -2 2 10m	vs2 -1.5 0 .5
plot	-i(vs2)
dc	vs2 -2 0 10m	vg2 -1.5 1 .5
plot	-i(vs2)
.endc

.end
