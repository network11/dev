nonlinear oscillator

.option	noinit seed=random

c	c 0	.159
l	c r	.159
r	r 0	'i(r)^2/3-1'

.control
let	p=pi*sunif(0)
let	m=1m+2.8
while	m>0
	alter	l	ic=m*cos(p)
	alter	c	ic=m*sin(p)
	tran	5m 10	uic
	plot	v(c) vs i(l)	retraceplot
	let	m=m-1.4
end
plot	v(r) vs i(l)	retraceplot
plot	v(c) i(l)
.endc

.end
