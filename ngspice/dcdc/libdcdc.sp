* dcdc model

.subckt	dcdc	vh vl vn cp cn
bl	vl vn	v='u2(v(cp,cn))*v(vh,vn)'
bh	vn vh	i='u2(v(cp,cn))*i(bl)'
.ends

.subckt	dcdc_ss	vh vl vn cp cn	ron=1 roff=1e12
rh	vh vl	'v(cp,cn)>0 ? ron : roff'
rl	vl vn	'v(cp,cn)>0 ? roff : ron'
.ends

.subckt	dcdc_sd	vh vl vn cp cn	ron=1 roff=1e12
rh	vh vl	'v(cp,cn)>0 ? ron : roff'
rl	vl vn	'v(vn,vl)>0 ? ron : roff'
.ends

.subckt	dcdc_ds	vh vl vn cp cn	ron=1 roff=1e12
rh	vh vl	'v(vl,vh)>0 ? ron : roff'
rl	vl vn	'v(cp,cn)>0 ? roff : ron'
.ends
