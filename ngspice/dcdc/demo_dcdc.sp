dcdc

.option	noinit
.inc	libdcdc.sp

xd0	i md0 0 d 0	dcdc
ld0	od0 md0	'l'
cd0	od0 0	'c'
rd0	od0 0	'r'

xd1	i md1 0 d c	dcdc_ss
ld1	od1 md1	'l'
cd1	od1 0	'c'
rd1	od1 0	'r'

xd2	i md2 0 d c	dcdc_sd
ld2	od2 md2	'l'
cd2	od2 0	'c'
rd2	od2 0	'r'

lu0	i mu0	'l'
xu0	ou0 mu0 0 d 0	dcdc
cu0	ou0 0	'c'
ru0	ou0 0	'r'

lu1	i mu1	'l'
xu1	ou1 mu1 0 d c	dcdc_ss
cu1	ou1 0	'c'
ru1	ou1 0	'r'

lu2	i mu2	'l'
xu2	ou2 mu2 0 d c	dcdc_ds
cu2	ou2 0	'c'
ru2	ou2 0	'r'

vi	i 0	1
vd	d 0	pulse(.75 .25 1 0 0 1 2)
vc	c 0	pwl(0 1 'per/2' 0 'per' 1) r=0

.param	per=10m l=78 c=5u r=10k

.control
tran	.1m 3	1m uic
plot	i*d od0 od1 od2
plot	i/d ou0 ou1 ou2
.endc

.end
