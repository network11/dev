dcdc ripple

.option	noinit
.inc	libdcdc.sp

xd	i md 0 d c	dcdc_ss
ld	od md	'l'
cd	od 0	'c'
rd	od 0	'r'

lu	i mu	'l'
xu	ou mu 0 d c	dcdc_ss
cu	ou 0	'c'
ru	ou 0	'r'

bpd	pd 0	v='pd(v(i),v(d))'
bpu	pu 0	v='pu(v(i),v(d))'
.func	pd(i,d)='i*d*(1-d)/8 * (per^2/l/c)'
.func	pu(i,d)='i/d*(1-d) * (per/c/r)'

vi	i 0	1
bd	d 0	v='1-ceil(time)/4'
vc	c 0	pwl(0 1 'per/2' 0 'per' 1) r=0

.param	per=10m l=78 c=5u r=10k

.control
tran	.1m 3	1m uic
plot	-pd/2 pd/2 od-i*d	ylimit -5m 5m
plot	-pu/2 pu/2 ou-i/d	ylimit -.5 .5
.endc

.end
