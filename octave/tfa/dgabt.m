%% v = dgabt(n, p=0)
%%	discrete Gabor transform kernel

function v = dgabt(n, p=0)
	g = toeplitz(dgaus(n));
	f = ifft(eye(n))*sqrt(n);
	p = fft(eye(n)).^p;
	v = reshape(g,n,n,1) .* reshape(f,n,1,n) .* reshape(p,1,n,n);
end

%!test
%!	for n = 1:17 for p = -1:2
%!		v = dgabt(n,p);
%!		assert(size_equal(v, zeros(n,n,n)));
%!		assert(v(:,:)*v(:,:)', eye(n), eps^.5);
%!		assert(abs(v), abs(dgabt(n,p-1)), eps^.5);
%!		assert(v({[1 n:-1:2]}{[1 1 1]}), v, eps^.5);
%!		r = circshift(rot90(reshape(1:n^2,n,n)),1);
%!		assert(fft(v(:,:))/sqrt(n), dgabt(n,1-p)(:,r), eps^.5);
%!	end end

%!demo
%!	n = 64; h = imshow(zeros(n,n,3));
%!	for p = 0:1
%!		v = dgabt(n,p); v /= max(abs(v(:)));
%!	for i = 1:n x = v(:,i,:)(:,:);
%!		set(h,'cdata',(cat(3,real(x),imag(x),x*0)+1)/2);
%!		drawnow; pause(1/n);
%!	end end

%!demo
%!	n = 64; h = imshow(zeros(n));
%!	s = 1:n/8:n;
%!	v = dgabt(n); v = abs(v(:,:)'*v(:,s,s)(:,:)); v /= max(v(:));
%!	for v = v
%!		set(h,'cdata',reshape(v,n,n));
%!		drawnow; pause(1/16);
%!	end

%!demo
%!	n = 64; h = imshow(zeros(n));
%!	g = dgabt(n); m = max(abs(g(:,:)'*g(:,1)));
%!	for s = sub2ind([n n],1+n/8*[1:3;2 4 4],1+n/8*[1:3;0 0 2])'
%!	for a = 0:1/16:1
%!		v = abs(g(:,:)'*(dfrft(n,a)*g(:,s))); v /= m;
%!		set(h,'cdata',fftshift(reshape(v,n,n,[])));
%!		drawnow; pause(1/16);
%!	end end
