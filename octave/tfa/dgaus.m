%% v = dgaus(n)
%%	discrete Gaussian

function v = dgaus(n)
	a = speye(n); a = a*2-circshift(a,1)-circshift(a,-1);
	a = a + diag(1-cos(2*pi/n*(0:n-1)))*2;
	if (n == 1) v = 1; return end
	[v,~] = eigs(a,1,'sa');
	v = v * sign(v(1));
end

%!test
%!	for n = 1:65
%!		assert(dgaus(n), dherm(n)(:,1), eps^.5);
%!	end

%!demo
%!	t = -2:1/16:2; g = exp(-pi*t.^2)*nthroot(2,4);
%!	for n = 1:16 subplot(4,4,n);
%!		x = floor(center(1:n))/sqrt(n);
%!		y = fftshift(dgaus(n))*nthroot(n,4);
%!		plot(x,y,'o-',t,g); grid on;
%!	end
