%% v = dfrft(n, a=1)
%%	discrete fractional Fourier transform kernel

function v = dfrft(n, a=1)
	[v,l] = dherm(n);
	v = v * diag(1j^a.^l) * v';
end

%!test
%!	for n = 1:65
%!		assert(dfrft(n,0), eye(n), eps^.5);
%!		assert(dfrft(n,1), ifft(eye(n))*sqrt(n), eps^.5);
%!		assert(dfrft(n,2), eye(n)(:,[1 n:-1:2]), eps^.5);
%!		assert(dfrft(n,3), fft(eye(n))/sqrt(n), eps^.5);
%!		a = randn*2; v = dfrft(n,a);
%!		b = randn*2; u = dfrft(n,b);
%!		assert(issymmetric(v, eps^.5));
%!		assert(v'*v, eye(n), eps^.5);
%!		assert(v({[1 n:-1:2]}{[1 1]}), v, eps^.5);
%!		assert(dfrft(n,-a)', v, eps^.5);
%!		assert(dfrft(n,a+4), v, eps^.5);
%!		assert(dfrft(n,a+b), v*u, eps^.5);
%!	end

%!demo
%!	n = 64; h = imshow(zeros(n,n,3));
%!	for a = 0:1/16:4
%!		v = dfrft(n,a); v = fftshift(v);
%!		set(h,'cdata',(cat(3,real(v),imag(v),v*0)+1)/2);
%!		drawnow; pause(1/16);
%!	end

%!demo
%!	n = 64; h = imshow(zeros(n));
%!	for a = 0:1/4:1
%!		v = abs(dgabt(n)(:,:)'*dfrft(n,a)); v /= max(v(:));
%!	for v = v
%!		set(h,'cdata',fftshift(reshape(v,n,n)));
%!		drawnow; pause(1/n);
%!	end end

%!demo
%!	n = 64; h = imshow(zeros(n));
%!	g = dgabt(n); m = max(abs(g(:)));
%!	s = 1+n/4*(0:2);
%!	for a = 0:1/16:1
%!		v = abs(g(:,:)'*dfrft(n,a)(:,s)); v /= m;
%!		set(h,'cdata',fftshift(reshape(v,n,n,[])));
%!		drawnow; pause(1/16);
%!	end
