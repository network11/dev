%% [v, l] = dherm(n)
%%	discrete Hermite functions

function [v, l] = dherm(n)
	a = speye(n); a = a*2-circshift(a,1)-circshift(a,-1);
	a = a + diag(1-cos(2*pi/n*(0:n-1)))*2;
	i1 = (0:n-1)>n/2; i0 = ~i1;
	p = diag(i0-i1) + speye(n)([1 n:-1:2],:);
	p = p / diag(sqrt(sumsq(p)));
	a = p * a * p;
	[v0,l0] = eig(a(i0,i0),'vector');
	[v1,l1] = eig(a(i1,i1),'vector');
	[~,i] = sort([l0;l1-eps^.75]);
	l = [(1:nnz(i0))*2-2 (1:nnz(i1))*2-1](i);
	v = p * blkdiag(v0,v1)(:,i);
	[~,i] = max(cumsum(abs(v(1:ceil(n/2),:))>eps^.5,1),[],1);
	v = v .* sign(v(sub2ind([n n],i,1:n)));
%	assert(p,p'); assert(p*p, eye(n), eps^.5);
end

%!test
%!	for n = 1:65
%!		[v,l] = dherm(n);
%!		assert(issorted(l(mod(l,2)==0)));
%!		assert(issorted(l(mod(l,2)==1)));
%!		a = speye(n); a = a*2-circshift(a,1)-circshift(a,-1);
%!		a = a + diag(1-cos(2*pi/n*(0:n-1)))*2;
%!		l = v'*a*v; assert(l, diag(diag(l)), eps^.5);
%!		l = diag(l); assert(l, sort(l), eps^.5);
%!	end

%!test
%!	for n = 1:65
%!		[v,l] = dherm(n);
%!		assert(sort(l), [0:n-2 n-mod(n,2)]);
%!		assert(size(v), [n n]);
%!		assert(isreal(v));
%!		assert(v'*v, eye(n), eps^.5);
%!		assert(v([1 n:-1:2],:), (-1).^l.*v, eps^.5);
%!		assert(fft(v)/sqrt(n), (-1j).^l.*v, eps^.5);
%!	end

%!demo
%!	n = 64;
%!	v = dherm(n); v = fftshift(v,1); v /= max(abs(v(:)));
%!	imshow((v+1)/2);

%!demo
%!	n = 64; h = imshow(zeros(n));
%!	v = abs(dgabt(n)(:,:)'*dherm(n)); v /= max(v(:));
%!	for v = v
%!		set(h,'cdata',fftshift(reshape(v,n,n)));
%!		drawnow; pause(1/n);
%!	end
