%% [x, y] = ppol2cart(theta, r)
%%	Transform periodized polar coordinates to Cartesian coordinates.

function [x,y] = ppol2cart(t,r)
	ct = cos(t); st = sin(t); cr = cos(r); sr = sin(r);
	rr = sqrt((hypot(2*ct.*st.*sr,cr)+cr).*(1+cr)/2).\sr;
	x = 2*atan(ct.*rr);
	y = 2*atan(st.*rr);
end

%!shared tol,sz
%!	tol = eps^.5; sz = [9 3];
%!test
%!	t = 1e4*(rand(sz)*2-1);
%!	r = 1e4*(rand(sz)*2-1);
%!	[x,y] = ppol2cart(t,r);
%!	[a,b] = ppol2cart(t+2*pi,r); assert({a b}, {x y}, tol);
%!	[a,b] = ppol2cart(t,r+2*pi); assert({a b}, {x y}, tol);
%!test
%!	t = 1e4*(rand(sz)*2-1);
%!	r = 1e4\(rand(sz)*2-1);
%!	[x,y] = pol2cart(t,r);
%!	[a,b] = ppol2cart(t,r/sqrt(2));
%!	assert({a b}, {x y}, tol);
%!test
%!	x = pi*(rand(sz)*2-1);
%!	y = pi*(rand(sz)*2-1);
%!	[t,r] = pcart2pol(x,y);
%!	[a,b] = ppol2cart(t,r);
%!	assert({a b}, {x y}, tol);

%!demo
%!	t = -5:.01:5; f = t';
%!	t0 = vec(-9:9,3);
%!	y = exp(-pi*(t-t0).^2-2j*pi*f.*(t0-t/2));
%!	y = sum(y,3);
%!	assert(y, rot90(y), eps^.5);
%!	figure(1); imshow(abs(y),[0 1.11],'xdata',t,'ydata',f); axis xy on;
%!	figure(2); imshow(arg(y),[-pi pi],'xdata',t,'ydata',f); axis xy on;
%!demo
%!	t = -10:.05:10; f = t'; z = t+1j*f;
%!	h = imshow(real(z),[-1 1],'xdata',t,'ydata',f); axis xy on; grid on;
%!	for z0 = [ 3*exp(2j*pi*(0:.01:1)) (-5:.1:5)*exp(.3j*pi) ]
%!		y = exp(-pi/2*abs(z-z0).^2+1j*pi*imag(z0.*conj(z)))/sqrt(2);
%!		set(h,'cdata',real(y)); drawnow;
%!	end
%!demo
%!	t = -10:.05:10; f = t'; z = t+1j*f;
%!	h = imshow(real(z),[-1 1],'xdata',t,'ydata',f); axis xy on; grid on;
%!	for t0 = -5:.1:5
%!		y = exp(-pi*(t-t0).^2-2j*pi*f.*(t0-t/2));
%!		set(h,'cdata',real(y)); drawnow;
%!	end
%!	for f0 = -5:.1:5
%!		y = exp(-pi*(f-f0).^2+2j*pi*t.*(f0-f/2));
%!		set(h,'cdata',real(y)); drawnow;
%!	end
