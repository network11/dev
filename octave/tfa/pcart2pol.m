%% [theta, r] = pcart2pol(x, y)
%%	Transform periodized Cartesian coordinates to polar coordinates.

function [t,r] = pcart2pol(x,y)
	t = atan2(tan(y/2),tan(x/2));
	r = acos((cos(x)+cos(y))/2);
end

%!shared tol,sz
%!	tol = eps^.5; sz = [9 3];
%!test
%!	x = 1e4*(rand(sz)*2-1);
%!	y = 1e4*(rand(sz)*2-1);
%!	[t,r] = pcart2pol(x,y);
%!	[a,b] = pcart2pol(x+2*pi,y); assert({a b}, {t r}, tol);
%!	[a,b] = pcart2pol(x,y+2*pi); assert({a b}, {t r}, tol);
%!test
%!	x = 1e4\(rand(sz)*2-1);
%!	y = 1e4\(rand(sz)*2-1);
%!	[t,r] = cart2pol(x,y);
%!	[a,b] = pcart2pol(x,y);
%!	assert({a b}, {t r/sqrt(2)}, tol);
%!test
%!	t = pi*(rand(sz)*2-1);
%!	r = pi*rand(sz);
%!	[x,y] = ppol2cart(t,r);
%!	[a,b] = pcart2pol(x,y);
%!	assert({a b}, {t r}, tol);

%!demo
%!	[x,y] = ppol2cart(deg2rad(-179:2:181)',deg2rad(0:10:180));
%!	[a,b] = ppol2cart(deg2rad(-175:10:175),deg2rad(0:2:180)');
%!	plot(x/pi,y/pi,'--',a/pi,b/pi); grid on; axis([-1 1 -1 1],'equal');
%!demo
%!	x = deg2rad(-180:2:180); l = deg2rad(-180:10:180);
%!	[t,r] = pcart2pol(x,x'); contour(x/pi,x/pi,r,l,'--');
%!	hold on; contour(x/pi,x/pi,t,l); grid on; axis equal;

%!demo
%!	[x1,y1] = ndgrid(deg2rad(-179:2:179),-deg2rad(min(179,0:45:180)));
%!	h = plot(x1/pi,y1/pi); grid on; axis([-1 1 -1 1],'equal');
%!	for t1 = deg2rad(0:2:90)
%!		[t,r] = pcart2pol(x1,y1); [x,y] = ppol2cart(t+t1,r);
%!		set(h,{'xdata' 'ydata'},[num2cell(x/pi,1);num2cell(y/pi,1)]');
%!		drawnow; pause(2/90);
%!	end
