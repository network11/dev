%% colorpick(filename)

function colorpick(varargin)
	set(clf,'windowbuttonmotionfcn',{@fcn,[imshow(varargin{:}) title(0)]});
end

function fcn(~,~, h)
	try
		i = round(get(gca,'currentpoint')(1,1:2));
		i = vec(get(h(1),'cdata')(i(2),i(1),:),2);
		set(h(2),'string',num2str(i));
	end
end

%!demo colorpick('default.img');
%!demo colorpick('octave-sombrero.png');
