%% Ymesh = star2mesh(Ystar)
%%	star-mesh transform

function y = star2mesh(y)
	assert(isvector(y));
	y = y.'.*y / sum(y);
	y = y-diag(diag(y));
end

%!test
%!	for n = 1:9 for i = 1:5
%!		gstar = rande(1,n)+1j*randn(1,n);
%!		gmesh = star2mesh(gstar);
%!		assert(issymmetric(gmesh));
%!		assert(diag(gmesh) == 0);
%!		v = randn(1,n)+1j*randn(1,n);
%!		istar = gstar.*(v-sum(gstar.*v)/sum(gstar));
%!		imesh = sum(gmesh.*(v-v.'));
%!		assert(imesh, istar, eps^.5);
%!		assert(sum(istar), 0, eps^.5);
%!	end end
