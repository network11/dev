%% map = colorcode(n)
%%	electronic color code

function map = colorcode(n=-3:9)
	map = reshape(hex2dec(reshape([
		'FF69B4'	% Pink
		'C0C0C0'	% Silver
		'CFB53B'	% Gold
		'000000'	% Black
		'964B00'	% Brown
		'FF0000'	% Red
		'FFA500'	% Orange
		'FFFF00'	% Yellow
		'9ACD32'	% Green
		'6495ED'	% Blue
		'9400D3'	% Violet
		'A0A0A0'	% Gray
		'FFFFFF'	% White
		]',2,[])'),3,[])'(n+4,:)/255;
end

%!demo imshow(i=1:13,colorcode); text(i,0*i,num2str((-3:9)'));
