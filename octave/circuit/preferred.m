%% [values,relerr] = preferred(series)
%%	E-series of preferred values

function [values,relerr] = preferred(series=6)
	r = logspace(0,1,series*2+1);
	values = r(1:2:end-1);
	relerr = r(2) - 1;
end

%!demo
%!	[a,e] = preferred(24); a = (1+e).^(-1:1)'*a;
%!	b = [ 1 1.2 1.5 1.8 2.2 2.7 3.3 3.9 4.7 5.6 6.8 8.2
%!		1.1 1.3 1.6 2.0 2.4 3.0 3.6 4.3 5.1 6.3 7.5 9.1 ](:);
%!	semilogy(0:numel(b)-1,a,'+--k',0:numel(b)-1,b,'o-'); grid on;
%!	axis([0 24 1 10]); xticks(0:4:24);
%!	E6  = reshape(b(1:4:end),1,6)
%!	E12 = reshape(b(1:2:end),2,6)
%!	E24 = reshape(b(1:1:end),4,6)
