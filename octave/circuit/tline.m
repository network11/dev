%% [v,i, ist,msg] = tline(z_x,y_x, v_0,i_0, x, ...)
%%	transmission line

function [v,i, varargout] = tline(z_x,y_x, v_0,i_0, x, varargin)
	vi_0 = [v_0 i_0];
	fcn = @(vi,x) [z_x(x) y_x(x)].*[vi(2) vi(1)];
	[vi, varargout{1:nargout-1}] = clsode(fcn, vi_0, x, varargin{:});
	[v,i] = deal(num2cell(reshape(vi,[size(x) 2]),1:ndims(x)){:});
end

%!xtest
%!	[z,y,v0,i0] = num2cell(randn(1,4)+1j*randn(1,4)){:};
%!	x = sort([0 rand(1,9)*3]);
%!	[v,i] = tline(@(x)z,@(x)y,v0,i0,x);
%!	vi = [arrayfun(@(x){expm([0 z;y 0]*x)*[v0;i0]},x){:}];
%!	assert(vi, [v;i], 1e-3);

%!demo
%!	x = 0:1e-3:2; z0 = 50; fn = @(x) [50 150 450](lookup(0:.75:1.5,x));
%!	[v,i] = tline(@(x)2j*pi*fn(x),@(x)2j*pi/fn(x),z0,1,x);
%!	z = v./i; zc = fn(x); s11 = (z-zc)./(z+zc);
%!	subplot(221); plot(x,zc,'k',x,real(z),x,imag(z)); grid on; ylabel Z;
%!	subplot(223); plot(x,real(s11),x,imag(s11)); grid on; ylabel S;
%!	subplot(222); plot(zc,0*zc,'k',real(z),imag(z)); grid on; axis equal;
%!	subplot(224); plot(real(s11),imag(s11)); grid on; axis equal;

%!demo
%!	x = -4:1e-2:4; z0 = 1; fn = @(x,l) 2+max(-1,min(1,x/l));
%!	subplot(211); h = plot(x,x,x,x,x,x); grid on; ylim([-4 10]); ylabel Z;
%!	subplot(212); h = [h;plot(x,x,x,x)]; grid on; ylim([-1 1]); ylabel S;
%!	for l = 4.^[1:-1/8:-1 -1:-1/4:-2 -5]
%!		[v,i] = tline(@(x)2j*pi*fn(x,l),@(x)2j*pi/fn(x,l),z0,1,x);
%!		z = v./i; zc = fn(x,l); s11 = (z-zc)./(z+zc);
%!		set(h,{'ydata'},{real(z);imag(z);zc;real(s11);imag(s11)});
%!		drawnow;
%!	end
