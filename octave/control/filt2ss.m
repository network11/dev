%% [a,b,c,d] = filt2ss(b,a)
%%	SISO digital filter from transfer function to state-space
%%	using its direct form II transposed implementation.

function [a,b,c,d] = filt2ss(b,a)
	assert(isvector(b));
	assert(isvector(a));
	n = max(numel(b),numel(a));
	[a,b,c,d] = tf2ss(vec(postpad(b,n),3), postpad(a,n), 2);
end

%!test
%!	for n = 1:5
%!	for m = 1:5
%!		hb = randn(1,m);
%!		ha = randn(1,n);
%!		[a,b,c,d] = filt2ss(hb,ha);
%!		s = randn(max(n,m)-1,1);
%!	for l = 0:7
%!		x = randn(1,l);
%!		[y1,s1] = filter(hb,ha,x);
%!		[y2,s2] = ssfilter(a,b,c,d,x);
%!		assert({y1 s1}, {y2 s2}, -eps^.5);
%!		[y1,s1] = filter(hb,ha,x,s);
%!		[y2,s2] = ssfilter(a,b,c,d,x,s);
%!		assert({y1 s1}, {y2 s2}, -eps^.5);
%!	end
%!	end
%!	end
