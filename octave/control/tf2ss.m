%% [a,b,c,d] = tf2ss(b,a,opt)
%%	MIMO A/D filter from transfer function to state-space.
%%	numel(a) == n
%%	size(b) == [q p n]
%%	The optional argument OPT selects the implementation:
%%		1: controllable form. [default]
%%		2: observable form.
%%		3: parallel controllable form.
%%		4: parallel observable form.

function [a,b,c,d] = tf2ss(hb,ha,opt=1)
	assert(size(hb,3) == numel(ha));
	switch (opt)
	case 1
		d = hb(:,:,1)/ha(1); hb = hb-d.*vec(ha,3);
		c = horzcat(zeros(rows(d),0),num2cell(hb,1:2){2:end})/ha(1);
		b = eye(columns(c),columns(d));
		a = kron(compan(ha), eye(columns(d)));
	case 2
		d = hb(:,:,1)/ha(1); hb = hb-d.*vec(ha,3);
		b = vertcat(zeros(0,columns(d)),num2cell(hb,1:2){2:end})/ha(1);
		c = eye(rows(d),rows(b));
		a = kron(compan(ha).', eye(rows(d)));
	case 3
		[hb,ha,d,he] = residuem(hb,ha);
		c = horzcat(zeros(rows(d),0),num2cell(hb,1:2){:});
		b = kron(he(:)==1, eye(columns(d)));
		a = kron(diag(ha)+diag(he(2:end)~=1,-1), eye(columns(d)));
	case 4
		[hb,ha,d,he] = residuem(hb,ha);
		b = vertcat(zeros(0,columns(d)),num2cell(hb,1:2){:});
		c = kron(he(:).'==1, eye(rows(d)));
		a = kron(diag(ha)+diag(he(2:end)~=1,1), eye(rows(d)));
	otherwise
		error('type %s not recognized', disp(opt)(1:end-1));
	end
end

function [r,p,k,e] = residuem(b,a)
	b = cellfun(@vec,num2cell(b,3),'uniformoutput',0);
	[r,p,k,e] = cellfun(@residue,b,{a},'uniformoutput',0);
	r = cell2mat(cellfun(@vec,r,{3},'uniformoutput',0));
	k = cell2mat(k);
	assert(isequal(p{1},p{:})); p = p{1};
	assert(isequal(e{1},e{:})); e = e{1};
end

%!function [b,a] = residuem(r,p,k)
%!	r = cellfun(@vec,num2cell(r,3),'uniformoutput',0);
%!	k = num2cell(k);
%!	[b,a] = cellfun(@residue,r,{p},k,'uniformoutput',0);
%!	b = cell2mat(cellfun(@vec,b,{3},'uniformoutput',0));
%!	assert(isequal(a{1},a{:})); a = a{1};
%!endfunction

%!function co = ctrb(a, b)
%!	co = [arrayfun(@(k)a^k*b, 0:length(a)-1, 'uniformoutput',0){:}];
%!endfunction
%!function ob = obsv(a, c)
%!	ob = ctrb(a.', c.').';
%!endfunction
%!xtest
%!	z = randn + 1j*randn;
%!	for n = 1:5
%!	for p = 1:3
%!	for q = 1:3
%!		x = randn(p,7);
%!		hb = randn(q,p,n);
%!		ha = randn(1,n);
%!	for opt = 1:2
%!		[a,b,c,d] = tf2ss(hb,ha,opt);
%!		[a b];[c d];[a;c];[b;d]; r = length(a);
%!		assert(polymval(hb,z)/polyval(ha,z), c/(z*eye(r)-a)*b+d, eps^.5);
%!		assert(tffilter(hb,ha,x), ssfilter(a,b,c,d,x), -eps^.5);
%!		assert(rank(ifelse(mod(opt,2),ctrb(a,b),obsv(a,c)), eps^.5), r);
%!	end
%!	end
%!	end
%!	end
%!xtest
%!	z = randn + 1j*randn;
%!	for n(1) = 1:3
%!	for n(2) = 0:n(1)
%!	for p = 1:3
%!	for q = 1:3
%!		x = randn(p,7);
%!		hb = randn(q,p,sum(n));
%!		ha = repelem(randn(1,numel(n)),n);
%!		[hb,ha] = residuem(hb,ha,randn(q,p));
%!	for opt = 3:4
%!		[a,b,c,d] = tf2ss(hb,ha,opt);
%!		[a b];[c d];[a;c];[b;d]; r = length(a);
%!		assert(polymval(hb,z)/polyval(ha,z), c/(z*eye(r)-a)*b+d, eps^.5);
%!		assert(tffilter(hb,ha,x), ssfilter(a,b,c,d,x), -eps^.5);
%!		assert(rank(ifelse(mod(opt,2),ctrb(a,b),obsv(a,c)), eps^.5), r);
%!	end
%!	end
%!	end
%!	end
%!	end

%!demo
%!	a = 91:94; b = [1:3;4:6]+10*vec(1:4,3); d = [1:2:5;2:2:6]+80;
%!	ha = [1 a]; hb = cat(3,0*d,b)+d.*vec([1 a],3);
%!	for opt = 1:2 disp(opt);
%!		[a,b,c,d] = tf2ss(-5*hb,-5*ha,opt);
%!		s = [a b;c d]; disp(s);
%!	end
%!demo
%!	a = [92 91 91 91]; b = [1:3;4:6]+10*vec(1:4,3); d = [1:2:5;2:2:6]+80;
%!	function [b,a] = residuem(r,p,k)
%!		r = cellfun(@vec,num2cell(r,3),'uniformoutput',0);
%!		k = num2cell(k);
%!		[b,a] = cellfun(@residue,r,{p},k,'uniformoutput',0);
%!		b = cell2mat(cellfun(@vec,b,{3},'uniformoutput',0));
%!		assert(isequal(a{1},a{:})); a = a{1};
%!	end
%!	[hb,ha] = residuem(b,a,d);
%!	for opt = 3:4 disp(opt);
%!		[a,b,c,d] = tf2ss(-5*hb,-5*ha,opt);
%!		s = [a b;c d]; disp(round(s)); assert(s,round(s),1e-4);
%!	end
