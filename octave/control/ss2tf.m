%% [b,a] = ss2tf(a,b,c,d)
%%	MIMO A/D filter from state-space to transfer function.
%%	size(a) == [r r]
%%	size(b) == [r p]
%%	size(c) == [q r]
%%	size(d) == [q p]

function [hb,ha] = ss2tf(a,b,c,d)
	[hb,ha] = polym(a);
	hb = blkmm(repmat(c,1,1,size(hb,3)), hb);
	hb = blkmm(hb, repmat(b,1,1,size(hb,3)));
	hb = cat(3,zeros(size(d)),hb) + d .* vec(ha,3);
end

%!xtest
%!	z = randn + 1j*randn;
%!	for r = 0:5
%!	for p = 1:3
%!	for q = 1:3
%!		x = randn(p,7);
%!		[a,c,b,d] = mat2cell(randn(r+q,r+p),[r q],[r p]){:};
%!		[hb,ha] = ss2tf(a,b,c,d);
%!		assert(size_equal(hb, zeros(q,p,numel(ha))));
%!		assert(polymval(hb,z)/polyval(ha,z), c/(z*eye(r)-a)*b+d, eps^.5);
%!		assert(tffilter(hb,ha,x), ssfilter(a,b,c,d,x), -eps^.5);
%!	end
%!	end
%!	end
