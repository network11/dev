%% y = ssfilter(a,b,c,d,x)
%% [y,sf] = ssfilter(a,b,c,d,x)
%% [y,sf] = ssfilter(a,b,c,d,x,si)
%%	MIMO digital filter in state-space form.
%%	size(a) == [r r]
%%	size(b) == [r p]
%%	size(c) == [q r]
%%	size(d) == [q p]
%%	size(x) == [p l]
%%	size(y) == [q l]
%%	size(s) == [r 1]

function [y,s] = ssfilter(a,b,c,d,x,s=[])
	[q,p] = size(d);
	r = length(a);
	l = columns(x);
	if (isempty(s)) s = zeros(r,1); end

	y = nan(q,l);
	for k = 1:l
		y(:,k) = c*s + d*x(:,k);
		s      = a*s + b*x(:,k);
	end
end

%!test
%!	for r = 0:3
%!	for p = 0:3
%!	for q = 0:3
%!		[a,c,b,d] = mat2cell(randn(r+q,r+p),[r q],[r p]){:};
%!	for l1 = 0:3
%!	for l2 = 0:3
%!		x1 = randn(p,l1);
%!		x2 = randn(p,l2);
%!		[y1,s] = ssfilter(a,b,c,d,x1);
%!		[y2,s] = ssfilter(a,b,c,d,x2,s);
%!		[y,sf] = ssfilter(a,b,c,d,[x1 x2]);
%!		assert(size(y), [q l1+l2]);
%!		assert(size(s), [r 1]);
%!		assert({y,sf}, {[y1 y2],s}, eps^.5);
%!	end
%!	end
%!	end
%!	end
%!	end
