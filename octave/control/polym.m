%% [B,a] = polym(A)
%%	Calculate resolvent matrix using Leverrier's algorithm.
%%	inv(z*eye(n)-A)(i,j) == polyval(B(i,j,:),z) / polyval(a,z)

function [p,c] = polym(a)
	assert(issquare(a));
	n = length(a);
	p = zeros(n,n,n+1);
	c = ones(1,n+1);
	for k = 1:n
		p(:,:,k+1) = p(:,:,k)*a + c(k)*eye(n);
		c(k+1) = -trace(p(:,:,k+1)*a)/k;
	end
	p = p(:,:,2:end);
end

%!function [p,c] = resolvent(a)
%!	c = poly(a);
%!	p = zeros(0,0,0);
%!	for k = 1:length(a)
%!		p(:,:,k) = polyvalm(c(1:k),a);
%!	end
%!endfunction

%!test
%!	z = randn + 1j*randn;
%!	for n = 0:9
%!		a = randn(n) + 1j*randn(n);
%!		[p,c] = polym(a);
%!		assert({p,c}, nthargout(1:2,@resolvent,a), eps^.5);
%!		assert(polymval(p,z)/polyval(c,z), inv(z*eye(n)-a), eps^.5);
%!		assert(polyval(c,z), det(z*eye(n)-a), eps^.5);
%!		assert(polyvalm(c,a), zeros(n), eps^.5);
%!			% Cayley-Hamilton theorem
%!	end
