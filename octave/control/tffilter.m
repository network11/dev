%% y = tffilter(b,a,x)
%% [y,sf] = tffilter(b,a,x)
%% [y,sf] = tffilter(b,a,x,si)
%%	MIMO digital filter in transfer function form.
%%	numel(a) == n
%%	size(b) == [q p m]
%%	size(x) == [p l]
%%	size(y) == [q l]
%%	size(s) == [q p max(n,m)-1]

function [y,s] = tffilter(b,a,x,s=[])
	[q,p,m] = size(b);
	n = numel(a);
	l = columns(x);
	if (isempty(s)) s = zeros(q,p,max(n,m)-1); end

	y = zeros(q,l);
	for i = 1:q for j = 1:p
		[o,s(i,j,:)] = filter(b(i,j,:)(:),a,x(j,:),s(i,j,:)(:),2);
		y(i,:) += o;
	end end
end

%!test
%!	for n = 1:3
%!	for m = 1:3
%!	for p = 0:3
%!	for q = 0:3
%!		b = randn(q,p,m);
%!		a = randn(1,n);
%!	for l1 = 0:3
%!	for l2 = 0:3
%!		x1 = randn(p,l1);
%!		x2 = randn(p,l2);
%!		[y1,s] = tffilter(b,a,x1);
%!		[y2,s] = tffilter(b,a,x2,s);
%!		[y,sf] = tffilter(b,a,[x1 x2]);
%!		assert(size(y), [q l1+l2]);
%!		assert(size_equal(s, zeros(q,p,max(n,m)-1)));
%!		assert({y,sf}, {[y1 y2],s}, eps^.5);
%!	end
%!	end
%!	end
%!	end
%!	end
%!	end
