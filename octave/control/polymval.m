%% Y = polymval(P, x)
%%	Evaluate a polynomial matrix.
%%	Y(i,j) == polyval(P(i,j,:), x)

function y = polymval(p,x)
	p = cellfun(@vec, num2cell(p,3), 'uniformoutput',0);
	y = cellfun(@polyval, p, {x});
end
