%% filewrite(filename, str)

function filewrite(filename, str)
	[fid, msg] = fopen(filename, 'w');
	if (fid < 0) error(msg); end
	ocu = onCleanup(@() fclose(fid));
	fwrite(fid, str);
end

%!test
%!	fname = tempname;
%!	ocu = onCleanup(@() unlink(fname));
%!	for n = [0:9 randi(1e3,1,30)]
%!		str = char(randi([0 255],1,n));
%!		filewrite(fname, str);
%!		assert(str, fileread(fname));
%!	end
