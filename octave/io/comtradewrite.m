%% comtradewrite(cfgname,datname, cfginfo,datinfo)

function comtradewrite(cfgname,datname, cfginfo,datinfo)

	cfginfo.start   = sec2str(cfginfo.start  );
	cfginfo.trigger = sec2str(cfginfo.trigger);
	c = structfun(@any2str, cfginfo, 'uniformoutput',0);
	[fid, msg] = fopen(cfgname, 'w');
	if (fid < 0) error(msg); end
	fprintf(fid, '%s,%s,%s\r\n', [
		c.station_name ;
		c.rec_dev_id   ;
		c.rev_year     ;
		]{:});
	fprintf(fid, '%s,%sA,%sD\r\n', [
		c.TT           ;
		c.A            ;
		c.D            ;
		]{:});
	fprintf(fid, '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\r\n', [
		c.An           ;
		c.Ach_id       ;
		c.Aph          ;
		c.Accbm        ;
		c.uu           ;
		c.a            ;
		c.b            ;
		c.skew         ;
		c.min          ;
		c.max          ;
		c.primary      ;
		c.secondary    ;
		c.PS           ;
		]{:});
	fprintf(fid, '%s,%s,%s,%s,%s\r\n', [
		c.Dn           ;
		c.Dch_id       ;
		c.Dph          ;
		c.Dccbm        ;
		c.y            ;
		]{:});
	fprintf(fid, '%s\r\n', [
		c.lf           ;
		]{:});
	fprintf(fid, '%s\r\n', [
		c.nrates       ;
		]{:});
	fprintf(fid, '%s,%s\r\n', [
		c.samp         ;
		c.endsamp      ;
		]{:});
	fprintf(fid, '%s\r\n%s\r\n', [
		c.start        ;
		c.trigger      ;
		]{:});
	fprintf(fid, '%s\r\n', [
		c.ft           ;
		]{:});
	fprintf(fid, '%s\r\n', [
		c.timemult     ;
		]{:});
	fclose(fid);

	switch (cfginfo.ft)
	case {'ASCII','ascii'}
		csvwrite(datname, [
			datinfo.n         ...
			datinfo.timestamp ...
			datinfo.Ak        ...
			datinfo.Dm        ...
			], 'newline','pc');
	case {'BINARY','binary'}
		datinfo.n         = toint16(datinfo.n        , 'uint32');
		datinfo.timestamp = toint16(datinfo.timestamp, 'uint32');
		datinfo.Ak        = toint16(datinfo.Ak       ,  'int16');
		datinfo.Dm        = toint16(datinfo.Dm, 'logical');
		[fid, msg] = fopen(datname, 'w');
		if (fid < 0) error(msg); end
		fwrite(fid, [
			datinfo.n         ...
			datinfo.timestamp ...
			datinfo.Ak        ...
			datinfo.Dm        ...
			]', 'int16', 0, 'l');
		fclose(fid);
	end

end

function s = any2str(n)
	if (iscellstr(n) || ischar(n))
		s = cellstr(n);
	else
		s = arrayfun(@(x)sprintf('%.16g',x), n, 'uniformoutput',0);
		s(isnan(n)) = {''};
	end
end

function s = sec2str(n)
	if (isnan(n))
		s = '';
	else
		s = [strftime('%d/%m/%Y,%H:%M:%S', localtime(floor(n))) ...
			sprintf('.%06d', round(mod(n,1)*1e6))];
	end
end

function n = toint16(n, type)
	if (strcmp(type, 'logical'))
		n = postpad(logical(n), ceil(columns(n)/16)*16, false, 2);
	else
		n = reshape(bitunpack(cast(n, type)'), [],rows(n))';
	end
	n = reshape(bitpack(n', 'int16'), [],rows(n))';
end

%!demo
%!	function test_diff(cn1,dn1)
%!		cn2 = tempname();
%!		dn2 = tempname();
%!		[ci1,di1] = comtraderead(cn1,dn1);
%!		comtradewrite(cn2,dn2, ci1,di1);
%!		[ci2,di2] = comtraderead(cn2,dn2);
%!		assert(ci1,ci2);
%!		assert(di1,di2);
%!		assert(fileread(dn1),fileread(dn2));
%!		[cm1,cs1] = regexp(fileread(cn1), ',|\r\n', 'split','match');
%!		[cm2,cs2] = regexp(fileread(cn2), ',|\r\n', 'split','match');
%!		assert(cs1,cs2);
%!		str = cellfun(@strcmp, cm1,cm2);
%!		num = arrayfun(@isequaln, str2double(cm1),str2double(cm2));
%!		assert(str|num);
%!		unlink(cn2);
%!		unlink(dn2);
%!	end
%!	[n,p] = uigetfile({'*.cfg;*.dat' 'COMTRADE-Files'},'multiselect','on');
%!	test_diff(fullfile(p,n){:});
