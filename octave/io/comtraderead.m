%% [cfginfo,datinfo] = comtraderead(cfgname,datname)

function [cfginfo,datinfo] = comtraderead(cfgname,datname)

	c = fileread(cfgname);
	c = strsplit(c,sprintf('\r\n'), 'collapsedelimiters',0)(1:end-1)(:);
	c = cellfun(@(s){strsplit(s,',', 'collapsedelimiters',0)}, c);
	pop = @(x,r,c){reshape(ifelse(r,cat(1,x{1:r}),{}),r,c) x(r+1:end)}{:};
	[f,c] = pop(c, 1,3); i = 1;
	cfginfo.station_name =           (f{i++});
	cfginfo.rec_dev_id   =           (f{i++});
	cfginfo.rev_year     = str2double(f{i++});
	[f,c] = pop(c, 1,3); i = 1;
	cfginfo.TT           = str2double(f{i++});
	cfginfo.A            = str2double(f{i++}(1:end-1));
	cfginfo.D            = str2double(f{i++}(1:end-1));
	[f,c] = pop(c, cfginfo.A,13); i = 1;
	cfginfo.An           = str2double(f(:,i++)');
	cfginfo.Ach_id       =           (f(:,i++)');
	cfginfo.Aph          =           (f(:,i++)');
	cfginfo.Accbm        =           (f(:,i++)');
	cfginfo.uu           =           (f(:,i++)');
	cfginfo.a            = str2double(f(:,i++)');
	cfginfo.b            = str2double(f(:,i++)');
	cfginfo.skew         = str2double(f(:,i++)');
	cfginfo.min          = str2double(f(:,i++)');
	cfginfo.max          = str2double(f(:,i++)');
	cfginfo.primary      = str2double(f(:,i++)');
	cfginfo.secondary    = str2double(f(:,i++)');
	cfginfo.PS           =           (f(:,i++)');
	[f,c] = pop(c, cfginfo.D,5); i = 1;
	cfginfo.Dn           = str2double(f(:,i++)');
	cfginfo.Dch_id       =           (f(:,i++)');
	cfginfo.Dph          =           (f(:,i++)');
	cfginfo.Dccbm        =           (f(:,i++)');
	cfginfo.y            = str2double(f(:,i++)');
	[f,c] = pop(c, 1,1); i = 1;
	cfginfo.lf           = str2double(f{i++});
	[f,c] = pop(c, 1,1); i = 1;
	cfginfo.nrates       = str2double(f{i++});
	[f,c] = pop(c, cfginfo.nrates,2); i = 1;
	cfginfo.samp         = str2double(f(:,i++)');
	cfginfo.endsamp      = str2double(f(:,i++)');
	[f,c] = pop(c, 2,2); i = 1;
	cfginfo.start        = str2sec   (strjoin(f(i++,:),','));
	cfginfo.trigger      = str2sec   (strjoin(f(i++,:),','));
	[f,c] = pop(c, 1,1); i = 1;
	cfginfo.ft           =           (f{i++});
	[f,c] = pop(c, 1,1); i = 1;
	cfginfo.timemult     = str2double(f{i++});
	assert(isempty(c));

	switch (cfginfo.ft)
	case {'ASCII','ascii'}
		sz = [1 1 cfginfo.A cfginfo.D];
		n = csvread(datname);
		n = reshape(n, rows(n),sum(sz));
		[
			datinfo.n         ...
			datinfo.timestamp ...
			datinfo.Ak        ...
			datinfo.Dm        ...
			] = mat2cell(n, rows(n),sz){:};
	case {'BINARY','binary'}
		sz = [2 2 cfginfo.A ceil(cfginfo.D/16)];
		[fid, msg] = fopen(datname, 'r');
		if (fid < 0) error(msg); end
		n = fread(fid, inf, '*int16', 0, 'l');
		fclose(fid);
		n = reshape(n, sum(sz),[])';
		[
			datinfo.n         ...
			datinfo.timestamp ...
			datinfo.Ak        ...
			datinfo.Dm        ...
			] = mat2cell(n, rows(n),sz){:};
		datinfo.n         = int16to(datinfo.n        , 'uint32');
		datinfo.timestamp = int16to(datinfo.timestamp, 'uint32');
		datinfo.Ak        = int16to(datinfo.Ak       ,  'int16');
		datinfo.Dm        = int16to(datinfo.Dm, 'logical', cfginfo.D);
	end

end

function n = str2sec(s)
	if (isempty(s))
		n = nan;
	else
		n = mktime(strptime(s(1:end-7), '%d/%m/%Y,%H:%M:%S')) ...
			+ str2double(s(end-6:end));
	end
end

function n = int16to(n, type, d)
	n = reshape(bitunpack(n'), [],rows(n))';
	if (strcmp(type, 'logical'))
		n = double(n(:,1:d));
	else
		n = double(reshape(bitpack(n', type), [],rows(n))');
	end
end

%!demo
%!	function test_read(cn,dn)
%!		[ci,di] = comtraderead(cn,dn);
%!		disp(ci);
%!		disp(structfun(@size, di, 'uniformoutput',0));
%!		close all;
%!		if (numel(di.n) || numel(di.timestamp))
%!			figure; h = arrayfun(@subplot,211:212);
%!			plot(h(1),di.n); ylabel n; grid on;
%!			plot(h(2),di.timestamp); ylabel timestamp; grid on;
%!		end
%!		i = 0; for y = di.Ak i++; j = mod(i-1,4)+1;
%!			if (j == 1) figure; end; subplot(4,1,j);
%!			plot(y); ylabel(num2str(i,'A%d')); grid on;
%!		end
%!		i = 0; for y = di.Dm i++; j = mod(i-1,8)+1;
%!			if (j == 1) figure; end; subplot(8,1,j);
%!			plot(y); ylabel(num2str(i,'D%d')); grid on;
%!			ylim([-.5 1.5]); yticks(0:1);
%!		end
%!	end
%!	[n,p] = uigetfile({'*.cfg;*.dat' 'COMTRADE-Files'},'multiselect','on');
%!	test_read(fullfile(p,n){:});
