%% tdisp(x)

function tdisp(x)
	persistent head = nthargout(2,@system,'tput ed');
	persistent tail = nthargout(2,@system,'tput home');
	puts([head disp(x) tail]);
end

%!demo
%!	x = y = 0;
%!	clc();
%!	while (1)
%!		buf(1:22, 1:66) = ' ';
%!		buf(11+y, 33+x*3-3) = '<';
%!		buf(11+y, 33+x*3+3) = '>';
%!		buf(11+y-1, 33+x*3) = '^';
%!		buf(11+y+1, 33+x*3) = 'v';
%!		tdisp(buf);
%!		pause(.05);
%!		switch (kbhit(1))
%!		case char(27) break
%!		case 'h' x -= 4;
%!		case 'j' y += 4;
%!		case 'k' y -= 4;
%!		case 'l' x += 4;
%!		end
%!		x -= sign(x);
%!		y -= sign(y);
%!	end
%!	tdisp('');
