%% L = spinplus(L1, L2)
%%	compose two angular momentum matrices

function l = spinplus(l1, l2)
	n1 = length(l1);
	n2 = length(l2);
	p = bincoeff(n1-1,0:n1-1) .* bincoeff(n2-1,0:n2-1)';
	p = full(sparse(1:n1*n2, (1:n1)+(1:n2)'-1, p));
	p = sqrt(p ./ sum(p));
	l = kron(l1, eye(n2)) + kron(eye(n1), l2);
	l = p' * l * p;
end

%!shared tol,n
%!	tol = eps^.5;
%!	n = 4;
%!test
%!	for na = 1:n a = randn(na);
%!	for nb = 1:n b = randn(nb);
%!		assert(spinplus(a,b), spinplus(b,a), tol);
%!	for nc = 1:n c = randn(nc);
%!		assert(spinplus(spinplus(a,b),c), spinplus(a,spinplus(b,c)), tol);
%!	end
%!	end
%!	end
