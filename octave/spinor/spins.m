%% L = spins(s)
%%	spin-s matrices

function l = spins(s=1/2)
	a = num2cell(spin/2,1:2)(:);
	l = {0}(ones(size(a)));
	for i = ones(1,s*2)
		l = cellfun(@spinplus,l,a,'uniformoutput',0);
	end
	l = cat(3,l{:});
end

%!test
%!	for s = 0:.5:2
%!		[x,y,z] = num2cell(spins(s),1:2){:};
%!		assert(cellfun(@ishermitian,{x y z}));
%!		assert(eig(x), (-s:s)', eps^.5);
%!		assert(eig(y), (-s:s)', eps^.5);
%!		assert(eig(z), (-s:s)', eps^.5);
%!		assert(x*y-y*x, 1j*z, eps^.5);
%!		assert(y*z-z*y, 1j*x, eps^.5);
%!		assert(z*x-x*z, 1j*y, eps^.5);
%!		assert(x*x+y*y+z*z, s*(s+1)*eye(s*2+1), eps^.5);
%!		assert(z, diag(s:-1:-s), eps^.5);
%!	end

%!test
%!	l = vertcat(num2cell(spins1,1:2){:});
%!	for s = 0:.5:2
%!		s = vertcat(num2cell(spins(s),1:2){:});
%!	for b = randn(3,9)*2
%!		q = expm(-1j * kron(b', eye(columns(l))) * l);
%!		u = expm(-1j * kron(b', eye(columns(s))) * s);
%!		assert(kron(q, u) * s / u, s, eps^.5);
%!	end
%!	end
