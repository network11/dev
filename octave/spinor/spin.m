%% sigma = spin(n)
%%	n-dimensional spin matrices

function [l, h,g] = spin(n=3, m=0)
	x = [0 1;1 0]; y = [0 -1j;1j 0]; z = [1 0;0 -1]; t = eye(2);
	assert(mod([m n],2), 0:1);
	l = {x y z t}(1+cumsum(abs((1:n)'-(n:-2:3))<=1)); l = reshape(l,n,[]);
	l = cellfun(@kron,{1},{1},num2cell(l,1){:},'uniformoutput',0);
	g = diag([-1 1](1+(1:n>m)));
	h = kron(1,1,{t z}{1+(n-1:-2:2==m)});
	l(1:m) = cellfun(@mtimes,{h},l(1:m),'uniformoutput',0);
	l = cat(3,l{:});
end

%!test
%!	[x,y,z] = num2cell(spin,1:2){:};
%!	assert(cellfun(@ishermitian,{x y z}));
%!	assert(isequal(eig(x), eig(y), eig(z), [-1;1]));
%!	assert(isequal(x*y, -y*x, 1j*z));
%!	assert(isequal(y*z, -z*y, 1j*x));
%!	assert(isequal(z*x, -x*z, 1j*y));
%!	assert(isequal(x*x, y*y, z*z, eye(2)));
%!	assert(kron(x,x)+kron(y,y)+kron(z,z), blkdiag(1,[-1 2;2 -1],1));

%!test
%!	for i = randi(9,6,30)
%!		a = randn(i(1:2)); c = randn(i(2:3));
%!		b = randn(i(4:5)); d = randn(i(5:6));
%!		assert(kron(a,b)*kron(c,d), kron(a*c,b*d), eps^.5);
%!		assert(kron(a,b)', kron(a',b'));
%!	end

%!test
%!	for n = 1:2:9 for m = 0:2:n k = 2^((n-1)/2);
%!		[s,h,g] = spin(n,m);
%!		s = num2cell(s,1:2); r = horzcat(s{:}); c = vertcat(s{:});
%!		assert(isequal(g, g', inv(g)));
%!		assert(isequal(h, h', inv(h)));
%!		x = mtimes(1,s{:}); assert(x/x(1), eye(k));
%!		x = mtimes(1,h,s{1:m}); assert(x/x(1), eye(k));
%!		x = c * r; x += cell2mat(mat2cell(x,{k*ones(1,n)}{[1 1]})');
%!		assert(kron(g, eye(k)) * 2, x);
%!		assert(kron(g, eye(k)) * c, r');
%!		assert(kron(eye(n), h) * c * h, r');
%!		w = randn(n); w -= w';
%!		q = expm(g * w);
%!		u = expm(r * kron(w, eye(k)) * c / 4);
%!		assert(kron(q, u) * c / u, c, eps^.5);
%!		assert(q' * g * q, g, eps^.5);
%!		assert(u' * h * u, h, eps^.5);
%!	end end
