%% [v,lambda] = spineig(a,b, opt)
%%	a*v == v*lambda, isdefinite(b), a == b\a'*b
%%	if opt == 'vector': v^-1 == v'*b, lambda == lambda', isdiag(lambda)
%%	if opt == 'matrix': v^-1 == b\v'*b, lambda == b\lambda'*b

function [v,l] = spineig(a,b, opt='vector')
	b = sqrtm(b);
	[v,l] = eig(b*a/b);
	switch (opt)
	case 'vector'	v = b\v;
	case 'matrix'	v = b\v*b; l = b\l*b;
	end
end

%!test
%!	tol = -eps^.5;
%!	for n = 1:9 for i = 1:9
%!		a = randn(n)+1j*randn(n);
%!		b = randn(n)+1j*randn(n); b = b*b';
%!		assert(isdefinite(b));
%!		[v,l] = spineig(a,b,'vector');
%!		assert(a*v, v*l, tol); assert(isdiag(l));
%!		[v,l] = spineig(a,b,'matrix');
%!		assert(a*v, v*l, tol);
%!		a = (a+a')*b;
%!		assert(b\a'*b, a, tol);
%!		[v,l] = spineig(a,b,'vector');
%!		assert(v'*b*v, eye(n), tol); assert(l', l, tol);
%!		[v,l] = spineig(a,b,'matrix');
%!		assert(b\v'*b*v, eye(n), tol); assert(b\l'*b, l, tol);
%!	end end
