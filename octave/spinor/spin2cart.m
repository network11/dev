%% c = spin2cart(s)
%%	c = [lx ly lz]
%%	s = [psi+ psi-]

function c = spin2cart(s)
	r = sqrt(sumsq(s,2));
	z = s(:,2)./s(:,1); theta = 2*atan(abs(z)); phi = arg(z);
	theta = pi/2-theta; [c(:,1),c(:,2),c(:,3)] = sph2cart(phi,theta,r);
end

%!test
%!	tol = eps^.5;
%!	for n = 1:9
%!		s = randn(n,2)+1j*randn(n,2);
%!		s1 = cart2spin(spin2cart(s));
%!		assert(s1(:,2)./s1(:,1), s(:,2)./s(:,1), -tol);
%!	end
