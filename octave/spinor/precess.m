%% [x, ist,msg] = precess(b, x_0, t, ...)
%%	spinor precession under magnetic field
%%	x: spinor [psi+,psi-] or [lx,ly,lz]
%%	b: magnetic field function @(t)[bx,by,bz]
%%	t: time

function [varargout] = precess(b, x_0, varargin)
	switch (numel(x_0))
	case 2	[varargout{1:nargout}] = precesss(b, x_0, varargin{:});
	case 3	[varargout{1:nargout}] = precessv(b, x_0, varargin{:});
	end
end

function [varargout] = precesss(b, x_0, t, varargin)
	m = .5j*spin;
	fcn = @(s,t) sum(m.*vec(b(t),3),3)*s(:);
	[varargout{1:nargout}] = clsode(fcn, x_0, t, varargin{:});
end

function [varargout] = precessv(b, x_0, t, varargin)
	fcn = @(l,t) cross(l(:),b(t)(:));
	[varargout{1:nargout}] = lsode(fcn, x_0, t, varargin{:});
end

%!test
%!	t = sort(rand(1,9)*10);
%!	[a,w,p] = num2cell(randn(3,3),2){:}; b = @(t)a.*cos(w.*t+p);
%!	s0 = randn(1,2)+1j*randn(1,2); c0 = spin2cart(s0);
%!	[s,i] = precess(b,s0,t); assert(i,2);
%!	[c,i] = precess(b,c0,t); assert(i,2);
%!	assert(c, spin2cart(s), 1e-5);

%!demo
%!	h = plot3({eye(2)}{[1 1 1]}); grid on; axis([-1 1 -1 1 -1 1]*3);
%!	t = -5:.05:5;
%!	[a,b] = num2cell(randn(2,3),2){:}; b = @(t)a+b.*tanh(t);
%!	[c,i] = precess(b,randn(1,3),t); assert(i,2);
%!	b = b(t(:)); for i = 1:numel(t)
%!		set(h(1),{'xdata','ydata','zdata'},num2cell([0;1]*b(i,:),1));
%!		set(h(2),{'xdata','ydata','zdata'},num2cell([0;1]*c(i,:),1));
%!		drawnow;
%!	end

%!demo
%!	h = plot3({eye(2)}{[1 1 1]}); grid on; axis([-1 1 -1 1 -1 1]*3);
%!	t = 2*pi*(0:.02:10);
%!	for b = {
%!		@(t)[.2*[0*t      0*t     ] 0*t+1]
%!		@(t)[.2*[0*t      0*t+1   ] 0*t+1]
%!		@(t)[.2*[0*t      sin(1*t)] 0*t+1]
%!		@(t)[.2*[0*t      cos(1*t)] 0*t+1]
%!		@(t)[.2*[cos(1*t) sin(1*t)] 0*t+1]
%!		@(t)[.2*[sin(1*t) cos(1*t)] 0*t+1]
%!		@(t)[.2*[0*t      sin(2*t)] 0*t+1]
%!		@(t)[.2*[0*t      cos(2*t)] 0*t+1]
%!		@(t)[.2*[cos(2*t) sin(2*t)] 0*t+1]
%!		@(t)[.2*[sin(2*t) cos(2*t)] 0*t+1]
%!		}' b = b{1};
%!	[c,i] = precess(b,[0 0 1],t); assert(i,2);
%!	b = b(t(:)); for i = 1:numel(t)
%!		set(h(1),{'xdata','ydata','zdata'},num2cell([0;1]*b(i,:),1));
%!		set(h(2),{'xdata','ydata','zdata'},num2cell([0;1]*c(i,:),1));
%!		drawnow;
%!	end
%!	end
