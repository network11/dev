%% L = spins1
%%	angular momentum matrices

function l = spins1
	x = [0 0 0;0 0 -1j;0 1j 0];
	y = [0 0 1j;0 0 0;-1j 0 0];
	z = [0 -1j 0;1j 0 0;0 0 0];
	l = cat(3,x,y,z);
end

%!test
%!	[x,y,z] = num2cell(spins1,1:2){:};
%!	assert(cellfun(@ishermitian,{x y z}));
%!	assert(isequal(eig(x), eig(y), eig(z), (-1:1)'));
%!	assert(x*y-y*x, 1j*z);
%!	assert(y*z-z*y, 1j*x);
%!	assert(z*x-x*z, 1j*y);
%!	assert(x*x+y*y+z*z, 2*eye(3));

%!test
%!	l = vertcat(num2cell(spins1,1:2){:});
%!	for b = randn(3,9)*2
%!		q = expm(-1j * kron(b', eye(columns(l))) * l);
%!		assert(kron(q, q) * l / q, l, eps^.5);
%!	end

%!test
%!	l = vertcat(num2cell(spins1,1:2){:});
%!	s = vertcat(num2cell(spins(1),1:2){:});
%!	p = [-1 0 1; -1j 0 -1j; 0 2^.5 0] / 2^.5;
%!	assert(kron(eye(3), p) * s / p, l, eps^.5);
