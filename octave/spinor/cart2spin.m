%% s = cart2spin(c)
%%	c = [lx ly lz]
%%	s = [psi+ psi-]

function s = cart2spin(c)
	[phi,theta,r] = cart2sph(c); theta = pi/2-theta;
	s = [cos(.5*theta).*exp(-.5j*phi) sin(.5*theta).*exp(.5j*phi)].*r;
end

%!test
%!	tol = eps^.5;
%!	for n = 1:9
%!		c = randn(n,3); p = exp(1j*randn(n,1));
%!		c1 = spin2cart(cart2spin(c).*p);
%!		assert(c1, c, tol);
%!	end

%!demo
%!	h = plot3({eye(4)}{[1 1 1]}); grid on; axis([-1 1 -1 1 -1 1]);
%!	legend('\psi_a','\psi_b','\psi_R','\psi_L');
%!	s = spin()(:,:,3); p = kron([1 1;1 -1],eye(2)/sqrt(2));
%!	a = randn(2,1); b = s*a;
%!	for t = tanh(atanh(0:.01:1-eps)/2)
%!		ab = [a;t*b]; ab = ab/sqrt(sumsq(ab)); rl = p*ab;
%!		c = spin2cart(reshape([ab;rl],2,[]).');
%!	for i = 1:numel(h)
%!		set(h(i),{'xdata','ydata','zdata'},num2cell([0;1]*c(i,:),1));
%!	end
%!		drawnow;
%!	end
