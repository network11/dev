%% [r_theta_rdot_thetadot, ist,msg] = schwarz(r_theta_rdot_thetadot_0, tau)
%%	motion under spherical symmetric gravity
%%	-g_tt = 1/g_rr = 1-2/r

function [rpvw, ist,msg] = schwarz(rpvw, t, m=1)
	[r,p,v,w] = num2cell(rpvw){:};
	l = w*r^2;
	rpv = [r p v];
	[rpv,ist,msg] = lsode(@(rpv,~)f(rpv,l,m), rpv, t);
	[r,p,v] = num2cell(rpv,1){:};
	w = l./r.^2;
	rpvw = [r p v w];
end

function vwa = f(rpv, l,m)
	[r,p,v] = num2cell(rpv){:};
	w = l/r^2;
	a = - m^2/r^2 + l^2/r^3 - 3*l^2/r^4;
	vwa = [v w a];
end

%!xtest
%!	tol = 1e-6;
%!	t = sort(rand(1,10)*3); i = randi(numel(t));
%!	r0 = 10.^(rand+1); p0 = randn;
%!	[v0,w0] = pol2cart((rand-.5)*5, rand*.1); w0 /= r0;
%!	x0 = [r0 p0 v0 w0];
%!	[x3,ist,msg] = schwarz(x0,t); assert(ist,2);
%!	[x1,ist,msg] = schwarz(x0,t(1:i)); assert(ist,2);
%!	[x2,ist,msg] = schwarz(x1(end,:),t(i:end)); assert(ist,2);
%!	assert(x3(1,:), x0, tol);
%!	assert(x3(1:i,:), x1, tol);
%!	assert(x3(i:end,:), x2, tol);
%!	[r,p,v,w] = num2cell(x3,1){:};
%!	l = w.*r.^2;
%!	e = sqrt(v.^2 + (1+l.^2./r.^2) .* (1-2./r));
%!	assert(range(l), 0, tol);
%!	assert(range(e), 0, tol);

%!xtest
%!	tol = 1e-6;
%!	t = sort(rand(1,10)*3);
%!	r0 = 10.^(rand+1); p0 = randn;
%!	[v0,w0] = pol2cart((rand-.5)*5, rand+.5); w0 /= r0;
%!	x0 = [r0 p0 v0 w0];
%!	[x1,ist,msg] = schwarz(x0,t,0); assert(ist,2);
%!	a = rande; b = [1 1 a a];
%!	[x2,ist,msg] = schwarz(x0.*b,t/a,0); x2./=b; assert(ist,2);
%!	a = 10.^(rand+3); b = [1 1 a a];
%!	[x3,ist,msg] = schwarz(x0.*b,t/a,1); x3./=b; assert(ist,2);
%!	assert(x2, x1, tol);
%!	assert(x3, x1, tol);
%!	[r,p,v,w] = num2cell(x1,1){:};
%!	l = w.*r.^2;
%!	e = sqrt(v.^2 + (l.^2./r.^2) .* (1-2./r));
%!	assert(range(l), 0, tol);
%!	assert(range(e), 0, tol);

%!demo
%!	r = p = [];
%!	l2 = 1e4; for e = [0 .5 1 2]
%!		r0 = l2/(1+e); w0 = l2^.5/r0^2;
%!		x0 = [r0 0 0 w0]; t = linspace(0,25/w0);
%!		[x,ist] = schwarz(x0,t); assert(ist,2);
%!		[r(:,end+1),p(:,end+1)] = num2cell(x,1){:};
%!	end
%!	polar(p,r);
%!demo
%!	r = p = [];
%!	l2 = 1e4; for e = secd(15:15:75)
%!		r0 = l2/(1+e); w0 = l2^.5/r0^2; v0 = sqrt((e^2-1)/l2);
%!		x0 = [r0 0 0 w0]; t = linspace(0,50*l2/v0);
%!		[x,ist] = schwarz(x0,t); assert(ist,2);
%!		[r(:,end+1),p(:,end+1)] = num2cell(x,1){:};
%!	end
%!	polar(p,r);

%!demo
%!	r = p = [];
%!	for r0 = [60./(1:3) 900] for r1 = r0*(1+(0:2)/20)
%!		l2 = r0^2/(r0-3); w0 = l2^.5/r0^2; w1 = l2^.5/r1^2;
%!		x0 = [r1 0 0 w1]; t = linspace(0,(1+3/r0)*10*pi/w0);
%!		[x,ist] = schwarz(x0,t); assert(ist,2);
%!		[r(:,end+1),p(:,end+1)] = num2cell(x,1){:};
%!	end end
%!	semilogy(p,r); grid on; xticks(2*pi*(0:5));

%!demo
%!	r = p = [];
%!	for r0 = [40./(1:3) 1e3]
%!		w0 = 1e3/r0; x0 = [r0 2/r0 0 -w0]; t = logspace(-3,3);
%!		[x,ist] = schwarz(x0,t); assert(ist,2);
%!		[r(:,end+1),p(:,end+1)] = num2cell(x,1){:};
%!	end
%!	semilogy(p,r); grid on; xticks([-pi/2 (0:2)/10]);
