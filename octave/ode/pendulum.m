%% [x,v, ist,msg] = pendulum(g, x_0,v_0, t)
%%	pendulum motion
%%	a = -g(t)*sin(x)

function [x,v, ist,msg] = pendulum(g, x_0,v_0, t)
	xv_0 = [x_0 v_0];
	fcn = @(xv,t) [xv(2) -g(t)*sin(xv(1))];
	[xv, ist,msg] = lsode(fcn, xv_0, t);
	[x,v] = deal(num2cell(xv,1){:});
end

%!demo
%!	t = linspace(0,8*pi);
%!	g = @(t)1;
%!	y = []; for x0 = deg2rad(0:15:180)
%!		[x,~,ist] = pendulum(g,x0,0,t); assert(ist==2);
%!		y = [y x];
%!	end
%!	plot(2*pi\t,pi\y); grid on; xlabel 't/2\pi'; ylabel 'x/\pi';

%!demo
%!	t = linspace(0,8*pi,1e3);
%!	g = @(t)1+20*cos(10*t);
%!	y = []; for x0 = deg2rad(0:15:180)
%!		[x,~,ist] = pendulum(g,x0,0,t); assert(ist==2);
%!		y = [y x];
%!	end
%!	plot(2*pi\t,pi\y); grid on; xlabel 't/2\pi'; ylabel 'x/\pi';

%!xtest
%!	% ellipsoid
%!	[a,b,c] = num2cell(rand(3,1)+1){:};
%!	o = [];
%!	for d = rand(3,9)-.5
%!		[x,y,z] = num2cell(d){:};
%!		s2 = @(t) -log(hypot(b*(y-sin(t)),a*(x-cos(t))));
%!		p2 = @(t) s2(t).*cos(t);
%!		s3 = @(t,p) sin(t)./hypot(c*(z-cos(t)),b*(y-sin(t).*sin(p)),a*(x-sin(t).*cos(p)));
%!		p3 = @(t,p) s3(t,p).*cos(t);
%!		o(end+1,:) = [
%!			quad(s2, 0,2*pi)
%!			quad(p2, 0,2*pi)./x
%!			quad2d(s3, 0,pi,0,2*pi)
%!			quad2d(p3, 0,pi,0,2*pi)./z
%!			];
%!	end
%!	o = range(o); assert(o,0*o,eps^.5);
