%% [r,v, ist,msg] = coulomb(rp, r_0,v_0, t)
%%	plasma motion under electrostatic force
%%	rp: proton postion [xp,yp,zp]
%%	r: electron postion [x,y,z]
%%	v: electron velocity [vx,vy,vz]
%%	t: time

function [r,v, ist,msg] = coulomb(rp, r,v, t)
	assert(size_equal(rp,r,v));
	x_0 = [r(:);v(:)];
	fcn = @(x,~)[x(end/2+1:end);f(x(1:end/2),rp)];
	[x, ist,msg] = lsode(fcn, x_0, t);
	r = reshape(x(:,1:end/2)', [size(r) numel(t)]);
	v = reshape(x(:,end/2+1:end)', [size(v) numel(t)]);
end

function a = f(x, p)
	n = columns(p);
	Rd = reshape(x, [],1,n);
	Rs = reshape(x, 1,[],n);
	R = Rd-Rs;
	r = sqrt(sumsq(R, 3));
	r = max(.5,r);
	A = sum(R./r.^3, 2);
	Rs = reshape(p, 1,[],n);
	R = Rd-Rs;
	r = sqrt(sumsq(R, 3));
	r = max(.5,r);
	A -= sum(R./r.^3, 2);
	a = reshape(A, size(x));
end

%!demo
%!	t = 0:.1:20;
%!	for i = 1:6
%!		r = center(1:i)';
%!		[r,~,ist] = coulomb(r,r+rand(size(r))-.5,r*0,t); assert(ist==2);
%!		subplot(2,3,i); plot(t,squeeze(r)); grid on;
%!	end

%!demo
%!	[x,y] = ndgrid(1:3,1:2); r = [x(:) y(:)]; t = 0:.1:10;
%!	[r,~,ist] = coulomb(r,r+rand(size(r))-.5,r*0,t); assert(ist==2);
%!	h(1) = plot(r(:,1,:)(:),r(:,2,:)(:),'o'); axis manual; grid on;
%!	h(2) = title(0);
%!	for i = 1:numel(t)
%!		set(h(1),'xdata',r(:,1,i),'ydata',r(:,2,i));
%!		set(h(2),'string',t(i)); drawnow; pause(.1);
%!	end
