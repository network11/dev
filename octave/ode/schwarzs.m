%% [phi,psi,p,rho, ist,msg] = schwarzs(fcn, p_0, r)
%%	spherical symmetric stellar structure
%%	rho = fcn(p)
%%	g_tt = -phi
%%	g_rr = 1/psi

function [phi,psi,p,rho, ist,msg] = schwarzs(fcn, p_0, r)
	[x, ist,msg] = lsode(@(x,r)f(x,r,fcn), [1 1 p_0], r);
	[phi,psi,p] = deal(num2cell(x,1){:});
	phi /= phi(end);
	rho = fcn(p);
end

function xd = f(x, r, fcn)
	[phi,psi,p] = deal(num2cell(x){:});
	rho = fcn(p);
	psi_d = (1-psi)/r - rho*r;
	phi_d = ((1-psi)/r + p*r) * (phi/psi);
	p_d = -.5*(p+rho)/phi * phi_d;
	xd = reshape([phi_d psi_d p_d], size(x));
end

%!demo
%!	d0 = 1; p0 = 1; f = @(p)d0*(p>0); r = logspace(-3,3,121);
%!	[a,b,c,d,ist,msg] = schwarzs(f,p0,r); assert(ist==2);
%!	semilogx(r,[a b c d]); grid on; legend '\phi' '\psi' p '\rho';

%!demo
%!	d0 = 1; p0 = d0/3e2; f = @(p)d0/p0*p; r = logspace(-3,3,121);
%!	[a,b,c,d,ist,msg] = schwarzs(f,p0,r); assert(ist==2);
%!	semilogx(r,[a b c d]); grid on; legend '\phi' '\psi' p '\rho';
%!demo
%!	d0 = 1; p0 = d0/3; f = @(p)d0/p0*p; r = logspace(-3,3,121);
%!	[a,b,c,d,ist,msg] = schwarzs(f,p0,r); assert(ist==2);
%!	semilogx(r,[a b c d]); grid on; legend '\phi' '\psi' p '\rho';
