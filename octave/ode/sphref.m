%% theta = sphref(b, n, c)
%%	unit-radius spherical reflection/refraction
%%	theta: deflection angle
%%	b: impact parameter
%%	n: refractive index
%%	c: reflection/refraction count

function theta = sphref(b, n, c)
	i = asin(b);
	r = asin(b./n);
	theta = (r-i).*2 + (r+r-pi).*(c-2);
end

%!test
%!	b = rand(9,1)*2-1; n = 1+rande; c = 1:6; t = 2*acos(b);
%!	assert(sphref(b,n,1), t, eps^.5);
%!	assert(sphref(b,1,c), t.*(2-c), eps^.5);
%!	assert(sphref(0,n,c), pi*(2-c), eps^.5);

%!demo
%!	b = (-1:1e-2:1)'; n = secd(0:18:90);
%!	for c = 4:-1:1 subplot(2,2,c);
%!		t = sphref(b,n,c); t = rad2deg(t);
%!		plot(t,b); xticks(-720:90:720); grid on;
%!	title(c); end; legend(num2str(n(:)));

%!demo
%!	n = secd(0:10:50); l = [2 8 8 4];
%!	for c = 4:-1:1 subplot(2,2,c);
%!		[s,t] = section(@(b)sphref(b,n,c),1,1e5,180); t = rad2deg(t);
%!		plot(t,s); axis([0 180 0 l(c)]); xticks(0:30:180); grid on;
%!	title(c); end; legend(num2str(n(:)));

%!demo
%!	p = 2*pi*rand(500,1); b = sqrt(rand(size(p))); n = 1.333;
%!	for c = 4:-1:1 subplot(2,2,c);
%!		t = sphref(b,n,c); [x,y,z] = sph2cart(p,pi/2-t,1);
%!		plot3(x,y,z,'.'); axis([-1 1 -1 1 -1 1],'equal'); grid on;
%!	title(c); end; legend(num2str(n(:)));

%!demo
%!	n = 1.333+.005*(-1:1); c = vec(1:4,3);
%!	[s,t] = section(@(b)sphref(b,n,c),1,1e5,180); t = rad2deg(t);
%!	s /= max(s(:)); s = shiftdim(s,2); s = repelem(s,10,1);
%!	imshow(s,'xdata',t); axis ticx on; xticks(0:30:180); grid on;
