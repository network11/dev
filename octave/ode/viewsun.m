%% [azimuth, elevation] = viewsun(time, date, latitude, obliquity)
%%	compute solar angles from
%%	local time since midnight,
%%	date since winter solstice,
%%	north latitude,
%%	ecliptic obliquity (default to 23.5 degree)

function [azimuth, elevation] = viewsun(time, date, latitude, obliquity=deg2rad(23.5))
	[north, east, top] = sph2cart(time, ssp(date, obliquity), 1);
	[top, north] = rot(top, north, pi/2-latitude);
	[azimuth, elevation] = cart2sph(north, east, top);
end

function [x, y] = rot(x, y, rotate)
	[theta, r] = cart2pol(x, y);
	theta = theta + rotate;
	[x, y] = pol2cart(theta, r);
end

%%	subsolar point
function point = ssp(date, obliquity)
	[solar.x, solar.y, solar.z] = sph2cart(date, 0, 1);
	[north.x, north.y, north.z] = sph2cart(pi, pi/2-obliquity, 1);
	point = asin(solar.x.*north.x + solar.y.*north.y + solar.z.*north.z);
end

%!demo
%!	t = 0:2:23; d = [0 3 6 8 10]; l = deg2rad(40);
%!	[a,e] = viewsun(nthargout(1:2,@ndgrid,pi/12*t,pi/6*d){:},l);
%!	[n,e,t] = sph2cart(a,e,1);
%!	plot3(e,n,t,'-o'); grid on; legend(num2str(d(:),'date=%d'));
%!	text([1 -1 0 0 0 0],[0 0 1 -1 0 0],[0 0 0 0 1 -1],cellstr('EWNSTB'.'));
%!	axis([-1 1 -1 1 -1 1],'equal'); view(270,0);

%!demo
%!	t = linspace(0,24); d = 0:6; l = deg2rad(40);
%!	[a,e] = viewsun(nthargout(1:2,@ndgrid,pi/12*t,pi/6*d){:},l);
%!	l = {num2str(d(:),'date=%d'),'location','northeastoutside'};
%!	subplot(211); plot(t,rad2deg(unwrap(a))); grid on; legend(l{:});
%!	yticks(0:30:360); ylim([0 360]); ylabel azimuth;
%!	xticks(0:2:24); xlim([0 24]); xlabel time;
%!	subplot(212); plot(t,rad2deg(unwrap(e))); grid on; legend(l{:});
%!	yticks(-90:30:90); ylim([-90 90]); ylabel elevation;
%!	xticks(0:2:24); xlim([0 24]); xlabel time;

%!demo
%!	planet = @(t,r) r.*exp(2j*pi*r.^-1.5.*t);
%!	t = 0:.01:30;
%!	r = [0.387 0.723 1.524 5.205 9.576]';
%!	z = planet(t,r)-planet(t,1);
%!	plot(z.'); grid on; axis equal;
%!	legend Mercury Venus Mars Jupiter Saturn;

%!demo
%!	% Lagrangian point
%!	[x,y] = meshgrid(-2:.02:2); a = vec([0 .1 .3 .5],3); l = -2.2:.02:0;
%!	p = (a-1)./hypot(a+x,y)-a./hypot(a-1+x,y)-.5*hypot(x,y).^2;
%!	z = exp(1j*pi/3); z = [0 1;z z']'-a;
%!	for i = 1:numel(a) subplot(2,2,i);
%!		contour(x,y,p(:,:,i),l); grid on; axis equal;
%!		hold on; plot(z(:,:,i),'+'); hold off;
%!	end

%!demo
%!	% black drop effect
%!	s = 10; r = 50;
%!	c = sqrt(max(0,s^2-(-s:s).^2-(-s:s)'.^2)); c /= sum(c(:));
%!	b = hypot(-r:r+20,(-r-s/2:r+s/2)')>r+.5;
%!	b = vertcat(arrayfun(@(n){circshift(b,n,2)},1:3:13){:});
%!	b = [true(s,columns(b));b;true(s,columns(b))];
%!	b = [false(rows(b),r+s) b true(rows(b),s)];
%!	subplot(131); imshow(conv2(b,c==max(c(:)),'valid'));
%!	subplot(132); imshow(d=conv2(b,c,'valid'));
%!	subplot(133); imshow(d>.5);
