%% [r,v, ist,msg] = lorentz(e,b, r_0,v_0, t)
%%	motion under electromagnetic field
%%	r: postion [x,y,z]
%%	v: velocity [vx,vy,vz]
%%	e: electric field function @(r)[ex,ey,ez]
%%	b: magnetic field function @(r)[bx,by,bz]
%%	t: time

function [r,v, ist,msg] = lorentz(e,b, r_0,v_0, t)
	x_0 = [r_0 v_0];
	fcn = @(x,~) [x(4:6)' e(x(1:3)')+cross(x(4:6)',b(x(1:3)'))];
	[x, ist,msg] = lsode(fcn, x_0, t);
	[r,v] = deal(mat2cell(x, rows(x),[3 3]){:});
end

%!demo
%!	e = @(r) [0 0 0];
%!	b = @(r) (3*r(3)*r - [0 0 1]*sumsq(r)) / (4*pi*sumsq(r)^2.5);
%!	t = 0:300;
%!	for p = linspace(-pi/3,pi/3,9)
%!		[r0(1),r0(2),r0(3)] = sph2cart(0,p,1);
%!		v0 = -.03*norm(b(r0))/norm(r0)*r0;
%!		[r,v,ist,msg] = lorentz(e,b,r0,v0,t); assert(ist==2);
%!		plot3(num2cell(r,1){:},'b'); hold on;
%!		plot3(num2cell(r0){:},'.r');
%!	end
%!	plot3(0,0,0,'o'); hold off; grid on; axis equal;

%!demo
%!	v0 = .1; tm = 300;
%!	b0 = 2*pi*6/tm; e0 = b0*v0;
%!	r = lorentz(@(~)[0 e0 0],@(~)[0 0 b0],[0 0 0],[v0 1 0],0:tm);
%!	assert(r(:,3)==0);
%!	plot(r(:,1),r(:,2)); grid on; axis equal;
