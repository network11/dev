%% [v,lambda] = schrod(a,k,b)
%%	compute the eigenvectors and the eigenvalues of wave equation
%%	a v - diff k diff v = lambda b v

function [v,l] = schrod(a,k,b)
	a = sparse(diag(a));
	k = sparse(diag(k)); k -= circshift(k,1,1); k -= circshift(k,1,2);
	b = sparse(diag(b));
	[v,l] = eig(a+k,b,'vector');
end

%!test
%!	tol = eps^.5;
%!	for n = 1:8
%!		a = randn(n)+1j*randn(n);
%!		b = randn(n)+1j*randn(n); b = b*b'; assert(isdefinite(b));
%!		[v,l] = eig(a,b);
%!		assert(b*v*l, a*v, tol);
%!		assert(isdiag(l));
%!		a = randn(n)+1j*randn(n); a = a+a'; assert(ishermitian(a));
%!		[v,l] = eig(a,b);
%!		assert(v'*b*v, eye(n), tol);
%!		assert(isreal(l));
%!		a = randn(n)+1j*randn(n); a = a*a'; assert(isdefinite(a));
%!		[v,l] = eig(a,b);
%!		assert(v'*b*v, eye(n), tol);
%!		assert(l, abs(l));
%!	end

%!demo
%!	n = 8; a = zeros(n,3); k = b = a+1; k(end,2) = 0; b(end,3) = 1e3;
%!	m = columns(a); for i = 1:m
%!		[v,l] = schrod(a(:,i),k(:,i),b(:,i));
%!		subplot(3,m,i+[0 m]); plot(v+(1:n)); grid on; yticks(1:n);
%!		subplot(3,m,i+m*2); plot(l,'o-'); grid on; ylim([0 4]);
%!	end

%!demo
%!	n = 100; k = b = ones(n,3); k(:,1) = n; k(:,2) = (n/2/pi)^2;
%!	a(:,1) = ceil(center(1:n)).^2/n; a(end,1:2) = 1e6; a(1,3) = -2;
%!	m = columns(a); for i = 1:m
%!		[v,l] = schrod(a(:,i),k(:,i),b(:,i));
%!		subplot(2,m,i); plot(v(:,1:5)+(1:5)); grid on; ylim([0 6]);
%!		subplot(2,m,i+m); plot(l(1:5),'o-'); grid on;
%!	end

%!demo
%!	n = 200; m = [-1;1]; k = b = ones(1,n);
%!	i = 0; for p = [1 10 n/8 n/2]
%!		a = kron(ones(p,1),m,ones(n/2/p,1));
%!		[~,l] = schrod(a,k,b);
%!		subplot(2,2,++i); plot([a l]); grid on; title(p);
%!	end

%!demo
%!	% delta periodic potential
%!	e = [linspace(-10,25,1e4) linspace(5,20,1e4).^2]'; u = -3:3;
%!	k = sqrt(e*2);
%!	p = acos(cos(k)+sin(k)./k.*u); p = ifelse(imag(p),nan,p);
%!	plot(e,p+u*5); grid on;
%!	xlabel E; xticklabels([]); xticks((pi*(0:8)).^2/2); xlim(e([1 end]));
%!	ylabel p; yticklabels([]); yticks(unique(u*5+[0;pi]));
