%% [sigma, theta] = section(fcn, bmax, ntot, nbin)
%%	sigma: differential scattering cross section
%%	theta: scattering angle
%%	fcn: function theta=fcn(b)
%%	bmax: maximum of impact parameter
%%	ntot: number of incidence points
%%	nbin: number of bins

function [sigma, theta] = section(fcn, bmax, ntot, nbin)
	b = bmax*sqrt((.5:ntot)/ntot)';
	theta = pi*((.5:nbin)/nbin)';
	n = hist(acos(cos(fcn(b))),theta);
	sigma = bmax^2/ntot*nbin/(2*pi)./sin(theta).*n;
end

function nn = hist(y,x)
	nn = reshape(builtin('hist',y(:,:),x), [numel(x) size(y)(2:end)]);
end

%!demo
%!	[y,x] = section(@(x)2*acos(x),1,1e5,180); z = ones(size(x))/4;
%!	subplot(211); x = rad2deg(x); plot(x,y,x,z,'--'); grid on;
%!	xticks(0:30:180); axis([0 180 0 2]);
%!	[y,x] = section(@(x)2*acot(x),4,1e5,180); z = csc(x/2).^4/4;
%!	subplot(212); x = rad2deg(x); plot(x,y,x,z,'--'); grid on;
%!	xticks(0:30:180); axis([0 180 0 8]);
