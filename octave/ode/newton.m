%% [r,v, ist,msg] = newton(r_0,v_0, t)
%%	dust motion under gravity
%%	r: postion [x,y,z]
%%	v: velocity [vx,vy,vz]
%%	t: time

function [r,v, ist,msg] = newton(r,v, t)
	assert(size_equal(r,v));
	n = columns(r);
	x_0 = [r(:);v(:)];
	fcn = @(x,~)[x(end/2+1:end);f(x(1:end/2),n)];
	[x, ist,msg] = lsode(fcn, x_0, t);
	r = reshape(x(:,1:end/2)', [size(r) numel(t)]);
	v = reshape(x(:,end/2+1:end)', [size(v) numel(t)]);
end

function a = f(x, n)
	Rd = reshape(x, [],1,n);
	Rs = reshape(x, 1,[],n);
	R = Rd-Rs;
	r = sqrt(sumsq(R, 3));
	r = max(.5,r);
	A = -sum(R./r.^3, 2);
	a = reshape(A, size(x));
end

%!demo
%!	t = 0:.1:20;
%!	for i = 1:6
%!		r = center(1:i)';
%!		[r,~,ist] = newton(r,r*0,t); assert(ist==2);
%!		subplot(2,3,i); plot(t,squeeze(r)); grid on;
%!	end

%!demo
%!	[x,y] = ndgrid(1:3,1:2); r = [x(:) y(:)]; t = 0:.1:10;
%!	[r,~,ist] = newton(r,r*0,t); assert(ist==2);
%!	h(1) = plot(r(:,1,:)(:),r(:,2,:)(:),'o'); axis manual; grid on;
%!	h(2) = title(0);
%!	for i = 1:numel(t)
%!		set(h(1),'xdata',r(:,1,i),'ydata',r(:,2,i));
%!		set(h(2),'string',t(i)); drawnow; pause(.1);
%!	end
