%% vq = vqn_gnd(Rd, Rs, rs)
%% vq = vqn_gnd(Rd, Rs, rs, rg)

function vq = vqn_gnd(Rd, Rs, rs, rg=0)
	dim = size(Rs,2);
	r = @(R) sqrt(sumsq(R,2));
	rp = r(Rd-Rs);
	if (rg)
		s = r(Rs)/rg;
		rn = ifelse(s, r(Rd.*s-Rs./s), rg);
	else
		n = [ones(1,dim-1) -1];
		rn = r(Rd-Rs.*n);
	end
	rp = max(rs,rp);
	rn = max(rs,rn);
	switch (dim)
	case 1	f = @(r) 1/(-2)    * r;
	case 2	f = @(r) 1/(-2*pi) * log(r);
	case 3	f = @(r) 1/(4*pi) ./ r;
	end
	vq = f(rp)-f(rn);
end

%!shared tol,r,n,rs,rg
%!	tol = eps^.5;
%!	r = @(R) sqrt(sumsq(R,2));
%!	n = 30; rs = rand(n,1); rg = rande;
%!test
%!	for d = 1:3
%!		Rd = randn(n,d)*2;
%!		Rs = randn(n,d)*2;
%!		v1 = vqn_gnd(Rd,Rs,rs);
%!		v2 = vqn_gnd(Rd,Rs,rs,rg);
%!		assert(v1, vqn_gnd(-Rd,-Rs,rs), tol);
%!		assert(v2, vqn_gnd(-Rd,-Rs,rs,rg), tol);
%!		a = [randn(1,d-1) 0];
%!		assert(v1, vqn_gnd(Rd+a,Rs+a,rs), tol);
%!		a = blkdiag(orth(randn(d-1)),1);
%!		assert(v1, vqn_gnd(Rd*a,Rs*a,rs), tol);
%!		a = orth(randn(d));
%!		assert(v2, vqn_gnd(Rd*a,Rs*a,rs,rg), tol);
%!		a = rande;
%!		assert(v1*a^(2-d), vqn_gnd(Rd*a,Rs*a,rs*a), tol);
%!		assert(v2*a^(2-d), vqn_gnd(Rd*a,Rs*a,rs*a,rg*a), tol);
%!	end
%!test
%!	for d = 1:3
%!		Rs = randn(n,d)*2;
%!		R1 = randn(n,d)*2; R1 .*= [ones(1,d-1) 0];
%!		R2 = randn(n,d)*2; R2 .*= rg./r(R2);
%!		assert(zeros(n,1), vqn_gnd(R1,Rs,rs), tol);
%!		assert(zeros(n,1), vqn_gnd(Rs,R1,rs), tol);
%!		assert(zeros(n,1), vqn_gnd(R2,Rs,rs,rg), tol);
%!		assert(zeros(n,1), vqn_gnd(Rs,R2,rs,rg), tol);
%!	end

%!demo
%!	zd = -6:.1:6; zs = 0:5; rs = 1; rg = 3;
%!	f = @(v,l,i) plot(zd,v,zs(i)+rs*[1;1]*[-1 1],[-l;l],'k--');
%!	t = (-2:2)*rg;
%!	v = []; for d = 1:3
%!		Rd = prepad(zd(:),d,0,2);
%!	v1 = []; for s = zs
%!		Rs = prepad(s,d,0,2);
%!		v1 = cat(3,v1,[vqn_gnd(Rd,Rs,rs) vqn_gnd(Rd,Rs,rs,rg)]);
%!	end
%!		v = cat(4,v,v1);
%!	end
%!	for i = 1:size(v,4) figure(i);
%!	for j = 1:size(v,3) subplot(2,3,j);
%!		f(v(:,:,j,i),max(abs(v(:,:,:,i)(:))),j); grid on; xticks(t);
%!	end
%!	end
%!demo
%!	zd = -6:.1:6; zs = 0:5; rs = 1; rg = 3;
%!	f = @(v) imshow(vec(-1:1,3)/max(abs(v(:))).*v,'xdata',zd,'ydata',zd);
%!	[y,z] = meshgrid(zd); t = (-2:2)*rg;
%!	v = []; for d = 1:3
%!		Rd = prepad([y(:) z(:)],d,0,2);
%!	v1 = v2 = []; for s = zs
%!		Rs = prepad(s,d,0,2);
%!		v1 = cat(3,v1,reshape(vqn_gnd(Rd,Rs,rs),size(z)));
%!		v2 = cat(3,v2,reshape(vqn_gnd(Rd,Rs,rs,rg),size(z)));
%!	end
%!		v = cat(4,v,v1,v2);
%!	end
%!	for i = 1:size(v,4) figure(i);
%!	for j = 1:size(v,3) subplot(2,3,j);
%!		f(v(:,:,j,i)); grid on; axis on xy; xticks(t); yticks(t);
%!	end
%!	end
