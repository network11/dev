%% [v,e, ist,msg] = sphvef(l,c_r, v_0,e_0, r, ...)
%%	compute l-degree radial voltages in spherical symmetric medium

function [v,e, varargout] = sphvef(l,c_r, v_0,e_0, r, varargin)
	vd_0 = [v_0 e_0*c_r(r(1))];
	fcn = @(vd,r) [-vd(2)/c_r(r) -l*(l+1)*c_r(r)/r^2*vd(1)-2/r*vd(2)];
	[vd, varargout{1:nargout-1}] = lsode(fcn, vd_0, r, varargin{:});
	v = reshape(vd(:,1),size(r)); e = reshape(vd(:,2),size(r))./c_r(r);
end

%!xtest
%!	[a,b,c] = num2cell(rand(1,3)+.5){:}; r = sort(rand(1,9)+.5);
%!	for l = 0:5
%!		v = b*r.^(-l-1) - a*r.^l; e = b*(l+1)*r.^(-l-2) + a*l*r.^(l-1);
%!		assert({v e}, nthargout(1:2,@sphvef,l,@(r)c,v(1),e(1),r), 1e-3);
%!	end

%!demo
%!	r = 1e-3:1e-3:9;
%!	f = @lsode_options; k = 'maximum step size';
%!	v = f(k); f(k,.01); ocu = onCleanup(@() f(k,v));
%!	i = 0; for x = [.1 .1 .9 .99; 1 10 10 100]
%!		c = @(r) ifelse(r<1 & r>x(1), x(2), 1);
%!		[v,e] = sphvef(1,c,r(1),-1,r); v /= -e(end); e /= e(end);
%!		subplot(2,2,++i); plot(r,v,r,e); grid on; axis([0 3 0 3]);
%!	end
