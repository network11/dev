%% [x,y] = fuck(x,y, dx,dy)

function [x,y] = fuck(x,y, dx,dy)
	for i = 1:numel(x) if (mod(x(i),1) == 0 && y(x(i)) >= 1)
		y(x(i)) -= 1; x(i) -= dx;
	end end
	x = mod(x,numel(y)); x += dx; y += dy;
end

%!demo
%!	y = zeros(1,12); x = .5:2:numel(y); dy = .1; dx = 1/8;
%!	x += dx*randi(3,size(x));
%!	h = plot(y,'^',x,x*0,'>'); grid on; axis([0 12 0 12]);
%!	xx = x; for n = 1:999
%!		[x,y] = fuck(x,y, dx,dy); xx = [xx;x];
%!		set(h(1),'ydata',y); set(h(2),'xdata',x);
%!		drawnow; pause(.01);
%!	end
%!	plot(mod(xx-.8*dx*(0:n)',numel(y)),'.'); grid on;
