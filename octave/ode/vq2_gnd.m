%% vq = vq2_gnd(zd, zs, rs)
%% vq = vq2_gnd(zd, zs, rs, rg)

function vq = vq2_gnd(zd, zs, rs, rg=0)
	rp = abs(zd-zs);
	if (rg)
		rn = abs(zd.*conj(zs)/rg-rg);
	else
		rn = abs(zd-conj(zs));
	end
	rp = max(rs,rp);
	rn = max(rs,rn);
	vq = log(rn./rp) / (2*pi);
end

%!test
%!	tol = eps^.5;
%!	zs = [(randn(9,1)+1j*randn(9,1))*2;0];
%!	[zd,zs] = meshgrid(zs); zd = zd(:); zs = zs(:);
%!	rs = rand(size(zs)); rg = rande;
%!	Rd = [real(zd) imag(zd)];
%!	Rs = [real(zs) imag(zs)];
%!	assert(vq2_gnd(zd,zs,rs), vqn_gnd(Rd,Rs,rs), tol);
%!	assert(vq2_gnd(zd,zs,rs,rg), vqn_gnd(Rd,Rs,rs,rg), tol);

%!demo
%!	% waveguide
%!	x = 2:1/16:14; y = (-16:1/16:16)'; yi = (abs(y)<=1.5); yc = find(~y);
%!	subplot(311); h(1:2) = plot(x,x,x,x); axis([2 14 -.1 .1]); grid on;
%!	subplot(312); h(3) = imagesc(x,y(yi),x+y(yi),[-.1 .1]); colormap gray;
%!	subplot(313); h(4) = imagesc(x,y(yi),x+y(yi),[-.1 .1]); colormap gray;
%!	b = cos(pi*y).*hanning(numel(y))/16; r = hypot(x,y);
%!	for f = 2.^(-3:1/8:1)
%!		a = 1j/4*besselh(0,-2*pi*f*r)*sqrt(f);
%!		c = conv2(a,b,'same');
%!		set(h(1:2),{'ydata'},{real(a(yc,:));real(c(yc,:))});
%!		set(h(3:4),{'cdata'},{real(a(yi,:));real(c(yi,:))});
%!		drawnow;
%!	end

%!demo
%!	% opaque screen diffraction
%!	x = (3:3:12).^2; y = (0:1/16:25)'; r = hypot(x,y);
%!	a = cumsum(1j/4*besselh(0,-2*pi*r));
%!	subplot(221); plot(a); axis equal; grid on; legend(num2str(x'));
%!	subplot(223); plot(y,abs(a)); grid on;
%!	x = 2:1/16:12; y = (-25:1/16:25)'; yi = (abs(y)<=10);
%!	b = (y>=0).*hanning(numel(y))/16; r = hypot(x,y);
%!	a = 1j/4*besselh(0,-2*pi*r);
%!	c = conv2(a,b,'same');
%!	subplot(122); imagesc(x,y(yi),abs(c(yi,:))); colormap gray;
%!demo
%!	% Poisson bright spot
%!	x = 1/16:1/16:20; y = (-50:1/16:50)'; yi = (abs(y)<=10);
%!	b = (abs(y)>=5).*hanning(numel(y)); r = hypot(x,y);
%!	a = 1j/4*besselh(0,-2*pi*r);
%!	c = conv2(a,b,'same');
%!	imagesc(x,y(yi),abs(c(yi,:))); colormap gray;
%!demo
%!	% Gaussian beam
%!	x = 1/8:1/8:20; y = (-50:1/8:50)'; yi = (abs(y)<=10);
%!	b = exp(-y.^2/2); r = hypot(x,y);
%!	a = 1j/4*besselh(0,-2*pi*r);
%!	c = conv2(a,b,'same');
%!	imagesc(x,y(yi),abs(c(yi,:))); colormap gray;

%!demo
%!	% magnetic dipole and electric quadrupole
%!	[x,y,z] = sphere(120);
%!	d = @(c) surf(x.*abs(c),y.*abs(c),z.*abs(c));
%!	subplot(121); d(hypot(x.*z,y.*z,x.^2+y.^2)); shading interp; axis equal;
%!	subplot(122); d(hypot(x.*z,y.*z,x.^2-y.^2)); shading interp; axis equal;

%!demo
%!	% polarized reflection
%!	cr = tand(min(90-1e-9,0:5:90)).^2; cr = [1./(1+cr) 1+cr 1-1j*cr];
%!	t1 = deg2rad(max(1e-9,0:1e-2:90))';
%!	t2 = asin(sin(t1)./sqrt(cr));
%!	ro = sin(t2-t1)./sin(t2+t1);
%!	rp = tan(t2-t1)./tan(t2+t1);
%!	h = plot(1,'b-',1,'bo;ortho;',1,'r-',1,'rx;parallel;');
%!	axis([-1 1 -1 1],'equal'); grid on;
%!	i = linspace(1,numel(t1),19); for j = 1:numel(cr)
%!		set(h,{'xdata' 'ydata'},{
%!			real(ro(:,j)) imag(ro(:,j));real(ro(i,j)) imag(ro(i,j))
%!			real(rp(:,j)) imag(rp(:,j));real(rp(i,j)) imag(rp(i,j))
%!		}); drawnow; pause(.1);
%!	end

%!demo
%!	% wave beam
%!	r = -8:1/16:8; t = r';
%!	h = imshow(t+r+vec(1:3,3),'xdata',r,'ydata',t); axis on;
%!	xticks(r(1:64:end)); yticks(t(1:64:end));
%!	x = @(t,r) besselj(0,2*pi*sqrt(t.^2-r.^2)) .* ((t>abs(r))-(t<-abs(r)));
%!	y = @(t,r) besselj(0,2*pi*sqrt(t.^2+r.^2));
%!	function x = z(x,m) x=conv2(x,hanning(m)','same'); x/=max(x(:)); end
%!	for a = {y(t,r) x(t,r)} a = a{1};
%!	for m = 1:2:r(end)/(r(2)-r(1))
%!		set(h,'cdata',abs(cat(3,z(a,m),0*a,a)));
%!		drawnow; pause(r(2)-r(1));
%!	end
%!	end
%!	for p = deg2rad(0:45)
%!		a = (t+1j*r)*exp(1j*p); a = x(real(a),imag(a));
%!		set(h,'cdata',abs(cat(3,z(a,m),0*a,a)));
%!		drawnow;
%!	end
