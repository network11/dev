%% mulcoeff(k)
%% mulcoeff(k, dim)
%%	Return the multinomial coefficient of n and k's along dimension dim
%%	where n == sum(k, dim)

function m = mulcoeff(k, varargin)
	k(k~=round(k)) = nan;
	g = @(x) gammaln(x+1);
	s = @(x) sum(x,varargin{:});
	m = round(exp(g(s(k))-s(g(k))));
end

%!test
%!	for i = 1:4 for j = 1:4 for d = 1:2
%!		k = randi(5,i,j);
%!		y = mulcoeff(k,d);
%!		assert(y, cell2mat(cellfun(@mulcoeff,num2cell(k,d),'uniformoutput',0)));
%!	end end end
%!test
%!	for m = 1:4
%!		k = randi(5,m,30);
%!		y = mulcoeff(k,1);
%!		assert(y, prod(bincoeff(cumsum(k,1),k),1));
%!		assert(y, plus(0,cellfun(@(e){mulcoeff(k-e,1)},num2cell(eye(m),1)){:}));
%!	end
%!test
%!	for m = 1:4
%!		kk=[]; [kk{1:m}]=ndgrid(0:4); kk=[cellfun(@(x){x(:)},kk){:}]';
%!		for n = 0:4
%!			k = kk(:,sum(kk,1)==n);
%!			y = mulcoeff(k,1);
%!			assert(numel(y), bincoeff(n+m-1,m-1));
%!			assert(sum(y), m^n);
%!		end
%!	end

%!demo
%!	[n,k] = ndgrid(0:9);
%!	y = mulcoeff(cat(3,k,n-k),3);
%!	disp('triangle'); disp(y);
%!demo
%!	for n = 0:9
%!		[a,b] = ndgrid(0:n);
%!		y =  mulcoeff(cat(3,a,b,n-a-b),3);
%!		disp('pyramid'); disp(y);
%!	end
