%% [setx,sety,indxy] = gset(numxy)

function [x,y,i] = gset(n)
	d = length(n)+1;
	xy = (1:columns(n))*d+(1:rows(n))';
	xy = unique(perms(repelem(xy(:),n(:))),'rows');
	[x,~,ix] = unique(fix(xy/d),'rows');
	[y,~,iy] = unique(mod(xy,d),'rows');
	i = accumarray([iy ix],1);
end

%!test
%!	for sx = 1:3 for sy = 1:3 for n = 1:9
%!		do n = randi([0 round(20/sx/sy)],sy,sx); nn = sum(n(:));
%!		until (nn>0 && nn<10)
%!		[x,y,i] = gset(n);
%!		gxy = factorial(nn)/prod(factorial(n)(:));
%!		gx = factorial(nn)/prod(factorial(sum(n,1)));
%!		gy = factorial(nn)/prod(factorial(sum(n,2)));
%!		assert(size(x), [gx nn]);
%!		assert(size(y), [gy nn]);
%!		assert(size(i), [gy gx]);
%!		assert(lookup(1:sx, x, 'b'));
%!		assert(lookup(1:sy, y, 'b'));
%!		assert(lookup(0:1, i, 'b'));
%!		assert(sum(i,1) == gxy/gx);
%!		assert(sum(i,2) == gxy/gy);
%!		assert(gx*gy >= gxy);
%!	end end end

%!test
%!	for m = 1:9 for n = 1:9
%!		p = rand(m,n); p /= sum(p(:));
%!		i = log2(p./(sum(p,1).*sum(p,2)));
%!		assert(sum((p.*i)(:)) >= -eps^.5);
%!	end end
%!test
%!	% (a+b+c+d)!/(a+c)!(b+d)! >= (a+b)!(c+d)!/a!b!c!d!
%!	n = [cellfun(@(x){x(:)}, nthargout(1:4,@ndgrid,0:9)){:}]';
%!	x = mulcoeff([sum(n([1 3],:)); sum(n([2 4],:))]);
%!	y = mulcoeff(n([1 2],:)).*mulcoeff(n([3 4],:));
%!	assert(x >= y);
