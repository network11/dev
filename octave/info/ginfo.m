%% g = ginfo(n)
%% g = ginfo(n, dim)

function i = ginfo(n, varargin)
	g = @(n) gammaln(n+1)/log(2);
	s = @(x) sum(x,varargin{:});
	i = g(s(n))-s(g(n));
end

%!demo
%!	g = @(x) gammaln(x+1);
%!	h = @(x) x.*log(x)-x+log(2*pi*x)/2;
%!	x = linspace(0,5); subplot(3,2,1:2:3); plot(x,g(x),x,h(x)); grid on;
%!	x = logspace(0,5); subplot(3,2,2:2:4); loglog(x,g(x)-h(x)); grid on;

%!demo
%!	n = 0:30; q = n/n(end); p = rand;
%!	x = n'.*cat(3,q,1-q); y = sum(x.*log2(cat(3,p,1-p)),3);
%!	g = y + ginfo(x,3); h = y + hinfo(x,3);
%!	for i = 1:2 subplot(2,2,i);
%!		mesh(q,n,g); hold on; mesh(q,n,h);
%!		mesh([p;p]*[1 1], [0;n(end)]*[1 1], [1;1]*zlim); hold off;
%!	end; view(0,0);
%!	[x,y] = meshgrid(n); x = cat(3,x,y); y = sum(x.*log2(cat(3,p,1-p)),3);
%!	g = y + ginfo(x,3); h = y + hinfo(x,3);
%!	x = [0;min(n(end)./[p 1-p])];
%!	for i = 3:4 subplot(2,2,i);
%!		mesh(n,n,g); hold on; mesh(n,n,h);
%!		mesh(x*p*[1 1], x*(1-p)*[1 1], [1;1]*zlim); hold off;
%!	end; view(-atand(p/(1-p)),0);

%!demo
%!	n = 1e3; k = 0:n;
%!	g = ginfo([k;n-k]); h = hinfo([k;n-k]);
%!	subplot(221); x = sum(2.^g); plot(k,2.^[g;h]/x); grid on;
%!	subplot(222); plot(k,h-g); grid on;
%!	subplot(223); plot(k,[g;h]); grid on;
%!	subplot(224); plot(k,h./g); grid on;
%!	assert(h >= g);
%!demo
%!	n = 300; k = 30;
%!	g = h = []; for i = 1:1e2
%!		x = randi(k,1,randi(n/k)); g(i) = ginfo(x); h(i) = hinfo(x);
%!	end
%!	subplot(221); x = max(2.^h); plot(2.^g/x,2.^h/x,'o'); grid on;
%!	subplot(222); plot(g,h-g,'o'); grid on;
%!	subplot(223); plot(g,h,'o'); grid on;
%!	subplot(224); plot(g,h./g,'o'); grid on;
%!	assert(h >= g);

%!shared tol
%!	tol = eps^.5;
%!test
%!	for n = 1:4
%!		x = ones(1,n)/n; g1u = ginfo(x); h1u = hinfo(x);
%!		nn=[]; [nn{1:n}]=ndgrid(1:4); nn=[cellfun(@(x){x(:)},nn){:}]';
%!		gn = ginfo(nn,1); g1 = ginfo(nn./sum(nn,1),1);
%!		hn = hinfo(nn,1); h1 = hinfo(nn./sum(nn,1),1);
%!		assert(g=2.^gn, round(g), -tol);
%!		assert(hn./sum(nn,1), h1, tol);
%!		assert(2^h1u, n, tol);
%!		assert(g1 <= g1u && h1 <= h1u);
%!	end
%!test
%!	for n = 1:100
%!		k = 0:n/2; g = ginfo([k;n-k]); h = hinfo([k;n-k]);
%!		assert(h >= log2(cumsum(2.^g)));
%!	end
%!function [gx,gy,gxy,hx,hy,hxy] = joint(n)
%!	nx = sum(n,2); ny = sum(n,1); nxy = reshape(n,[],1,size(n,3));
%!	gx = ginfo(nx,1); gy = ginfo(ny,2); gxy = ginfo(nxy,1);
%!	hx = hinfo(nx,1); hy = hinfo(ny,2); hxy = hinfo(nxy,1);
%!endfunction
%!test
%!	for i = 1:3 for j = 1:3
%!		[gx,gy,gxy,hx,hy,hxy] = joint(3*rand(i,1)*rand(1,j));
%!		assert(gx+gy-gxy >= 0);
%!		assert(hx+hy-hxy, 0, tol);
%!		[gx,gy,gxy,hx,hy,hxy] = joint(3*rand(i,j,30));
%!		assert(gx+gy-gxy >= 0);
%!		assert(hx+hy-hxy >= 0);
%!		assert(gxy-gx >= 0 && gxy-gy >= 0);
%!		assert(hxy-hx >= 0 && hxy-hy >= 0);
%!		[gx,gy,gxy,hx,hy,hxy] = joint(randi(3,i,j,30));
%!		assert(g=2.^(gxy-gx), round(g), -tol);
%!		assert(g=2.^(gxy-gy), round(g), -tol);
%!	end end
