%% h = hinfo(p)
%% h = hinfo(p, dim)

function i = hinfo(n, varargin)
	h = @(n) ifelse(n,n.*log2(n),0);
	s = @(x) sum(x,varargin{:});
	i = h(s(n))-s(h(n));
end

%!test
%!	g = @(x) sqrt(2*pi)\exp(-x.^2/2);
%!	q = @(x) erfc(x/sqrt(2))/2;
%!	x = randn(1,9);
%!	assert(q(x), arrayfun(@quad, g, x, inf), eps^.5);
%!demo
%!	% repetition code
%!	db = (0:12)'; a = 10.^(db/20); nn = 1:2:7;
%!	q = @(x) erfc(x/sqrt(2))/2;
%!	pp = []; for n = nn
%!		k = ceil(n/2):n; p = q(a./sqrt(n));
%!		pp(:,end+1) = sum(bincoeff(n,k).*p.^k.*(1-p).^(n-k),2);
%!	end
%!	assert(pp(:,1), q(a));
%!	semilogy(db,pp); grid on; xlabel dB; legend(num2str(nn(:)));

%!demo
%!	db = -10:10;
%!	x = center(0:200);
%!	n = center(1:600);
%!	ax = 30;
%!	px_a = exp(-(x/ax).^2/2); px_a /= sum(px_a);
%!	px_d = abs(x)==ax; px_d /= sum(px_d);
%!	px_d = polyreduce(flip(polyreduce(px_d)));
%!	joint = @(p) hinfo(sum(p,2))+hinfo(sum(p,1))-hinfo(p(:));
%!	ixy = []; for an = ax./10.^(db/20)
%!		pn = exp(-(n/an).^2/2); pn /= sum(pn);
%!		pxy_a = conv2(diag(px_a),pn);
%!		pxy_s = conv2(diag(px_d),pn);
%!		pxy_d = pxy_s * blkdiag({ones(columns(pxy_s)/2,1)}{[1 1]});
%!		ixy_a = joint(pxy_a);
%!		ixy_s = joint(pxy_s);
%!		ixy_d = joint(pxy_d);
%!		ixy(end+1,:) = [ixy_a ixy_s ixy_d];
%!	end
%!	plot(db,ixy); grid on; xlabel dB; ylabel bit;
%!	legend analog soft hard location northwest;
