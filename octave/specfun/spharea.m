%% s = spharea(n)
%%	Compute the area of the unit-radius n-sphere.

function s = spharea(n)
	s = 2*pi.^((n+1)/2) ./ gamma((n+1)/2);
end

%!shared n,tol
%!	tol = eps^.5;
%!	n = 0:9;
%!assert(sphvolume(0:3), [1 2 pi pi*4/3], tol);
%!assert(spharea(0:2), [2 2*pi 4*pi], tol);
%!assert(sphvolume(n+1), spharea(n)./(n+1), tol);
%!assert(spharea(n+1), sphvolume(n)*(2*pi), tol);
%!assert(sphvolume(n+1), sphvolume(n).*beta((n+2)/2,.5), tol);
%!assert(spharea(n+1), spharea(n).*beta((n+1)/2,.5), tol);

%!assert(arrayfun(@(n)quad(@(x)sin(x).^n,0,pi),n), beta((n+1)/2,.5), tol);
%!assert(arrayfun(@(n)prod(n:-2:1),n*2-1)./2.^n*sqrt(pi), gamma(n+.5), tol);
%!assert(factorial(n), gamma(n+1), tol);
