%% p = legendren(n, l, m)
%% p = legendren(n, l, m, theta)
%%	Compute n-dimensional l-degree m-order associated Legendre polynomial.

function p = legendren(n, l, m, x)
	c = sqrt(beta(n/2,.5)) * norm(l+n/2-1,m+n/2-1);
	k = l-m-2:-2:0;
	r = (k+2).*(k+1)./(k+m-l)./(k+m+l+n-1);
	p(1+[l-m k]) = cumprod([c r]);
	p = p(end:-1:1);
	if (nargin > 3)
		p = polyval(p,cos(x)) .* sin(x).^m;
	end
end

function c = norm(l, m)
	if ([l m] == -.5) c = 1/sqrt(pi); return end
	c = sqrt((l+.5)/gamma(l+m+1)/gamma(l-m+1))*gamma(l*2+1)/gamma(l+1)/2^l;
end

%!shared tol
%!	tol = eps^.5;
%!test
%!	for n = 1:6 for l = 0:5 for m = 0:l
%!		p = legendren(n,l,m);
%!		assert(size(p), [1 1+l-m]);
%!		assert(all(p(2:2:end) == 0));
%!		assert(all(p(1:4:end) > 0));
%!		assert(all(p(3:4:end) < 0));
%!	end end end
%!test
%!	for n = 1:6 for l = 0:5 for m = 0:l
%!		x = (beta(n/2,.5)/2)^.5 * legendren(2,l+n/2-1,m+n/2-1);
%!		y = legendren(n,l,m);
%!		assert(y, x, tol);
%!	end end end
%!test
%!	for n = 1:6 for l = 0:5 for m = 0:l-1
%!		x = ((l-m)*(l+m+n-1))^.5 \ polyder(legendren(n,l,m));
%!		y = legendren(n,l,m+1);
%!		assert(y, x, tol);
%!	end end end
%!test
%!	% (1-x^2)*D^2 - (2*m+n)*r*D - m*(m+n-1) - l*(l+n-1) = 0
%!	for n = 1:6 for l = 0:5 for m = 0:l
%!		p = [0 0 legendren(n,l,m)];
%!		pd = polyder(p); pdd = polyder(pd);
%!		d = conv(pdd,[-1 0 1])-conv(pd,[2*m+n 0])+conv(p,l*(l+n-1)-m*(m+n-1));
%!		assert(d, zeros(size(d)), tol);
%!	end end end

%!test
%!	t = rand(1,9)*pi;
%!	for n = 1:6
%!		assert(legendren(n,0,0,t), ones(size(t)), tol);
%!	end
%!	for l = 1:5
%!		assert(legendren(1,l,0,t), cos(l*t)*2^.5, tol);
%!		assert(legendren(1,l,1,t), sin(l*t)*2^.5, tol);
%!		assert(legendren(3,l,0,t), sin(l*t+t)./sin(t), tol);
%!	end
%!	for l = 0:5 p = legendre(l,cos(t),'norm')*2^.5; for m = 0:l
%!		assert(legendren(2,l,m,t), p(m+1,:), tol);
%!	end end
%!test
%!	for n = 1:6 for L = 0:3 for l = 0:L for m = 0:l
%!		f = @(x)legendren(n,L,m,x).*legendren(n,l,m,x).*sin(x).^(n-1);
%!		q = (L==l)*spharea(n)/spharea(n-1);
%!		assert(quad(f,0,pi), q, tol);
%!	end end end end
%!test
%!	t = deg2rad(1:180-1); s = sin(t);
%!	for n = 1:6 for l = 0:3 for m = 0:l-1
%!		p = legendren(n,l,m,t);
%!		pt = gradient(p./s.^m,t).*s.^m/((l-m)*(l+m+n-1))^.5;
%!		d = pt + legendren(n,l,m+1,t);
%!		assert(max(abs(d(3:end-2))), 0, 1e-3);
%!	end end end
%!test
%!	t = deg2rad(1:1e-2:180-1); ct = cot(t); cs = csc(t).^2;
%!	for n = 1:6 for l = 0:3 for m = 0:l
%!		p = legendren(n,l,m,t);
%!		pt  = gradient(p, t);
%!		ptt = gradient(pt,t);
%!		d = ptt + (n-1).*ct.*pt + (l*(l+n-1)-m*(m+n-2).*cs).*p;
%!		assert(max(abs(d(3:end-2))), 0, 1e-4);
%!	end end end

%!test
%!	dt = 1e-1; t = -9:dt:9;
%!	[tt,tr] = cart2pol(nthargout(1:2,@ndgrid,t){:});
%!	for l = 0:3 for m = 0:min(l,1)
%!		s = legendren(1,l,m,tt);
%!		h = s.*tr.^l;
%!		d = 4*del2(h,dt);
%!		assert(max(abs(d(i=3:end-2,i)(:))), 0, tol);
%!		h = s.*besseljn(2,l,tr);
%!		d = 4*del2(h,dt) + h;
%!		assert(max(abs(d(i=3:end-2,i)(:))), 0, 1e-3);
%!	end end
%!test
%!	dt = 4e-1; t = -9:dt:9;
%!	[tt,tp,tr] = cart2sph(nthargout(1:3,@ndgrid,t){:}); tp = pi/2-tp;
%!	for l = 0:2 for m = 0:l for M = 0:min(m,1)
%!		s = legendren(2,l,m,tp).*legendren(1,m,M,tt);
%!		h = s.*tr.^l;
%!		d = 6*del2(h,dt);
%!		assert(max(abs(d(i=3:end-2,i,i)(:))), 0, tol);
%!		h = s.*besseljn(3,l,tr);
%!		d = 6*del2(h,dt) + h;
%!		assert(max(abs(d(i=3:end-2,i,i)(:))), 0, 1e-2);
%!	end end end

%!demo
%!	d = 1:180-1; t = deg2rad(d); lim = [0 180 -6 6]; tic = 0:30:180;
%!	i = 0; for n = 1:4 figure(++i);
%!	for l = 0:3 subplot(2,2,l+1);
%!		p = []; for m = 0:l p = [p;legendren(n,l,m,t)]; end
%!		plot(d,p); axis(lim); xticks(tic); grid on; title(l);
%!	end end

%!demo
%!	[x,y,z] = sphere(120); [t,p,r] = cart2sph(x,y,z); p = pi/2-p;
%!	d1 = @(c) surf(x,y,z,c);
%!	d2 = @(c) surf(x,y,z,sign(c));
%!	d3 = @(c) surf(x.*abs(c),y.*abs(c),z.*abs(c),sign(c));
%!	i = 0; for l = 0:3 for m = 0:l
%!		s = legendren(2,l,m,p);
%!		r = legendren(1,m,0,t).*s;
%!		figure(++i);
%!		subplot(231); d1(r); try axis('equal'); end; shading interp;
%!		subplot(232); d2(r); try axis('equal'); end; shading interp;
%!		subplot(233); d3(r); try axis('equal'); end; shading interp;
%!		if (m < 1) continue end
%!		r = legendren(1,m,1,t).*s;
%!		subplot(234); d1(r); try axis('equal'); end; shading interp;
%!		subplot(235); d2(r); try axis('equal'); end; shading interp;
%!		subplot(236); d3(r); try axis('equal'); end; shading interp;
%!	end end

%!demo
%!	dt = df = 1/16; t = -.5/df:dt:.5/df-1e-9; f = -.5/dt:df:.5/dt-1e-9;
%!	xl = 1; yl = [1/4/pi/df 4/pi^2 2/pi^2]; tl = fl = [-4 4 -4 4];
%!	p = @(x,y,z,l) imshow(vec(-1:1,3)/l.*z,'xdata',x,'ydata',y);
%!	[tt,tr] = cart2pol(nthargout(1:2,@ndgrid,t){:});
%!	w = exp(-pi*(2*df*tr).^2); w(1,:) = w(:,1) = 0;
%!	i = 0; for l = 0:2 for m = 0:min(l,1)
%!		x(:,:,1) = besselj(l,2*pi*tr);
%!		x(:,:,2) = bessely(l,2*pi*tr);
%!		x(:,:,3) = besselh(l,2j*pi*tr)*1j^(l+1);
%!		x = w.*legendren(1,l,m,tt).*x; x(~isfinite(x)) = 0;
%!		y = fftshift(fft2(ifftshift(1j^l*x)))*dt^2;
%!		assert(max(abs(imag(x(:)))), 0, eps^.5);
%!		assert(max(abs(imag(y(:)))), 0, eps^.5);
%!		figure(++i); for j = 1:3
%!		subplot(2,3,j  ); p(t,t,real(x(:,:,j)),xl   ); axis(tl,'on');
%!		subplot(2,3,j+3); p(f,f,real(y(:,:,j)),yl(j)); axis(fl,'on');
%!		end
%!	end end

%!demo
%!	dt = df = 1/16; t = -.5/df:dt:.5/df-1e-9; f = -.5/dt:df:.5/dt-1e-9;
%!	xl = 1; yl = [4/pi^2 1/4/pi/df 1/4/pi/df]; tl = fl = [-4 4 -4 4];
%!	p = @(x,y,z,l) imshow(vec(-1:1,3)/l.*z,'xdata',x,'ydata',y);
%!	w = exp(-pi*(2*df)^2*(t.^2+t'.^2)); w(1,:) = w(:,1) = 0;
%!	s = t.^2-t'.^2; ts = (sign(s)+1)/2; tr = sqrt(s);
%!	x(:,:,1) = besselj(0,2*pi*tr).*ts;
%!	x(:,:,2) = x(:,:,1).*sign(t);
%!	x(:,:,3) = real(bessely(0,2*pi*tr));
%!	x = w.*x; x(~isfinite(x)) = 0; l = vec([0 1 0],3);
%!	y = fftshift(fft2(ifftshift(1j.^l.*x)))*dt^2;
%!	assert(max(abs(imag(x(:)))), 0, eps^.5);
%!	assert(max(abs(imag(y(:)))), 0, eps^.5);
%!	for j = 1:3
%!	subplot(2,3,j  ); p(t,t,real(x(:,:,j)),xl   ); axis(tl,'on');
%!	subplot(2,3,j+3); p(f,f,real(y(:,:,j)),yl(j)); axis(fl,'on');
%!	end
