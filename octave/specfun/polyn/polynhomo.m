%% a = polynhomo(p, n)
%%	Transform odd/even polynomial to homogeneous multinomial.

function a = polynhomo(p, n)
	assert(all(p(2:2:end) == 0));
	r2 = accumarray(1+2*eye(n), 1);
	sz = [numel(p)*ones(1,n) 1];
	r = 1; a = 0; for c = diag(p)(end:-1:1,1:2:end)
		a += resize(convn(vec(c,n),r),sz);
		r = convn(r,r2);
	end
end

%!test
%!	tol = eps^.5;
%!	for n = 1:5 for l = 0:5
%!		p = randn(1,l+1); p(2:2:end) = 0;
%!		a = polynhomo(p,n);
%!		x = randn(5,n); x ./= sqrt(sumsq(x,2)); z = x(:,end);
%!		assert(sum(findn(a)-1,2) == l);
%!		assert(polynval(a,x), polyval(p,z), tol);
%!	end end
