%% a = polyndel2(a)
%% a = polyndel2(a, dim)

function b = polyndel2(a, n=1:ndims(a))
	b = 0; for n = n
		b += polynder(polynder(a,n),n);
	end
end
