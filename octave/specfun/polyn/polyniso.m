%% a = polyniso(p, n)
%%	Transform even polynomial to isotropic multinomial.

function a = polyniso(p, n)
	assert(all(p(end-1:-2:1) == 0));
	r2 = accumarray(1+2*eye(n), 1);
	sz = [numel(p)*ones(1,n) 1];
	r = 1; a = 0; for c = p(end:-2:1)
		a += resize(c*r,sz);
		r = convn(r,r2);
	end
end

%!test
%!	tol = eps^.5;
%!	for n = 1:5 for l = 0:2:6
%!		p = randn(1,l+1); p(2:2:end) = 0;
%!		a = polyniso(p,n);
%!		x = randn(5,n); r = sqrt(sumsq(x,2));
%!		assert(mod(findn(a)-1,2) == 0);
%!		assert(polynval(a,x), polyval(p,r), tol);
%!	end end
