%% a = polynlegendren(l)
%% p = polynlegendren(l, s)
%%	Compute spherical homogeneous multinomial.

function a = polynlegendren(l, x)
	n = numel(l); m = l(1)<0; l(1) = abs(l(1)); m = [m l(1:end-1)];
	if (nargin > 1)
		a = 1; for k = 1:n-1
			a = a .* legendren(k,l(k),m(k), x(:,k));
		end
		a = a .* x(:,n) .^ l(n);
	else
		a = [zeros(m(1));1]; for k = 1:n-1
			a = convn(a, polynhomo(legendren(k,l(k),m(k)), k+1));
		end
		a = convn(a, polyniso(eye(1,1+l(n)-m(n)), n));
		a = resize(a, [length(a)*ones(1,n) 1]);
	end
end

%!test
%!	tol = eps^.5;
%!	for n = 2:5 for l = 0:5 for m = l:-2:0 for M = sphdegs(n-1,m)' L = [M' l];
%!		a = polynlegendren(L);
%!		c = randn(5,n); s = cart2sphn(c);
%!		assert(size(a), size(zeros((l+1)*ones(1,n))));
%!		assert(sum(findn(a)-1,2) == l);
%!		assert(polynval(a,c), polynlegendren(L,s), tol);
%!	end end end end

%!test
%!	tol = eps^.5;
%!	for n = 2:4 for l = 0:5 for m = l:-2:0 for M = sphdegs(n-1,m)' L = [M' l];
%!		a = polynlegendren(L);
%!		assert(polyndel(a), l*a, tol);
%!		for k = 1:n-1
%!			assert(polyncurl2(a,k), -L(k)*(L(k)+k-1)*a, tol);
%!		end
%!	end end end end

%!test
%!	tol = eps^.5;
%!	for n = 2:4 for l = 0:5 for M = sphdegs(n-1,l)' L = [M' l];
%!		a = polynlegendren(L);
%!		assert(polyndel2(a), 0*a, tol);
%!	end end end
