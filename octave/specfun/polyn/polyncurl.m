%% a = polyncurl(a, dim1,dim2)

function b = polyncurl(a, x,y)
	b = shift(polynder(a,y),1,x) - shift(polynder(a,x),1,y);
end

function x = shift(x,b,dim)
	if (size(x,dim)>1) x = circshift(x,b,dim); end
end
