%% a = polyn2hermitee(a)
%%	Transform each monomial to Hermite_e multinomial.

function h = polyn2hermitee(p)
	[l,c] = findn(p); l--;
	h = 0; for i = 1:numel(c)
		h += resize(c(i)*polynhermitee(l(i,:)), size(p));
	end
end

%!function m = degs(n, l)
%!	[m{1:n}] = ndgrid(0:l); m = [cellfun(@(x){x(:)},m){:}];
%!endfunction
%!test
%!	tol = eps^.5;
%!	for n = 1:4 for l = degs(n,5)' l = l';
%!		h = polynhermitee(l); p = accumarray(1+l,1);
%!		assert(polyn2hermitee(p), h, tol);
%!	end end

%!test
%!	tol = eps^.5;
%!	for n = 2:5 for l = 0:5 for m = l:-2:0 for M = sphdegs(n-1,m)' L = [M' l];
%!		h = polynhermiteen(L); p = polynlegendren(L);
%!		assert(polyn2hermitee(p), h, tol);
%!		[s,c] = findn(p); c = sum(prod(gamma(s),2).*c.^2);
%!		s = prod(l+m+n-2:-2:n)*prod(l-m:-2:2);
%!		assert(c, s, tol);
%!	end end end end
