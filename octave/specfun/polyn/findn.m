%% [subs, vals] = findn(a)
%%	Return subscripts of nonzero values of an array.

function [s, v] = findn(a)
	[i,~,v] = find(a(:));
	[s{1:ndims(a)}] = ind2sub(size(a),i); s = [s{:}];
end

%!test
%!	for n = 1:5 for k = 1:5
%!		a = accumarray(randi(5,k,n), randn(k,1));
%!		[s,v] = findn(a); b = accumarray(s,v);
%!		assert(a, b);
%!		assert(s, round(s));
%!		assert(s > 0 && s <= size(a));
%!	end end
