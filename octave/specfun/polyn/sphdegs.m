%% l = sphdegs(n, l)
%%	List all possible subsphere degrees of l-degree n-sphere.

function m = sphdegs(n, l)
	assert(isscalar(n) && n == round(n) && n >= 1);
	assert(isscalar(l) && l == round(l) && l >= 0);
	m = degs(n, l);
end

function m = degs(n, l)
	if (n <= 1) m = unique([-l;l]); return end
	m = cell2mat(arrayfun(@degs, n-1, (0:l)', 'uniformoutput',0));
	m(:,end+1) = l;
end

%!test
%!	bincoeff = @(n,k) ifelse(n<k, 0, bincoeff(n,k));
%!	for n = 1:5 for l = 0:6
%!		l1 = sphdegs(n,l); l2 = ((1:n)-1+l1).*l1;
%!		assert(size(l1), [bincoeff(n+l,n)-bincoeff(n+l-2,n) n]);
%!		assert(all(diff(abs(l1),[],2)(:) >= 0));
%!		assert(range(mean(l2,1)./bincoeff((1:n)+1,2)), 0, eps^.5);
%!	end end

%!demo
%!	bincoeff = @(n,k) ifelse(n<k, 0, bincoeff(n,k));
%!	n = 1:5; l = 0:6;
%!	a = bincoeff(n+1,2)
%!	[l,n] = ndgrid(l,n);
%!	g = bincoeff(n+l,n)-bincoeff(n+l-2,n)
%!	assert(g(2:end,2:end), g(2:end,1:end-1)+g(1:end-1,2:end));
