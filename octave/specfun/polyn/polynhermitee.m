%% a = polynhermitee(l)
%% h = polynhermitee(l, c)
%%	Compute Hermite_e multinomial.

function a = polynhermitee(l, x)
	n = numel(l);
	if (nargin > 1)
		a = 1; for k = 1:n
			a = a .* hermitee(l(k), x(:,k));
		end
	else
		a = 1; for k = 1:n
			a = a .* vec(hermitee(l(k))(end:-1:1), k);
		end
	end
end

%!function m = degs(n, l)
%!	[m{1:n}] = ndgrid(0:l); m = [cellfun(@(x){x(:)},m){:}];
%!endfunction
%!test
%!	tol = eps^.5;
%!	for n = 1:4 for l = degs(n,5)' l = l';
%!		a = polynhermitee(l);
%!		c = randn(5,n);
%!		assert(size(a), size(zeros([l+1 1])));
%!		assert(sum(findn(a)-1,2) <= sum(l));
%!		assert(polynval(a,c), polynhermitee(l,c), tol);
%!	end end

%!test
%!	tol = eps^.5;
%!	for n = 1:3 for l = degs(n,5)' l = l';
%!		a = polynhermitee(l);
%!		assert(polyndel(a)-polyndel2(a), sum(l)*a, tol);
%!		for k = 1:n
%!			assert(polyndel(a,k)-polyndel2(a,k), l(k)*a, tol);
%!		end
%!	end end

%!test
%!	tol = eps^.5;
%!	for n = 1:3 for l = degs(n,5)' l = l';
%!		a = accumarray(1+l,1);
%!		assert(polyndel(a), sum(l)*a, tol);
%!		for k = 1:n
%!			assert(polyndel(a,k), l(k)*a, tol);
%!		end
%!	end end
