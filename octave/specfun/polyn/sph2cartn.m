%% c = sph2cartn(s)
%%	c = [... x y z]
%%	s = [... φ θ r]
%%	z = r cosθ , y = r sinθ cosφ , x = r sinθ sinφ ...

function c = sph2cartn(s)
	s = s(:,end:-1:1);
	c = cumprod([s(:,1) sin(s(:,2:end))], 2);
	c(:,1:end-1) .*= cos(s(:,2:end));
	c = c(:,end:-1:1);
end

%!function c = sph2cart2(s)
%!	[t,r] = deal(num2cell(s,1){:});
%!	z = r.*cos(t);
%!	y = r.*sin(t);
%!	c = [y z];
%!endfunction
%!function c = sph2cart3(s)
%!	[p,t,r] = deal(num2cell(s,1){:});
%!	z = r.*cos(t);
%!	y = r.*sin(t).*cos(p);
%!	x = r.*sin(t).*sin(p);
%!	c = [x y z];
%!endfunction
%!function c = sph2cart4(s)
%!	[k,p,t,r] = deal(num2cell(s,1){:});
%!	z = r.*cos(t);
%!	y = r.*sin(t).*cos(p);
%!	x = r.*sin(t).*sin(p).*cos(k);
%!	w = r.*sin(t).*sin(p).*sin(k);
%!	c = [w x y z];
%!endfunction
%!test
%!	tol = eps^.5;
%!	s = randn(9,1)*3; assert(sph2cartn(s), s, tol);
%!	s = randn(9,2)*3; assert(sph2cartn(s), sph2cart2(s), tol);
%!	s = randn(9,3)*3; assert(sph2cartn(s), sph2cart3(s), tol);
%!	s = randn(9,4)*3; assert(sph2cartn(s), sph2cart4(s), tol);
