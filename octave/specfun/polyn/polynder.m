%% a = polynder(a, dim)
%%	Return the partial derivative of the multinomial.

function b = polynder(a, n)
	b = shift(a.*vec(0:size(a,n)-1,n),-1,n);
end

function x = shift(x,b,dim)
	if (size(x,dim)>1) x = circshift(x,b,dim); end
end

%!function a = der(a, n)
%!	f = @(p) reshape(polyder([0;p(:)(end:-1:1)])(end:-1:1),size(p));
%!	a = cell2mat(cellfun(f,num2cell(a,n),'uniformoutput',0));
%!endfunction

%!test
%!	for l = 1:5
%!		p = randn(1,l);
%!		d = polyder([0 p(end:-1:1)])(end:-1:1);
%!		for n = 1:5
%!			assert(polynder(vec(p,n),n), vec(d,n));
%!		end
%!	end
%!test
%!	for n = 1:5 for i = 1:5
%!		a = randn([randi(5,1,n) 1]);
%!		for k = 1:n
%!			assert(polynder(a,k), der(a,k));
%!		end
%!	end end
