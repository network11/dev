%% a = polyncurl2(a, dim)

function b = polyncurl2(a, n)
	b = 0; for x = 1:n for y = x+1:n+1
		b += polyncurl(polyncurl(a,x,y),x,y);
	end end
end

%!test
%!	tol = eps^.5;
%!	for l = 0:9 m = -l:2:l;
%!		c = (diag(-l:-1,-1)+diag(1:l,1))^2;
%!		v = [arrayfun(@(m){diag(flip(polynlegendren([m l])))}, m){:}];
%!		assert(c*v, v*diag(-m.^2), tol);
%!	end
