%% a = polyndel(a)
%% a = polyndel(a, dim)

function b = polyndel(a, n=1:ndims(a))
	b = 0; for n = n
		b += shift(polynder(a,n),1,n);
	end
end

function x = shift(x,b,dim)
	if (size(x,dim)>1) x = circshift(x,b,dim); end
end
