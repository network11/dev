%% y = polynval(a, x)
%%	Evaluate the multinomial at the specified values.

function a = polynval(a, x)
	a = shiftdim(a,-1);
	for i = 2:ndims(a)
		a = a .* x(:,i-1) .^ vec(0:size(a,i)-1,i);
	end
	a = sum(a(:,:),2);
end

%!function a = val(a, x)
%!	f = @(p,x) polyval(p(:)(end:-1:1),x);
%!	for i = 1:columns(x)
%!		a = cellfun(f,num2cell(a,i),{x(:,i)});
%!	end
%!endfunction

%!shared tol
%!	tol = eps^.5;
%!test
%!	for l = 0:5 for k = 1:5
%!		p = randn(1,l); x = randn(k,1);
%!		y = polyval(p(end:-1:1),x);
%!		for n = 1:5
%!			assert(polynval(vec(p,n),x*eye(n)(end,:)), y, tol);
%!		end
%!	end end
%!test
%!	for n = 1:5 for k = 1:5
%!		a = randn([randi(5,1,n) 1]); x = randn(k,n);
%!		assert(polynval(a,x), cellfun(@polynval,{a},num2cell(x,2)));
%!	end end
%!test
%!	for n = 1:5 for i = 1:5
%!		a = randn([randi(5,1,n) 1]); x = randn(1,n);
%!		assert(polynval(a,x), val(a,x), tol);
%!	end end
