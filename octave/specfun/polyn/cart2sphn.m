%% s = cart2sphn(c)
%%	c = [... x y z]
%%	s = [... φ θ r]
%%	z = r cosθ , y = r sinθ cosφ , x = r sinθ sinφ ...

function s = cart2sphn(c)
	r = sqrt(cumsum(c.^2, 2)); r(:,1) = c(:,1);
	s = [atan2(r(:,1:end-1),c(:,2:end)) r(:,end)];
end

%!test
%!	tol = eps^.5;
%!	for n = 1:6
%!		c = randn(9,n)*3; assert(sph2cartn(cart2sphn(c)), c, tol);
%!	end
