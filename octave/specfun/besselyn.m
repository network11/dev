%% y = besselyn(n, l, r)
%%	Compute n-dimensional l-order Bessel functions of the second kind.

function y = besselyn(n, l, x)
	y = gamma(n/2)./(x/2).^(n/2-1) .* bessely(l+n/2-1,x);
end

%!function y = p(l,m,x)
%!	y = legendren(2,l,m,x);
%!endfunction
%!test
%!	dr = 1e-2; [l,r] = ndgrid(-3:.5:3, 3:dr:7);
%!	d = -gradient(besselh(l,r)./r.^l,dr).*r.^l - besselh(l+1,r);
%!	assert(max(abs(d(:,3:end-2)(:))), 0, 1e-4);
%!test
%!	dr = 1e-2; [l,r] = ndgrid(-3:.5:3, 3:dr:7);
%!	d = +gradient(besselh(l,r).*r.^l,dr)./r.^l - besselh(l-1,r);
%!	assert(max(abs(d(:,3:end-2)(:))), 0, 1e-4);
%!test
%!	t = deg2rad(5:.1:180-5); s = sin(t);
%!	for l = .5:.5:3 for m = -l:l-1
%!		c = ((l-m)*(l+m+1))^.5;
%!		d = -gradient(p(l,m,t)./s.^m,t).*s.^m / c - p(l,m+1,t);
%!		assert(max(abs(d(3:end-2))), 0, 1e-3);
%!	end end
%!test
%!	t = deg2rad(30:.1:180-30); s = sin(t);
%!	for l = .5:.5:3 for m = 1-l:l
%!		c = ((l+m)*(l-m+1))^.5;
%!		d = +gradient(p(l,m,t).*s.^m,t)./s.^m / c - p(l,m-1,t);
%!		assert(max(abs(d(3:end-2))), 0, 1e-3);
%!	end end

%!test
%!	r = rand(1,1e3)*9;
%!	t = rand(1,1e3)*pi;
%!	for n = 2:5
%!		x = exp(1j.*r.*cos(t));
%!		y = 0; for l = 0:25
%!			y += 1j^l.*besseljn(n,l,r).*legendren(n-1,l,0,t).*legendren(n-1,l,0,0);
%!		end
%!		assert(x, y, eps^.5);
%!	end
%!test
%!	a = rand(1,1e3)*5;
%!	r = rand(1,1e3)*5;
%!	t = rand(1,1e3)*pi;
%!	ra = sqrt(r.^2-2*r.*a.*cos(t)+a.^2);
%!	for n = 2:5
%!		x = besseljn(n,0,ra);
%!		y = 0; for l = 0:15
%!			y += besseljn(n,l,r).*legendren(n-1,l,0,t).*besseljn(n,l,a).*legendren(n-1,l,0,0);
%!		end
%!		assert(x, y, eps^.5);
%!	end
%!test
%!	a = rand(1,1e3)*pi;
%!	t = rand(1,1e3)*pi;
%!	p = rand(1,1e3)*pi;
%!	ta = acos(cos(t).*cos(a)+sin(t).*sin(a).*cos(p));
%!	for n = 2:5 for l = 0:5
%!		x = legendren(n,l,0,ta).*legendren(n,l,0,0);
%!		y = 0; for m = 0:l
%!			y += legendren(n,l,m,t).*legendren(n-1,m,0,p).*legendren(n,l,m,a).*legendren(n-1,m,0,0);
%!		end
%!		assert(x, y, eps^.5);
%!	end end

%!demo
%!	x = -10:.05:10;
%!	a = [-2:.05:0 0:.05:2];
%!	for a = flip(a)
%!		plot(x,besselj(a,x)); grid on; ylim([-2 2]); title(a);
%!		drawnow;
%!	end
%!demo
%!	x = -10:.05:10;
%!	k = exp(1j*deg2rad(-180:180));
%!	subplot(221); h(1:2) = plot(x,x,x,x); grid on; ylim([-1 1]*3); title J;
%!	subplot(222); h(3:4) = plot(x,x,x,x); grid on; ylim([-1 1]*3); title Y;
%!	subplot(223); h(5:6) = plot(x,x,x,x); grid on; ylim([-1 1]*3); title H;
%!	subplot(224); h(7) = plot(0:1); grid on; axis([-1 1 -1 1]);
%!	for k = k
%!		y = besselj(0,k*x); set(h(1:2),{'ydata'},{real(y);imag(y)});
%!		y = bessely(0,k*x); set(h(3:4),{'ydata'},{real(y);imag(y)});
%!		y = besselh(0,k*x); set(h(5:6),{'ydata'},{real(y);imag(y)});
%!		set(h(7),{'xdata' 'ydata'},{real([0 k]) imag([0 k])});
%!		drawnow;
%!	end

%!demo
%!	n = (2:5).^2;
%!	x = -.9:.05:.9;
%!	for k = 0:max(n)
%!		y = legendre(k,x,'norm')(1,:); y2(:,:,k+1) = y'*y;
%!	end
%!	i = 0; for n = n subplot(2,2,++i);
%!		surf(x,x,sum(y2(:,:,1:n+1),3)); grid on; title(n);
%!		zlim([-5 20]); shading interp;
%!	end
