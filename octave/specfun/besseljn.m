%% j = besseljn(n, l, r)
%%	Compute n-dimensional l-order Bessel functions of the first kind.

function j = besseljn(n, l, x)
	j = gamma(n/2)./(x/2).^(n/2-1) .* besselj(l+n/2-1,x);
end

%!shared tol
%!	tol = eps^.5;
%!test
%!	ll = randn(9,1); xx = randn(9,1); xx = [xx;1j*xx;(xx+1j*xx')(:)];
%!	x = xx;
%!	assert(besseljn(1,0,x), cos(x), -tol);
%!	assert(besselyn(1,0,x), sin(x), -tol);
%!	assert(besselhn(1,0,x), exp(1j*x), -tol);
%!	assert(besseljn(1,1,x), sin(x), -tol);
%!	assert(besselyn(1,1,x), -cos(x), -tol);
%!	assert(besselhn(1,1,x), -1j*exp(1j*x), -tol);
%!	assert(besseljn(3,0,x), sin(x)./x, -tol);
%!	assert(besselyn(3,0,x), -cos(x)./x, -tol);
%!	assert(besselhn(3,0,x), -1j*exp(1j*x)./x, -tol);
%!	assert(besseljn(3,-1,x), cos(x)./x, -tol);
%!	assert(besselyn(3,-1,x), sin(x)./x, -tol);
%!	assert(besselhn(3,-1,x), exp(1j*x)./x, -tol);
%!	[l,x] = ndgrid(ll, xx);
%!	assert(besseljn(2,l,x), besselj(l,x), -tol);
%!	assert(besselyn(2,l,x), bessely(l,x), -tol);
%!	assert(besselhn(2,l,x), besselh(l,x), -tol);
%!	[n,l,x] = ndgrid(1:9, ll, xx);
%!	assert(besselhn(n,l,x), besseljn(n,l,x)+1j*besselyn(n,l,x), -tol);
%!test
%!	[n,l,x] = ndgrid(1:9, -9:9, rande(1,9));
%!	assert(besseljn(n,l,x), (-1).^l.*besseljn(n,l,-x), -tol);
%!	assert(besselyn(n,l,x), (-1).^(l+n).*real(besselyn(n,l,-x)), -tol);
%!	assert(besselhn(n,l,x), (-1).^(l+n+1).*conj(besselhn(n,l,-x)), -tol);
%!	assert(besselhn(n,l,x), (-1).^(1-n/2-l).*besselhn(n,2-n-l,x), -tol);
%!test
%!	dr = 1e-2; [l,r,n] = ndgrid(0:3, 3:dr:7, 1:4);
%!	h = besselhn(n,l,r);
%!	hr = gradient(h./r.^l,dr,1,1).*r.^l;
%!	d = hr(1:end-1,:,:) + h(2:end,:,:);
%!	assert(max(abs(d(:,3:end-2,:)(:))), 0, 1e-4);
%!test
%!	dr = 1e-3; [r,n,l,k] = ndgrid(3:dr:7, 1:4, -3:3, [1 1j]);
%!	[r,n,l,k] = deal(cellfun(@(x){x(:,:).'}, {r,n,l,k}){:});
%!	h = besselhn(n,l,k.*r);
%!	hr  = gradient(h ,dr,1);
%!	hrr = gradient(hr,dr,1);
%!	d = hrr + (n-1)./r.*hr + (k.^2-l.*(l+n-2)./r.^2).*h;
%!	assert(max(abs(d(:,3:end-2)(:))), 0, 1e-4);
%!test
%!	dt = dr = 1e-2; [t,r,m,n,k] = ndgrid(5:dt:7,0:dr:2, 1:3,1:3, [1 1j]);
%!	[t,r,m,n,k] = deal(cellfun(@(x){x(:,:,:)}, {t,r,m,n,k}){:});
%!	h = besselhn(m+n,0,k.*sqrt(t.^2-r.^2));
%!	[hr,ht] = gradient(h ,dr,dt,1);
%!	[~,htt] = gradient(ht,dr,dt,1);
%!	hrr     = gradient(hr,dr,dt,1);
%!	d = htt + (m-1)./t.*ht - hrr - (n-1)./r.*hr + k.^2.*h;
%!	assert(max(abs(d(3:end-2,3:end-2,:)(:))), 0, 1e-4);

%!demo
%!	[x,l] = ndgrid(-20:1/16:20, 0:3); yl = [-1.5 1.5];
%!	for n = 1:4 figure(n);
%!		j = besseljn(n,l,x); y = besselyn(n,l,x); h = besselhn(n,l,x);
%!		subplot(221); plot(x,j); ylim(yl); grid on; title J;
%!		subplot(222); plot(x,y); ylim(yl); grid on; title Y;
%!		subplot(223); plot(x,real(h)); ylim(yl); grid on; title Re(H);
%!		subplot(224); plot(x,imag(h)); ylim(yl); grid on; title Im(H);
%!	end

%!demo
%!	a = -8:1/16:8; b = -3:1/16:3; c = a+b'*1j; l = 2;
%!	f = @(z) imshow(vec(-1:1,3)/l.*z,'xdata',a,'ydata',b);
%!	for n = 1:4 figure(n);
%!		x = besseljn(n,0,c); y = besselyn(n,0,c); z = besselhn(n,0,c);
%!		subplot(321); f(real(x)); axis on xy; title Re(J);
%!		subplot(322); f(imag(x)); axis on xy; title Im(J);
%!		subplot(323); f(real(y)); axis on xy; title Re(Y);
%!		subplot(324); f(imag(y)); axis on xy; title Im(Y);
%!		subplot(325); f(real(z)); axis on xy; title Re(H);
%!		subplot(326); f(imag(z)); axis on xy; title Im(H);
%!	end

%!demo
%!	l = [-4 4]; xl = [l -1 1];
%!	for n = 1:4 figure(n);
%!		dt = df = min(1/8, 2^n/64);
%!		t = -.5/df:dt:.5/df-1e-9; f = -.5/dt:df:.5/dt-1e-9;
%!		i = numel(t)/2+1; i = [':' {i}(ones(1,n-1))];
%!		if (n>1) tt = hypot(nthargout(1:n,@ndgrid,t){:});
%!		else tt = abs(t); end
%!		t1 = dt/2:dt/2:tt(1); w = exp(-pi*(2*df*t1).^2);
%!		x = interp1(t1,besseljn(n,0,2*pi*t1).*w,tt,'extrap');
%!		y = fftshift(fftn(ifftshift(x)))*dt^n;
%!		z = 2*df\exp(-pi*(2*df\(abs(f)-1)).^2)/spharea(n-1);
%!		assert(max(abs(imag(x(:)))), 0, eps^.5);
%!		assert(max(abs(imag(y(:)))), 0, eps^.5);
%!		subplot(321); plot(t,x(i{:})); grid on; axis(xl);
%!		subplot(322); plot(f,y(i{:}),f,z); grid on; xlim(l);
%!		x = interp1(t1,besselyn(n,0,2*pi*t1).*w,tt,'extrap');
%!		y = fftshift(fftn(ifftshift(x)))*dt^n;
%!		z = 2/pi./(1-f.^2)/spharea(n-1);
%!		assert(max(abs(imag(x(:)))), 0, eps^.5);
%!		assert(max(abs(imag(y(:)))), 0, eps^.5);
%!		subplot(323); plot(t,x(i{:})); grid on; axis(xl);
%!		subplot(324); plot(f,y(i{:}),f,z); grid on; xlim(l);
%!		x = interp1(t1,1j^(n-1)*besselhn(n,0,2j*pi*t1),tt,'extrap');
%!		y = fftshift(fftn(ifftshift(x)))*dt^n;
%!		z = 2/pi./(1+f.^2)/spharea(n-1);
%!		assert(max(abs(imag(x(:)))), 0, eps^.5);
%!		assert(max(abs(imag(y(:)))), 0, eps^.5);
%!		subplot(325); plot(t,x(i{:})); grid on; axis(xl);
%!		subplot(326); plot(f,y(i{:}),f,z); grid on; xlim(l);
%!	end

%!demo
%!	dt = df = 1/128; t = -.5/df:dt:.5/df-1e-9; f = -.5/dt:df:.5/dt-1e-9;
%!	tt = 2*pi*abs(t); ts = 1j*sign(t); ff = f.^2; fs = sign(f);
%!	fun = @(n,f) beta((n-1)/2,1/2)\f.^((n-3)/2).*(f>0);
%!	for c = {
%!		 besseljn(2,0,tt)	fun(2,1-ff)
%!		 besseljn(3,0,tt)	fun(3,1-ff)
%!		 besseljn(4,0,tt)	fun(4,1-ff)
%!		 besseljn(5,0,tt)	fun(5,1-ff)
%!		 besseljn(6,0,tt)	fun(6,1-ff)
%!		 besselhn(2,0,tt*1j)*1j	fun(2,ff+1)
%!		-besselyn(2,0,tt)	fun(2,ff-1)
%!		 besseljn(2,2-2,tt).*ts	fun(2,ff-1).*fs
%!		 besseljn(3,2-3,tt).*ts	fun(3,ff-1).*fs
%!		 besseljn(4,2-4,tt).*ts	fun(4,ff-1).*fs-fun(4,ff).*fs
%!		}' [x,z] = c{:};
%!		x(end/2+1) = mean(x(end/2+[0 2])); x(1) = 0;
%!		y = fftshift(fft(ifftshift(x)))*dt;
%!		subplot(221); plot(t,real(x)); grid on; axis([-4 4 -1 1]);
%!		subplot(222); plot(t,imag(x)); grid on; axis([-4 4 -1 1]);
%!		subplot(223); plot(f,real([y;z])); grid on; axis([-4 4 -1 1]);
%!		subplot(224); plot(f,imag([y;z])); grid on; axis([-4 4 -1 1]);
%!		drawnow; pause(.5);
%!	end

%!demo
%!	x = 0:.01:12; n = 1:4; k = 2*(0:11)';
%!	a = -1./k./(k+n-2); a(1,:) = 1; a = cumprod(a);
%!	for n = n subplot(2,2,n);
%!		plot(x,cumsum(a(:,n).*x.^k),x,besseljn(n,0,x),'k');
%!		grid on; ylim([-1 1]*1.5); title(n);
%!	end
