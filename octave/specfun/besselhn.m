%% h = besselhn(n, l, r)
%%	Compute n-dimensional l-order Bessel functions of the third kind.

function h = besselhn(n, l, x)
	h = gamma(n/2)./(x/2).^(n/2-1) .* besselh(l+n/2-1,x);
end
