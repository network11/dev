%% p = hermiteen(n, l, m)
%% h = hermiteen(n, l, m, r)
%%	Compute n-dimensional l-degree m-order Hermite_e polynomial.

function p = hermiteen(n, l, m, x)
	assert(mod(l-m,2) == 0);
	k = l-m-2:-2:0;
	r = (k+2).*(k+m+m+n)./(k+m-l);
	p(1+[l-m k]) = cumprod([1 r]);
	p = p(end:-1:1);
	if (nargin > 3)
		p = polyval(p,x) .* x.^m;
	end
end

%!shared tol
%!	tol = eps^.5;
%!test
%!	for n = 1:6 for l = 0:5 for m = l:-2:0
%!		p = hermiteen(n,l,m);
%!		assert(size(p), [1 1+l-m]);
%!		assert(p(1), 1);
%!		assert(p, round(p));
%!		assert(all(p(2:2:end) == 0));
%!		assert(all(p(1:4:end) > 0));
%!		assert(all(p(3:4:end) < 0));
%!	end end end
%!test
%!	r = randn(1,9);
%!	for l = 0:7 m = mod(l,2);
%!		assert([hermiteen(1,l,m) zeros(1,m)], hermitee(l));
%!		assert(hermiteen(1,l,m,r), hermitee(l,r));
%!	end
%!test
%!	for n = 1:6 for l = 0:5 for m = l:-2:0
%!		assert(hermiteen(n,l,m), hermiteen(2,l+n/2-1,m+n/2-1));
%!	end end end
%!test
%!	for n = 1:6 for l = 1:5 for m = l-2:-2:0
%!		x = polyder(hermiteen(n,l,m));
%!		y = [(l-m)*hermiteen(n,l-1,m+1) 0];
%!		assert(y, x);
%!	end end end
%!test
%!	for n = 1:6 for l = 1:5 for m = l:-2:1
%!		x = polyder([hermiteen(n,l,m) zeros(1,2*m+n-2)]);
%!		y = [(l+m+n-2)*hermiteen(n,l-1,m-1) zeros(1,2*m+n-3)];
%!		assert(y, x);
%!	end end end
%!test
%!	% r*D^2 + (2*m+n-1-r^2)*D + (l-m)*r = 0
%!	for n = 1:6 for l = 0:5 for m = l:-2:0
%!		p = [0 0 hermiteen(n,l,m)];
%!		pd = polyder(p); pdd = polyder(pd);
%!		d = conv(pdd,[0 0 1 0])+conv(pd,[-1 0 2*m+n-1])+conv(p,[l-m 0]);
%!		assert(d == 0);
%!	end end end
%!test
%!	for n = 1:6 for L = 0:5 for l = L:-2:0 for m = l:-2:0
%!		P = hermiteen(n,L,m); p = hermiteen(n,l,m);
%!		f = @(r)polyval(P,r).*polyval(p,r).*exp(-.5*r.^2).*r.^(2*m+n-1);
%!		q = (L==l)*2^(l+n/2-1)*gamma((l+m+n)/2)*gamma((l-m+2)/2);
%!		assert(quad(f,0,inf), q, tol);
%!	end end end end

%!demo
%!	r = -6:.01:6;
%!	for n = 1:2 figure(n);
%!	i = 0; for l = [0:3 (4:7).^2]; subplot(4,2,++i);
%!	h = []; for m = unique((l:-2:0)([1 end]))
%!		q = prod(l+m+n-2:-2:n)*prod(l-m:-2:2);
%!		h = [h;hermiteen(n,l,m,sqrt(4*pi)*r).*exp(-pi*r.^2)/sqrt(q)];
%!	end
%!		plot(r,h,sqrt(l/pi)*[-1 1;-1 1],[-1;1],'k'); grid on; title(l);
%!	end end
%!demo
%!	r = -3:.01:3;
%!	for n = 1:2 figure(n);
%!	i = 0; for l = 0:7; subplot(4,2,++i);
%!	h = []; for m = flip(l:-2:0)
%!		q = prod(l+m+n-2:-2:n)*prod(l-m:-2:2);
%!		h = [h;hermiteen(n,l,m,sqrt(4*pi)*r).*exp(-pi*r.^2)/sqrt(q)];
%!	end
%!		plot(r,h,sqrt(l/pi)*[-1 1;-1 1],[-1;1],'k'); grid on; title(l);
%!	end end

%!demo
%!	x = -3:.01:3; [t,r] = cart2pol(nthargout(1:2,@ndgrid,x){:});
%!	f = @(z) imshow(vec(-1:1,3).*z,'xdata',x,'ydata',x);
%!	for l = 0:9 for m = l:-2:-l
%!		q = prod(l+m:-2:2)*prod(l-m:-2:2);
%!		h = hermiteen(2,l,abs(m),sqrt(4*pi)*r).*exp(-pi*r.^2)/sqrt(q);
%!		p = legendren(1,abs(m),m<0,t);
%!		f(h'.*p'); axis on xy; title(num2str([l m]));
%!		c = sqrt(l/pi)*exp(1j*deg2rad(0:360));
%!		hold on; plot(real(c),imag(c)); hold off;
%!		drawnow;
%!	end end
