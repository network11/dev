%% v = sphvolume(n)
%%	Compute the volume of the unit-radius n-ball.

function v = sphvolume(n)
	v = pi.^(n/2) ./ gamma(n/2+1);
end

%!test
%!	x = randn(1,30)*3;
%!	assert(gamma(1+x).*gamma(1-x), 1./sinc(x), eps^.5);
