%% p = hermitee(n)
%% y = hermitee(n, x)
%%	Compute the coefficients of the Hermite_e polynomial.
%%	If the value is specified, the polynomial is also evaluated.

function p = hermitee(n, x)
	k = n-2:-2:0;
	r = (k+2).*(k+1)./(k-n);
	p(1+[n k]) = cumprod([1 r]);
	p = p(end:-1:1);
	if (nargin > 1)
		p = polyval(p, x);
	end
end

%!shared tol
%!	tol = eps^.5;
%!test
%!	for n = 0:7
%!		p = hermitee(n);
%!		assert(size(p), [1 n+1]);
%!		assert(p(1), 1);
%!		assert(p, round(p));
%!		assert(all(p(2:2:end) == 0));
%!		assert(all(p(1:4:end) > 0));
%!		assert(all(p(3:4:end) < 0));
%!	end
%!test
%!	for n = 1:7
%!		p = hermitee(n);
%!		assert(polyder([0 p]), [0 n*hermitee(n-1)]);
%!	end
%!test
%!	for n = 0:7
%!		p = hermitee(n);
%!		assert(hermitee(n+1), [p 0]-polyder([0 0 p]));
%!	end
%!test
%!	for n = 0:7
%!		m = 0:n/2;
%!		k = zeros(1,n+1); k(1:2:end) = (-.5).^m.*factorial(n)./factorial(m)./factorial(n-m*2);
%!		assert(hermitee(n), k);
%!	end
%!test
%!	for N = 0:7 for n = 0:N
%!		P = hermitee(N); p = hermitee(n);
%!		f = @(x)polyval(P,x).*polyval(p,x).*exp(-.5*x.^2);
%!		assert(quad(f,-inf,inf), sqrt(2*pi)*factorial(n)*(N==n), tol);
%!	end end
%!test
%!	t = -5:.1:5-1e-3;
%!	for n = 0:7
%!		x = hermitee(n,sqrt(4*pi)*t).*exp(-pi*t.^2);
%!		y = fftshift(fft(ifftshift(x)))/sqrt(numel(x));
%!		assert(y, (-1j)^n*x, tol);
%!	end

%!test
%!	n = 25;
%!	t = -9:9;
%!	for k = setdiff(0:n, 0:4:n)
%!		y = hermitee(k,sqrt(4*pi)*t).*exp(-pi*t.^2)/sqrt(factorial(k));
%!		assert(sum(y), 0, tol);
%!	end

%!test
%!	n = 9;
%!	t = (-3:1e-3:3)';
%!	for k = 0:n
%!		y = hermitee(k,sqrt(4*pi)*t).*exp(-pi*t.^2)/sqrt(factorial(k));
%!		y(:,end+1) = gradient(y(:,end),t)/(2*pi);
%!		y(:,end+1) = gradient(y(:,end),t)/(2*pi);
%!		y1(:,:,k+1) = y;
%!	end
%!	k = vec(1:n,3);
%!	assert(t.*y1(:,1,k+1)+y1(:,2,k+1), sqrt(k/pi).*y1(:,1,k), 1e-4);
%!	k = vec(0:n-1,3);
%!	assert(t.*y1(:,1,k+1)-y1(:,2,k+1), sqrt((k+1)/pi).*y1(:,1,k+2), 1e-4);
%!	k = vec(0:n,3);
%!	assert(t.^2.*y1(:,1,k+1)-y1(:,3,k+1), (k+.5)/pi.*y1(:,1,k+1), 1e-4);

%!xtest
%!	n = 9;
%!	t = -3:1e-3:3;
%!	for z = randn(1,n)+1j*randn(1,n)
%!		y = exp(-pi*((t-z).^2+(abs(z).^2-z.^2)/2));
%!		assert(t.*y+gradient(y,t)/(2*pi), z.*y, 1e-3);
%!	end

%!demo
%!	n = [0:3 (4:7).^2];
%!	t = -6:.01:6;
%!	i = 0; for n = n subplot(4,2,++i);
%!		y = hermitee(n,sqrt(4*pi)*t).*exp(-pi*t.^2)/sqrt(factorial(n));
%!		plot(t,y,sqrt(n/pi)*[-1 1;-1 1],[-1;1]); grid on; title(n);
%!	end

%!demo
%!	n = (1:4).^2;
%!	t = -3:.1:3;
%!	for k = 0:max(n)
%!		y = hermitee(k,sqrt(4*pi)*t).*exp(-pi*t.^2)/sqrt(factorial(k));
%!		y2(:,:,k+1) = y'*y;
%!	end
%!	i = 0; for n = n subplot(2,2,++i);
%!		surf(t,t,sum(y2(:,:,1:n+1),3)); zlim([-1 4]); grid on; title(n);
%!		shading interp;
%!	end

%!demo
%!	n = sqrt(0:9); [x,y] = ndgrid(n);
%!	plot(x,y,'or',n.*exp(1j*deg2rad(0:90)'),'b'); grid on; axis square;
%!demo
%!	t = -3:.01:3;
%!	f = @(z) imshow(vec(-1:1,3).*z,'xdata',t,'ydata',t);
%!	for n = 0:9
%!		y = hermitee(n,sqrt(4*pi)*t).*exp(-pi*t.^2)/sqrt(factorial(n));
%!		y1(:,n+1) = y;
%!	end
%!	for n = 0:9 for y = 0:n x = n-y;
%!		f(y1(:,x+1)'.*y1(:,y+1)); axis on xy; title(num2str([x y]));
%!		c = sqrt(n/pi)*exp(1j*deg2rad(0:360));
%!		p = sqrt([x y]/pi)*[1;1j]*[-1 1];
%!		hold on; plot(real(c),imag(c),real(p),imag(p)); hold off;
%!		drawnow;
%!	end end
