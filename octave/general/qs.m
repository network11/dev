%% [q,s] = qs(a)
%%	Compute the QS factorization (the polar decomposition).

function [q,s] = qs(a)
	[u,s,v] = svd(a);
	q = u*v';
	s = v*s*v';
end

%!shared tol
%!	tol = eps^.5;
%!test
%!	for n = 1:8
%!		a = randn(n)+1j*randn(n);
%!		[u,p] = qs(a);
%!		assert(u*p, a, tol);
%!		assert(u'*u, eye(n), tol);
%!		assert(isdefinite(p, tol));
%!		assert(p, sqrtm(a'*a), tol);
%!		assert(det(p), abs(det(a)), tol);
%!		assert(det(u), sign(det(a)), tol);
%!	end

%!test
%!	for n = 1:8
%!		a = randn(n)+1j*randn(n);
%!		[v,l] = eig(a);
%!		assert(v*l, a*v, tol);
%!		assert(diag(v'*v), ones(n,1), tol);
%!		assert(isdiag(l));
%!		b = a-a';
%!		assert(ishermitian(b, 'skew'));
%!		[v,l] = eig(b);
%!		assert(v'*v, eye(n), tol);
%!		assert(real(l), zeros(n), tol);
%!		b = a+a';
%!		assert(ishermitian(b));
%!		[v,l] = eig(b);
%!		assert(v'*v, eye(n), tol);
%!		assert(isreal(l));
%!		b = a*a';
%!		assert(isdefinite(b));
%!		[v,l] = eig(b);
%!		assert(v'*v, eye(n), tol);
%!		assert(l, abs(l));
%!		[u,s,v] = svd(b);
%!		assert(u, v, tol);
%!		b = orth(a);
%!		assert(b'*b, eye(n), tol);
%!		[v,l] = eig(b);
%!		assert(v'*v, eye(n), tol);
%!		assert(abs(l), eye(n), tol);
%!		[u,s,v] = svd(b);
%!		assert(s, eye(n), tol);
%!	end

%!test
%!	for n = 1:8 for m = 1:8
%!		a = randn(n,m)+1j*randn(n,m);
%!		[u,s,v] = svd(a);
%!		assert(u*s*v', a, tol);
%!		assert(u'*u, eye(n), tol);
%!		assert(v'*v, eye(m), tol);
%!		assert(s, abs(s));
%!		assert(isdiag(s));
%!		[q,r] = qr(a);
%!		assert(q*r, a, tol);
%!		assert(q'*q, eye(n), tol);
%!		assert(istriu(r));
%!		[l,u] = lu(a);
%!		assert(l*u, a, tol);
%!		assert(det(l'*l) >= 1 - tol);
%!		assert(istriu(u));
%!		b = a*a';
%!		assert(isdefinite(b + eye(n)*tol));
%!	end end

%!function b = ginv(a, tol)
%!	[u,s,v] = svd(a);
%!	t = ifelse(s>tol, 1./s, 0)';
%!	b = v*t*u';
%!endfunction
%!test
%!	for m = 1:5 for n = 1:5 for r = 0:min(m,n)
%!		a = (randn(m,r)+1j*randn(m,r)) * (randn(r,n)+1j*randn(r,n));
%!		b = ginv(a, tol);
%!		assert(size(a), [m n]); assert(rank(a), r);
%!		assert(size(b), [n m]); assert(rank(b), r);
%!		assert(pinv(a), b, tol);
%!		assert(pinv(b), a, tol);
%!		assert(a*b*a, a, tol); assert((a*b)', a*b, tol);
%!		assert(b*a*b, b, tol); assert((b*a)', b*a, tol);
%!	end end end
