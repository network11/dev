%% cis(x)
%%	Compute the cosine-plus-i-sine function.
%%	Return cos(x) + i*sin(x).

function y = cis(x)
	y = cos(x) + 1j*sin(x);
end

%!test
%!	x = randn(1,5) + 1j*randn(1,5);
%!	assert(cis(x), exp(1j*x), eps^.5);
