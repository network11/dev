%% y = bspline(xi, yi, x, k)
%%	y(x): k-order B-spline with knot vector xi and control points yi

function y = bspline(xi, yi, x, k)
	y = 0;
	for i = 1:numel(yi)
		y = y + yi(i) * bspline_i(xi, i, k, x);
	end
end

function y = bspline_i(xi, i, k, x)
	if (k > 1)
		y = (x-xi(i))./(xi(i+k-1)-xi(i)).*bspline_i(xi,i,k-1,x) + ...
		  (xi(i+k)-x)./(xi(i+k)-xi(i+1)).*bspline_i(xi,i+1,k-1,x);
	else
		y = (xi(i)<=x & x<xi(i+1));
	end
end

%!demo
%!	xi = 0:4;
%!	x = xi(1)-1:.1:xi(end)+1;
%!	kmax = numel(xi)-1;
%!	for k = 1:kmax
%!	for yi = eye(numel(xi)-k)
%!		y = bspline(xi, yi, x, k);
%!		subplot(kmax,1,k); hold on; plot(x,y); grid on; ylim(0:1);
%!	end
%!	end

%!test
%!	n = 1e3;
%!	h = ones(1,n+1); h([1 end]) /= 2;
%!	a = h; h /= sum(h);
%!	for k = 2:9
%!		a = conv(a, h);
%!		b = bspline(0:k, 1, 0:1/n:k, k);
%!		assert(sum(abs(a-b)) < 1.01/n);
%!	end
