%% adjugate(A)
%%	Compute the adjugate matrix of A.

function a = adjugate(a)
	a = polyvalm(poly(a)(1:end-1),a) * -(-1)^length(a);
end

%!test
%!	for n = 0:9
%!		a = randn(n) + 1j*randn(n);
%!		b = adjugate(a);
%!		d = det(a)*eye(n);
%!		assert(a*b, d, eps^.5);
%!		assert(b*a, d, eps^.5);
%!	end

%!function t = gtrace(a,k)
%!	if (k == 0) t = 1; return end
%!	t = 0; for c = nchoosek(1:length(a),k)' t += det(a(c,c)); end
%!endfunction
%!test
%!	for n = 1:9
%!		a = randn(n) + 1j*randn(n);
%!		t = arrayfun(@(k)gtrace(a,k), 0:n);
%!		assert(t([2 end]), [trace(a) det(a)], eps^.5);
%!		assert(t, poly(-a), eps^.5);
%!		assert(polyvalm(poly(a),a), zeros(n), eps^.5);
%!	end
