%% froots(fun, edges)
%%	Find the roots of a univariate function.

function x = froots(fun, x)
	assert(issorted(x));
	y = arrayfun(fun, x);
	i = find(diff(sign(y)([1:end-1;2:end])));
	x = arrayfun(@(i)fzero(fun,x(i:i+1)), i);
	x = unique(x);
end

%!assert(froots(@sin,0:10), 0:pi:10, eps^.5);
%!assert(froots(@cos,0:10), pi/2:pi:10, eps^.5);

%!demo
%!	x = 0:.01:12; x0 = x(1):x(end);
%!	f = @besselj; f = {f @(n,x)(f(n-1,x)-f(n+1,x))/2};
%!	for n = 0:3 for i = 1:2 subplot(4,2,n*2+i);
%!		plot(x,f{1}(n,x)); grid on; ylim([-1 1]);
%!		xticks(froots(@(x)f{i}(n,x),x0));
%!	end end
