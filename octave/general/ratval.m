%% x = ratval(b)
%% x = ratval(b, a)
%% [x,y] = ratval(...)
%%	Evaluate the continued fraction
%%		 x         a1|   a2|         an|
%%		--- = b0 + --- + --- + ... + ---
%%		 y         |b1   |b2         |bn

function [x,y] = ratval(b, a=ones(1,numel(b)-1))
	assert(isvector(a)); a = vec(a, 2);
	assert(isvector(b)); b = vec(b, 2);
	xy = [b(end) 1];
	for ba = [b(end-1:-1:1); a(end:-1:1)]
		xy = xy * [ba [1;0]];
		xy = xy / gcd(num2cell(xy){:});
	end
	[x,y] = num2cell(xy){:};
	if (nargout < 2) x = x / y; end
end

%!shared tol
%!	tol = eps^.5;
%!xtest
%!	for n = 0:9
%!		b = randi([-9 9], 1,n+1);
%!		a = randi([-9 9], 1,n);
%!		[xi,yi] = ratcon(b,a);
%!		[xn,yn] = ratval(b,a);
%!		assert(xn/yn, xi(end)/yi(end));
%!		xyi = ratcon(b,a);
%!		xyn = ratval(b,a);
%!		assert(xyn, xyi(end), tol);
%!	end
%!assert(ratval(ones(1,25)), (1+sqrt(5))/2, tol);
%!assert(ratval([1 2*ones(1,15)]), sqrt(2), tol);
%!assert(ratval([0 1:2:31], [4 (1:15).^2]), pi, tol);
