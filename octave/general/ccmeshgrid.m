%% xy = ccmeshgrid(x)
%% xy = ccmeshgrid(x, y)

function xy = ccmeshgrid(varargin)
	[x, y] = meshgrid(varargin{:});
	xy = complex(x, y);
end

%!shared tol,x,y
%!	tol = eps^.5;
%!	x = randn(1,9)+1j*randn(1,9);
%!	y = randn(1,9)+1j*randn(1,9);
%!test
%!	assert(cos(x*1j), cosh(x), tol);
%!	assert(sin(x*1j), sinh(x)*1j, tol);
%!	assert(cosh(x*1j), cos(x), tol);
%!	assert(sinh(x*1j), sin(x)*1j, tol);
%!test
%!	assert(cos(x+y), cos(x).*cos(y)-sin(x).*sin(y), tol);
%!	assert(sin(x+y), sin(x).*cos(y)+cos(x).*sin(y), tol);
%!	assert(cosh(x+y), cosh(x).*cosh(y)+sinh(x).*sinh(y), tol);
%!	assert(sinh(x+y), sinh(x).*cosh(y)+cosh(x).*sinh(y), tol);
