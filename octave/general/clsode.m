%% [x, ...] = clsode(fcn, x_0, t, ...)

function [x, varargout] = clsode(fcn, x_0, varargin)
	fcn = @(x,t) z2x(vec(fcn(x2z(vec(x,2)),t),2));
	x_0 = z2x(vec(x_0,2));
	[x, varargout{1:nargout-1}] = lsode(fcn, x_0, varargin{:});
	x = x2z(x);
end

function z = x2z(x)
	z = complex(x(:,1:end/2), x(:,end/2+1:end));
end
function x = z2x(z)
	x = [real(z) imag(z)];
end

%!test
%!	for n = 1:9
%!		t = unique(randn(9,1)/2); c = complex(randn/2,randn*2);
%!		g = @(t)exp(c*t); h1 = @(x,~)c*x; h2 = @(~,t)c*exp(c*t);
%!		assert(clsode(h1,g(t(1)),t), g(t), 1e-5);
%!		assert(clsode(h2,g(t(1)),t), g(t), 1e-5);
%!	end

%!demo
%!	l = 4; r = pi/2; ff = {
%!		-l l	,@sin	@(x)+cos(x)
%!		-l l	,@cos	@(x)-sin(x)
%!		-r r	,@tan	@(x)+sec(x).^2
%!		 0 r*2	,@cot	@(x)-csc(x).^2
%!		-r r	,@sec	@(x)+tan(x).*sec(x)
%!		 0 r*2	,@csc	@(x)-cot(x).*csc(x)
%!		-1 1	,@asin	@(x)+sqrt(1-x.^2).\1
%!		-1 1	,@acos	@(x)-sqrt(1-x.^2).\1
%!		-l l	,@atan	@(x)+(1+x.^2).\1
%!		 0 l	,@acot	@(x)-(1+x.^2).\1
%!		 1 l	,@asec	@(x)+sqrt(x.^2-1).*abs(x).\1
%!		 1 l	,@acsc	@(x)-sqrt(x.^2-1).*abs(x).\1
%!		-l l	,@sinh	@(x)+cosh(x)
%!		-l l	,@cosh	@(x)+sinh(x)
%!		-l l	,@tanh	@(x)+sech(x).^2
%!		 0 l	,@coth	@(x)-csch(x).^2
%!		-l l	,@sech	@(x)-tanh(x).*sech(x)
%!		 0 l	,@csch	@(x)-coth(x).*csch(x)
%!		-l l	,@asinh	@(x)+sqrt(x.^2+1).\1
%!		 1 l	,@acosh	@(x)+sqrt(x.^2-1).\1
%!		-1 1	,@atanh	@(x)+(1-x.^2).\1
%!		 1 l	,@acoth	@(x)-(x.^2-1).\1
%!		 0 1	,@asech	@(x)-sqrt(1-x.^2).*x.\1
%!		 0 l	,@acsch	@(x)-sqrt(1+x.^2).*abs(x).\1
%!		};
%!	x = linspace(-l,l,l*1e3); l = [-l l -l l]; nn = [1:3;4:6];
%!	for n = 1:rows(ff)
%!		figure(ceil(n/6)); subplot(2,3,nn(1+mod(n-1,6)));
%!		[a,b,f,g] = ff{n,:}; t = linspace(a+.1,b-.1)';
%!		assert(lsode(@(~,t)g(t),f(t(1)),t), f(t), 1e-5);
%!		y = f(x); y = ifelse( imag(y),nan,y);
%!		z = g(x); z = ifelse(isnan(y),nan,z);
%!		plot(x,y,x,z); grid on; axis(l,'equal'); title(func2str(f));
%!	end

%!demo
%!	l = 4; r = pi/2; ff = {
%!		-l l	,@sin	@(x)-cos(x)
%!		-l l	,@cos	@(x)+sin(x)
%!		-r r	,@tan	@(x)+log(abs(sec(x)))
%!		 0 r*2	,@cot	@(x)-log(abs(csc(x)))
%!		-r r	,@sec	@(x)+asinh(tan(x)).*sign(sec(x))
%!		 0 r*2	,@csc	@(x)-asinh(cot(x)).*sign(csc(x))
%!		-1 1	,@asin	@(x)x.*asin(x)+sqrt(1-x.^2)
%!		-1 1	,@acos	@(x)x.*acos(x)-sqrt(1-x.^2)
%!		-l l	,@atan	@(x)x.*atan(x)-log(1+x.^2)/2
%!		 0 l	,@acot	@(x)x.*acot(x)+log(1+x.^2)/2
%!		 1 l	,@asec	@(x)x.*asec(x)-asinh(sqrt(x.^2-1))
%!		 1 l	,@acsc	@(x)x.*acsc(x)+asinh(sqrt(x.^2-1))
%!		-l l	,@sinh	@(x)+cosh(x)
%!		-l l	,@cosh	@(x)+sinh(x)
%!		-l l	,@tanh	@(x)-log(sech(x))
%!		 0 l	,@coth	@(x)-log(abs(csch(x)))
%!		-l l	,@sech	@(x)-acos(tanh(x))
%!		 0 l	,@csch	@(x)-acosh(abs(coth(x)))
%!		-l l	,@asinh	@(x)x.*asinh(x)-sqrt(x.^2+1)
%!		 1 l	,@acosh	@(x)x.*acosh(x)-sqrt(x.^2-1)
%!		-1 1	,@atanh	@(x)x.*atanh(x)+log(1-x.^2)/2
%!		 1 l	,@acoth	@(x)x.*acoth(x)+log(x.^2-1)/2
%!		 0 1	,@asech	@(x)x.*asech(x)+acos(sqrt(1-x.^2))
%!		 0 l	,@acsch	@(x)x.*acsch(x)+acosh(sqrt(1+x.^2))
%!		};
%!	x = linspace(-l,l,l*1e3); l = [-l l -l l]; nn = [1:3;4:6];
%!	for n = 1:rows(ff)
%!		figure(ceil(n/6)); subplot(2,3,nn(1+mod(n-1,6)));
%!		[a,b,f,g] = ff{n,:}; t = linspace(a+.1,b-.1)';
%!		assert(lsode(@(~,t)f(t),g(t(1)),t), g(t), 1e-5);
%!		y = f(x); y = ifelse( imag(y),nan,y);
%!		z = g(x); z = ifelse(isnan(y),nan,z);
%!		plot(x,y,x,z); grid on; axis(l,'equal'); title(func2str(f));
%!	end
