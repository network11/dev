%% P- = park(theta, OPT)
%% P = park(-theta, OPT)
%%	Compute the inverse Park transform matrix.
%%	The optional argument OPT selects the type:
%%		'e': Compute the Givens rotation matrix. [default]
%%		'h': Compute the Lorentz scaling matrix.

function h = park(t, opt='e')
	assert(isscalar(t));
	switch (opt)
	case 'e' c = cos (t); s = sin (t); h = [c -s;s c];
	case 'h' c = cosh(t); s = sinh(t); h = [c  s;s c];
	end
end

%!test
%!	tol = eps^.5;
%!	g1 = park(1,'e');
%!	h1 = park(1,'h');
%!	gp = park( pi/4);
%!	gn = park(-pi/4);
%!	for t = randn(1,9)
%!		g = park(t,'e');
%!		h = park(t,'h');
%!		assert(g, expm([0 -t;t 0]), tol);
%!		assert(h, expm([0  t;t 0]), tol);
%!		assert(det(g), 1, tol);
%!		assert(det(h), 1, tol);
%!		assert(inv(g), park(-t,'e'), tol);
%!		assert(inv(h), park(-t,'h'), tol);
%!		assert(g, g1^t, tol);
%!		assert(h, h1^t, tol);
%!		assert(h, gp*expm(diag([t -t]))*gn, tol);
%!		assert(h, gn*expm(diag([-t t]))*gp, tol);
%!	end

%!demo
%!	x = exp(1j*deg2rad(0:45:360-1)); x = [real(x);imag(x)];
%!	g = h = []; for t = (-1:4)/8
%!		g = cat(3,g,park(t,'e')*x);
%!		h = cat(3,h,park(t,'h')*x);
%!	end
%!	g = permute(g,3:-1:1);
%!	h = permute(h,3:-1:1);
%!	plot(x(1,:),x(2,:),'ro',
%!		g(:,:,1),g(:,:,2),'gx-',
%!		h(:,:,1),h(:,:,2),'b+-'); grid on; axis square;
