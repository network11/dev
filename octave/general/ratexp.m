%% b = ratexp(x, n, op)
%% b = ratexp([x y], n, op)
%%	Perform a continued fraction expansion
%%		 x          1|    1|          1|
%%		--- = b0 + --- + --- + ... + ---
%%		 y         |b1   |b2         |bn

function b = ratexp(x, n=99, op='round')
	if (isscalar(x)) x = [x 1]; end
	assert(numel(x), 2);
	b = []; for i = 0:n
		[b(end+1), x(end+1)] = divmod(x(end-1), x(end), op);
		if (x(end) == 0) break end
	end
end

%!test
%!	x = randn * 10;
%!	y = randi(intmax, 1,2);
%!	for f = {'fix','ceil','floor','round'} f = f{1};
%!		b = ratexp(x,99,f);
%!		assert(ratval(b), x, eps^.5);
%!		for n = 1:numel(b)
%!			assert(ratexp(x,n-1,f), b(1:n));
%!		end
%!		b = ratexp(y,99,f);
%!		assert(ratval(b), y(1)/y(2));
%!		for n = 1:numel(b)
%!			assert(ratexp(y,n-1,f), b(1:n));
%!		end
%!	end
%!assert(ratexp((1+sqrt(5))/2,10,'fix'), ones(1,11));
%!assert(ratexp(sqrt(2),10,'fix'), [1 2*ones(1,10)]);
%!assert(ratexp(sqrt(5),10,'fix'), [2 4*ones(1,10)]);
%!assert(ratexp(sqrt(3),10,'fix'), [1 repmat(1:2,1,5)]);
