%% x = ratcon(b)
%% x = ratcon(b, a)
%% [x,y] = ratcon(...)
%%	Return successive convergents of the continued fraction
%%		xi        a1|   a2|         ai|
%%		-- = b0 + --- + --- + ... + ---  (i = 0, 1, 2, ..., n)
%%		yi        |b1   |b2         |bi

function [x,y] = ratcon(b, a=ones(1,numel(b)-1))
	assert(isvector(a)); a = vec(a, 2);
	assert(isvector(b)); b = vec(b, 2);
	xy = [1 b(1); 0 1];
	for ab = [a; b(2:end)]
		xy(:,end+1) = xy(:,end-1:end) * ab;
	end
	xy(:,1) = [];
	[x,y] = num2cell(xy, 2){:};
	if (nargout < 2) x = x ./ y; end
end

%!test
%!	b = randi([-9 9], 1,9+1);
%!	a = randi([-9 9], 1,9);
%!	[xn,yn] = ratcon(b,a);
%!	xyn = ratcon(b,a);
%!	assert(xyn, xn./yn);
%!	for i = 1:numel(b)
%!		[xi,yi] = ratcon(b(1:i),a(1:i-1));
%!		xyi = ratcon(b(1:i),a(1:i-1));
%!		assert([xi;yi], [xn;yn](:,1:i));
%!		assert(xyi, xyn(1:i));
%!	end
%!test
%!	[x,y] = ratcon(ones(1,9));
%!	f = fibonacci(9+1);
%!	assert([x;y], f([2:end;1:end-1]));
