%% q = cquad(f, a, b, ...)

function q = cquad(fn, varargin)
	r = quad(@(x) real(fn(x)), varargin{:});
	i = quad(@(x) imag(fn(x)), varargin{:});
	q = complex(r, i);
end

%!test
%!	for n = 1:9
%!		a = randn; b = randn; c = randn+1j*randn;
%!		g = @(x)exp(c*x); h = @(x)c*exp(c*x);
%!		assert(cquad(h,a,b), g(b)-g(a), eps^.5);
%!		p = randn(1,n)+1j*randn(1,n); q = polyder(p);
%!		g = @(x)polyval(p,x); h = @(x)polyval(q,x);
%!		assert(cquad(h,a,b), g(b)-g(a), eps^.5);
%!	end
%!xtest
%!	for n = 1:9
%!		a = complex(3^randn,randn/2);
%!		b = complex(randn*2,randn/2);
%!		y = sqrt(a)*cquad(@(x)exp(-pi*a.*(x-b).^2),-inf,inf);
%!		assert(y, 1, eps^.5);
%!	end
%!test
%!	for n = 1:9
%!		a = 10^(rand+1.5);
%!		b = .1^(rand+1.5);
%!		assert(+1j*pi\cquad(@(x)1./(x-1j*b),-a,a), 1, 1e-3);
%!		assert(-1j*pi\cquad(@(x)1./(x+1j*b),-a,a), 1, 1e-3);
%!	end

%!test
%!	for n = -4:4 for x = randn(1,5)
%!		y = 2*pi\cquad(@(t)exp(1j*(x.*sin(t)-n.*t)),-pi,pi);
%!		assert(y, besselj(n,x), eps^.5);
%!	end end
%!test
%!	% SFFM spectrum
%!	n = 64; a = -2:.2:2; k = ifftshift(floor(center(1:n)'));
%!	x = exp(2j*pi*a.*sin(2*pi/n*k));
%!	y = bsxfun(@besselj,k,2*pi*a);
%!	assert(y, fft(x)/n, eps^.5);
%!	assert(sum(y), ones(size(a)), eps^.5);
%!	assert(sumsq(y), ones(size(a)), eps^.5);
