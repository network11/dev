%% p = butterpoly(n)
%%	Compute the coefficients of the normalized Butterworth polynomial.

function p = butterpoly(n)
	if (mod(n,2)) p = [1 1]; else p = 1; end
	for k = 1:2:n-1 p = conv(p,[1 2*sin(pi/2/n*k) 1]); end
end

%!test
%!	for n = 0:7
%!		p = butterpoly(n);
%!		assert(p(1), 1);
%!		assert(p, flip(p));
%!		assert(p, poly(-exp(1j*pi/2/n*(1-n:2:n-1))), eps^.5);
%!	end
%!test
%!	for n = 1:6
%!		p = butterpoly(n) .* 1j.^(n:-1:0); a = real(p); b = imag(p);
%!		den = conv(a,a) + conv(b,b);
%!		num = prepad(0:1,n*2);
%!		assert(den, [1 num], eps^.5);
%!	end

%!demo
%!	for n = 1:6 subplot(2,3,n);
%!		p = butterpoly(n); r = roots(p);
%!		polar(arg(r),abs(r),'x'); rticks(0:2);
%!		title(n); disp(polyout(p));
%!	end

%!demo
%!	w = logspace(-2,2); s = 1j*w;
%!	for n = 1:4 p = butterpoly(n); h(n,:) = 1./polyval(p,s); end
%!	p = unwrap(arg(h),[],2);
%!	subplot(221); semilogx(w,abs(h)); grid on;
%!	subplot(222); semilogx(w,rad2deg(p)); grid on; yticks(-360:45:0);
%!	subplot(223); semilogx(w,p./w); grid on;
%!	subplot(224); semilogx(w,gradient(p,w)); grid on;
