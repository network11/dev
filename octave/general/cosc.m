%% cosc(x)
%%	Return cos(pi*x)/x - sin(pi*x)/(pi*x^2).
%%	This is the derivative of sinc(x).

function y = cosc(x)
	y = zeros(size(x));
	i = (x ~= 0);
	if (any(i(:)))
		t = x(i);
		y(i) = (cos(pi*t) - sinc(t)) ./ t;
	end
end
