%% seconds = cstrptime(str, fmt)

function seconds = cstrptime(str, fmt)
	seconds = cellfun(@(s)mktime(strptime(s,fmt)), cellstr(str));
end
