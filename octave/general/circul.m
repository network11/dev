%% circul(v)
%%	Create a circulant matrix.

function c = circul(v)
	c = toeplitz(v, v([1 end:-1:2]));
end

%!function h = circ(h)
%!	if (numel(h) > 1) h = gallery('circul', double(h)).'; end
%!endfunction
%!test
%!	for n = 1:9
%!		x = randn(n,1)+1j*randn(n,1);
%!		h = randn(n,1)+1j*randn(n,1);
%!		H = circul(h);
%!		f = ifft(eye(n));
%!		assert(H*x, circconv(x,h), eps^.5);
%!		assert(H*f, f*diag(fft(h)), eps^.5);
%!		assert(H, circ(h));
%!	end

%!test
%!	for n = 1:6
%!		z = randn(n);
%!		m = 0; for p = circul(1:n) m += z(p,p); end
%!		assert(m, circul(m(:,1)), eps^.5);
%!		m = 0; for p = perms(1:n)' m += z(p,p); end
%!		assert(m, circul(m(min(2,1:end),1)), eps^.5);
%!	end
%!test
%!	for n = 1:6
%!		z = circul(randn(1,n)); z += z';
%!		f = cas(2*pi/n*(0:n-1)'*(0:n-1));
%!		assert(f*f, n*eye(n), eps^.5);
%!		assert(d=f\z*f, diag(diag(d)), eps^.5);
%!		z = randn*ones(n)+randn*eye(n);
%!		f = (n^.5-1)/(n-1)*ones(n)-n^.5*eye(n); f(1,:) = f(:,1) = 1;
%!		assert(f*f, n*eye(n), eps^.5);
%!		assert(d=f\z*f, diag(diag(d)), eps^.5);
%!		f = -eye(n); f(1,:) = f(:,1) = 1;
%!		g = ones(n)-n*eye(n); g(1,:) = g(:,1) = 1;
%!		assert(f*g, n*eye(n), eps^.5);
%!		assert(d=f\z*f, diag(diag(d)), eps^.5);
%!		assert(d=g\z*g, diag(diag(d)), eps^.5);
%!	end

%!test
%!	for n = 1:6
%!		assert(triu(ones(n))^-1, eye(n)-diag(ones(1,n-1),+1));
%!		assert(tril(ones(n))^-1, eye(n)-diag(ones(1,n-1),-1));
%!	end

%!xtest
%!	for n = 1:4
%!		a = randn(n); b = randn(n); c = randn(n); d = randn(n);
%!		x = [a b;c d]^-1;
%!		y = [(a-b/d*c)^-1 (c-d/b*a)^-1;(b-a/c*d)^-1 (d-c/a*b)^-1];
%!		assert(x, y, eps^.5);
%!	end
%!xtest
%!	for m = 1:4 for n = 1:4
%!		a = randn(m); b = randn(m,n); c = randn(n,m); d = randn(n);
%!		x = [a b;c d]^-1;
%!		y = [(a-b/d*c)^-1 a\b/(c/a*b-d);d\c/(b/d*c-a) (d-c/a*b)^-1];
%!		z = [(a-b/d*c)^-1 (b/d*c-a)\b/d;(c/a*b-d)\c/a (d-c/a*b)^-1];
%!		assert(x, y, eps^.5);
%!		assert(x, z, eps^.5);
%!	end end
