%% str = cstrftime(seconds, fmt)

function str = cstrftime(seconds, fmt)
	str = char(arrayfun(@(s){strftime(fmt,localtime(s))}, seconds));
end
