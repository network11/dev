%% gmean(x, p)
%%	Compute the generalized mean.

function y = gmean(x, p)
	assert(x, abs(x));
	switch (p)
	case inf
		y = max(x);
	case -inf
		y = min(x);
	case 0
		y = exp(mean(log(x)));
	otherwise
		y = mean(x .^ p) .^ (1/p);
	end
end

%!test
%!	for n = 1:9
%!		x = rande(1,n);
%!		assert(gmean(x,+inf), max(x));
%!		assert(gmean(x,-inf), min(x));
%!		assert(gmean(x,+1), mean(x));
%!		assert(gmean(x,+0), prod(x).^(1/n), eps^.5);
%!		assert(gmean(x,-1), 1./mean(1./x), eps^.5);
%!	p = rand; for p = [p 1./p]
%!		assert(gmean(x,p), vecnorm(x,p)/n^(1/p), eps^.5);
%!	end
%!	p = rand; for p = [-inf -1./p -p 0 p 1./p inf]
%!	a = rand; for a = [a 1./a]
%!		assert(gmean(a*x,p), a*gmean(x,p), eps^.5);
%!	end
%!	end
%!	end
%!test
%!	p = [rand(1,5) inf]; p = unique([-1./p -p p 1./p]);
%!	for n = 2:9
%!		x = rande(1,n);
%!		y = arrayfun(@(p)gmean(x,p), p);
%!		assert(diff(y) > 0);
%!	end

%!demo
%!	p = -3:.1:3;
%!	for x = 10.^(0:.1:2)
%!		x = [1;x];
%!		y = arrayfun(@(p)gmean(x,p), p);
%!		semilogy(p,[y;x+0*p]); ylim([1e-1 1e3]); grid on;
%!		drawnow;
%!	end
%!demo
%!	p = -3:.1:3;
%!	for x = 10.^(0:.1:2)
%!		x = [1;x;1e2];
%!		y = arrayfun(@(p)gmean(x,p), p);
%!		semilogy(p,[y;x+0*p]); ylim([1e-1 1e3]); grid on;
%!		drawnow;
%!	end
