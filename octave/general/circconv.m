%% circconv(x)
%% circconv(x, y)
%%	Compute circular convolution.

function z = circconv(x, y=x)
	assert(numel(x), numel(y));
	z = conv(x([2:end 1:end]), y, 'valid');
end

%!test
%!	for n = 1:9
%!		x = randn(1,n) + 1j*randn(1,n);
%!		y = randn(1,n) + 1j*randn(1,n);
%!		assert(circconv(x,y), ifft(fft(x).*fft(y)), eps^.5);
%!	end
%!test
%!	for m = 1:9 for n = 1:9
%!		x = randn(1,m) + 1j*randn(1,m); xx = resize(x, 1,m+n-1);
%!		y = randn(1,n) + 1j*randn(1,n); yy = resize(y, 1,m+n-1);
%!		assert(conv(x,y), circconv(xx,yy), eps^.5);
%!	end end
