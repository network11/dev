%% fmean(x, f)
%%	Compute the generalized f-mean.

function y = fmean(x, f)
	assert(x, abs(x));
	y = mean(f(x));
	if (isempty(x)) return end
	y = fzero(@(x)f(x)-y, [min(x) max(x)]);
end

%!test
%!	for n = 0:9
%!		x = rande(1,n);
%!		assert(fmean(x,@log), gmean(x,0), 1e-5);
%!	p = rand; for p = [-1./p -p p 1./p]
%!		assert(fmean(x,@(x)x.^p), gmean(x,p), 1e-5);
%!	end
%!	end
