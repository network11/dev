%% gvander(p)
%% gvander(p,e)
%%	Return the generalized Vandermonde matrix.

function v = gvander(p, e=ones(size(p)))
	n = numel(p):-1:1; p = p(:); e = e(:);
	v = bsxfun(@bincoeff, n-1, e-1) .* p .^ max(0, n-e);
end

%!test
%!	for n = 0:9
%!		p = randi([-4 4],n,1);
%!		assert(gvander(p), vander(p));
%!		[e,i] = mpoles(p); p = p(i); v = gvander(p,e);
%!		assert(det(fliplr(v)), prod(nonzeros(tril(p-p.'))), -eps^.5);
%!		d = diag(p) + diag(e(2:end)~=1,1);
%!		assert(v.'*d/v.', compan(poly(p)), -eps^.5);
%!	end
