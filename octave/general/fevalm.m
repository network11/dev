%% X = fevalm(fdots, A)
%%	Evaluate a function in the matrix sense.
%%	X == f(A) where fdots(x,d) == f^(d)(x)

function x = fevalm(f, a)
	p = eig(a);
	[e,i] = mpoles(p); p = p(i); v = gvander(p,e);
	p = v \ (factorial(e-1) .\ f(p,e-1));
	x = polyvalm(p,a);
end

%!function id(f,fm,x)
%!	for n = 1:numel(x)
%!		v = randn(n);
%!		l = x(randi(end,1,n));
%!		a = diag(l(randperm(end))); a = v*a/v;
%!		assert(fevalm(f,a), fm(a), eps^.5);
%!	end
%!endfunction
%!function jd(f,fm,x)
%!	for n = 1:numel(x)
%!		v = randn(n);
%!		l = x(randi(end,1,n));
%!		l = sort(l); a = diag(l)+diag(~diff(l)(:),1); a = v*a/v;
%!		assert(fevalm(f,a), fm(a), 1e-3);
%!	end
%!endfunction
%!shared u
%!	u = rand(1,5);
%!xtest id(@(x,d)exp(x), @expm, u*2-1);
%!xtest jd(@(x,d)exp(x), @expm, u*2-1);
%!xtest id(@(x,d)bincoeff(.5,d).*gamma(d+1).*x.^(.5-d), @sqrtm, u*2);
%!xtest jd(@(x,d)bincoeff(.5,d).*gamma(d+1).*x.^(.5-d), @sqrtm, u*2);
%!xtest id(@(x,d)ifelse(d,-gamma(d).*(-x).^(-d),log(x)), @logm, u+.5);
%!xtest jd(@(x,d)ifelse(d,-gamma(d).*(-x).^(-d),log(x)), @logm, u+.5);
%!xtest a = 1.5; id(@(x,d)log(a).^d.*a.^x, @(x)a^x, u*2-1);
%!xtest a = 0.5; id(@(x,d)log(a).^d.*a.^x, @(x)a^x, u*2-1);
%!xtest a = 1.5; id(@(x,d)bincoeff(a,d).*gamma(d+1).*x.^(a-d), @(x)x^a, u*2);
%!xtest a = 0.5; id(@(x,d)bincoeff(a,d).*gamma(d+1).*x.^(a-d), @(x)x^a, u*2);
%!xtest a =-0.5; id(@(x,d)bincoeff(a,d).*gamma(d+1).*x.^(a-d), @(x)x^a, u+.5);
%!xtest a =-1.5; id(@(x,d)bincoeff(a,d).*gamma(d+1).*x.^(a-d), @(x)x^a, u+.5);
