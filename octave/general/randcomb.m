%% c = randcomb(n, k)
%%	Return a row vector containing a random combination of 1:n taken k
%%	using reservoir sampling.

function c = randcomb(n, k)
	c = 1:k;
	for i = k+1:n
		p = randi(i);
		if (p <= k) c(p) = i; end
	end
	c = sort(c);
end

%!test
%!	for n = 1:9 for k = 1:n
%!		c = randcomb(n,k);
%!		assert(numel(c), k);
%!		assert(unique(c), c);
%!		assert(ismember(c, 1:n));
%!	end end
%!test
%!	for n = 1:9 for k = 1:n
%!		p = randperm(n,k);
%!		assert(numel(p), k);
%!		assert(unique(p), sort(p));
%!		assert(ismember(p, 1:n));
%!	end end

%!demo
%!	n = 8; m = 100; for k = 1:n
%!		c = []; for i = 1:n*m c = [c;randcomb(n,k)]; end
%!		plot(hist(c(:),1:n), sprintf(';%d;',k)); hold on;
%!	end
%!	hold off; grid on; xticks(1:n); yticks((1:n)*m);
%!demo
%!	n = 8; m = 100; for k = 1:n
%!		p = []; for i = 1:n*m p = [p;randperm(n,k)]; end
%!		plot(hist(p(:),1:n), sprintf(';%d;',k)); hold on;
%!	end
%!	hold off; grid on; xticks(1:n); yticks((1:n)*m);
