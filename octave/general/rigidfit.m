%% [w,b] = rigidfit(x,y)
%%	rigid fit
%%	size(x) == [m,n]
%%	size(y) == [m,n]
%%	size(w) == [n,n]
%%	size(b) == [1,n]
%%
%%	Solve the optimization problem defined by
%%		min{w,b} norm(x*w+b-y,'fro') s.t. w'*w==eye(n)

function [w,b] = rigidfit(x,y)
	xc = mean(x,1);
	yc = mean(y,1);
	w = qs((x-xc)'*(y-yc));
	b = yc-xc*w;
end

%!shared tol
%!	tol = eps^.5;
%!function [w,b] = fit2(x,y)
%!	xc = mean(x);
%!	yc = mean(y);
%!	w = sign(dot(x-xc,y-yc));
%!	b = yc-xc*w;
%!endfunction
%!xtest
%!	for m = 3:9
%!		n = randn(m,1)+1j*randn(m,1);
%!		x = randn(m,1)+1j*randn(m,1);
%!		w = sign(randn+1j*randn);
%!		b = randn+1j*randn;
%!		y = x*w+b + n*.1;
%!		[w,b] = fit2(x,y);
%!		[wn,bn] = rigidfit([real(x) imag(x)],[real(y) imag(y)]);
%!		assert(wn, [real(w) imag(w);-imag(w) real(w)], tol);
%!		assert(bn, [real(b) imag(b)], tol);
%!	end

%!test
%!	for n = 2:5 for m = n+1:9
%!		x = randn(m,n);
%!		w = orth(randn(n));
%!		b = randn(1,n);
%!		[wn,bn] = rigidfit(x,x*w+b);
%!		assert({wn,bn}, {w,b}, tol);
%!	end end
%!test
%!	d = 1e-4;
%!	q = @(x) expm((x-x')/2);
%!	for n = 2:5 for m = n+1:9
%!		x = randn(m,n);
%!		y = randn(m,n);
%!		[w,b] = rigidfit(x,y);
%!		for i = 1:30
%!			we = q(randn(n)*d);
%!			be = randn(1,n)*d;
%!			assert(norm(x*w+b-y,'fro') < norm(x*w*we+b-y,'fro'));
%!			assert(norm(x*w+b-y,'fro') < norm(x*w+b+be-y,'fro'));
%!		end
%!	end end

%!demo
%!	n = 2; m = n+2;
%!	q = @(x) expm((x-x')/2);
%!	x = randn(m,n);
%!	y = randn(m,n)*.1+x*q(randn(n))+randn(1,n);
%!	[w,b] = rigidfit(x,y);
%!	z = x*w+b;
%!	c = @(x) num2cell(x,1);
%!	plot(c(x){:},'o-',c(z){:},'s-',c(y){:},'x--');
%!	grid on; axis equal;
%!demo
%!	n = 3; m = n+2;
%!	q = @(x) expm((x-x')/2);
%!	x = randn(m,n);
%!	y = randn(m,n)*.1+x*q(randn(n))+randn(1,n);
%!	[w,b] = rigidfit(x,y);
%!	z = x*w+b;
%!	c = @(x) num2cell(x,1);
%!	plot3(c(x){:},'o-',c(z){:},'s-',c(y){:},'x--');
%!	grid on; axis equal;
