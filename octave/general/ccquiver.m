%% ccquiver(xy, uv)

function varargout = ccquiver(xy, uv, varargin)
	x = real(xy); y = imag(xy); u = real(uv); v = imag(uv);
	[varargout{1:nargout}] = quiver(x,y, u,v, varargin{:});
	axis equal; grid on; xlabel Re; ylabel Im;
end

%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,z.^2);
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,z.^1.4);
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,z);
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,z.^.7);
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,z.^.5);

%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,exp(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,cosh(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,sinh(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,cos(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,sin(z));

%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,log(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,acosh(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,asinh(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,acos(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi,21)); ccquiver(z,asin(z));
