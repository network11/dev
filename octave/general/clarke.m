%% C+ = clarke(N, OPT)
%% C = clarke(-N, OPT)
%%	Compute the inverse Clarke transform matrix.
%%	The optional argument OPT selects the type:
%%		'a': amplitude invariant version. [default]
%%		'p': power invariant version.

function c = clarke(n, opt='a')
	opt += sign(n); n = abs(n);
	a = fft([zeros(n-1,1);1]); a = [real(a) imag(a)];
	k = 2/n;
	switch (opt)
	case 'a'+1	c = a;
	case 'a'-1	c = a' * k;
	case 'p'+1	c = a  * sqrt(k);
	case 'p'-1	c = a' * sqrt(k);
	end
end

%!test
%!	tol = eps^.5;
%!	a = randn(1,9)+randn(1,9)*1j;
%!	a2 = [real(a);imag(a)];
%!	p2 = a2 / sqrt(2);
%!	for n = 3:10
%!		an = real(a.*exp(2j*pi*(0:n-1)/n)');
%!		pn = an / sqrt(n);
%!		a2n = clarke(-n,'a');
%!		p2n = clarke(-n,'p');
%!		an2 = clarke(n,'a');
%!		pn2 = clarke(n,'p');
%!		assert(a2n*an, a2, tol);
%!		assert(p2n*pn, p2, tol);
%!		assert(an2*a2, an, tol);
%!		assert(pn2*p2, pn, tol);
%!		assert(a2n*an2, eye(2), tol);
%!		assert(p2n*pn2, eye(2), tol);
%!		assert(an2*a2n, circul(2/n*cos(2*pi*(0:n-1)/n)), tol);
%!		assert(pn2*p2n, circul(2/n*cos(2*pi*(0:n-1)/n)), tol);
%!		assert(a2n, pinv(an2), tol);
%!		assert(p2n, pinv(pn2), tol);
%!		assert(a2n, an2\eye(n), tol);
%!		assert(p2n, pn2\eye(n), tol);
%!		assert(an2, pinv(a2n), tol);
%!		assert(pn2, pinv(p2n), tol);
%!		assert(an2, a2n\eye(2), tol);
%!		assert(pn2, p2n\eye(2), tol);
%!		assert(pn2, p2n', tol);
%!	end

%!demo
%!	langevin = @(x)coth(x)-1./x;
%!	fplot(@(x)[tanh(x) langevin(x*3)]); grid on; legend location southeast;

%!demo
%!	% magnetic coenergy
%!	a = 3; l = [-a a -a a]; t = -a:a; i = -a:1e-3:a;
%!	p = tanh(i); w = log(cosh(i)); u = ((1+p).*log(1+p)+(1-p).*log(1-p))/2;
%!	plot(i,p,i,w,p,i,p,u); grid on; axis(l,'equal'); xticks(t); yticks(t);
%!	legend '\Psi(I)' 'W(I)' 'I(\Psi)' 'U(\Psi)' location southeast;
%!	assert(p.*i, u+w, eps^.5);
%!	assert(p(2:end-1), gradient(w,i)(2:end-1), 1e-6);
%!	assert(i(2:end-1), gradient(u,p)(2:end-1), 1e-6);

%!demo
%!	% SPWM spectrum
%!	m = 20; n = m^2; t = 0:n-1; l = [0 m*6 -1 1];
%!	c = 90\asind(sinewave(n,m,m/4));
%!	x = sinewave(n,n,n/4);
%!	y = sign(x-c);
%!	cc = fft(c)*(2/n); assert(imag(cc),0*cc,eps^.5);
%!	xx = fft(x)*(2/n); assert(imag(xx),0*xx,eps^.5);
%!	yy = fft(y)*(2/n); assert(imag(yy),0*yy,eps^.5);
%!	subplot(321); plot(t,c); subplot(322); plot(t,cc); grid on; axis(l);
%!	subplot(323); plot(t,x); subplot(324); plot(t,xx); grid on; axis(l);
%!	subplot(325); plot(t,y); subplot(326); plot(t,yy); grid on; axis(l);
%!demo
%!	% SPWM envelope
%!	n = 3;
%!	x = -360:360; t = -360:360/n:360; l = [-360 360];
%!	y1 = cosd(x-360/n*(0:n-1)');
%!	y2 = y1 - min(y1);
%!	y3 = y1 - (max(y1)+min(y1))/2;
%!	y4 = y3 ./ max(y3);
%!	z3 = y3 - circshift(y3,-1);
%!	z4 = y4 - circshift(y4,-1);
%!	subplot(321); plot(x,y1); grid on; xticks(t); axis([l -2 2]);
%!	subplot(322); plot(x,y2); grid on; xticks(t); axis([l -1 3]);
%!	subplot(323); plot(x,y3); grid on; xticks(t); axis([l -2 2]);
%!	subplot(324); plot(x,y4); grid on; xticks(t); axis([l -2 2]);
%!	subplot(325); plot(x,z3); grid on; xticks(t); axis([l -2 2]);
%!	subplot(326); plot(x,z4); grid on; xticks(t); axis([l -2 2]);
