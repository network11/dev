%% [q, r] = divmod(x, y, op)

function [q, r] = divmod(x, y, op='floor')
%	q = idivide(x, y, op);
	q = feval(op, x ./ y);
	r = x - y .* q;
end

%!shared x,y
%!	x = randn(3,4);
%!	y = randn(3,4);
%!test
%!	[q, r] = divmod(x, y, 'floor');
%!	assert(q, floor(x./y));
%!	assert(r, mod(x, y));
%!test
%!	[q, r] = divmod(x, y, 'fix');
%!	assert(q, fix(x./y));
%!	assert(r, rem(x, y));
