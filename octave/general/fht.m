%% fht(x)
%% fht(x, n)
%% fht(x, n, dim)
%%	Compute the discrete Hartley transform

function y = fht(x, varargin)
	y = rfht(real(x), varargin{:}) + 1j * rfht(imag(x), varargin{:});
end

function y = rfht(x, varargin)
	y = real((1+1j) * fft(x, varargin{:}));
end

%!test
%!	for n = 1:9
%!		a = fht(eye(n)); b = ifht(eye(n));
%!		f = fft(eye(n)); g = ifft(eye(n));
%!		assert(a, real((1+1j)*f));
%!		assert(b, real((1-1j)*g));
%!		assert(a, cas(2*pi/n*(0:n-1)'*(0:n-1))', eps^.5);
%!		assert(f, cis(2*pi/n*(0:n-1)'*(0:n-1))', eps^.5);
%!		assert(b, a'/n, eps^.5);
%!		assert(g, f'/n, eps^.5);
%!		assert(a*b, eye(n), eps^.5);
%!		assert(f*g, eye(n), eps^.5);
%!	end
