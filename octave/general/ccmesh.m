%% ccmesh(uv)

function varargout = ccmesh(uv, varargin)
	uv(end+1,:) = nan; uv(:,end+1) = nan; u = real(uv); v = imag(uv);
	ux = u'(:); vx = v'(:); uy = u(:); vy = v(:);
	[varargout{1:nargout}] = plot(ux,vx,varargin{:}, uy,vy,varargin{:});
	axis equal; grid on; xlabel Re; ylabel Im;
end

%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(z.^2);
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(z.^1.4);
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(z);
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(z.^.7);
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(z.^.5);

%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(exp(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(cosh(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(sinh(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(cos(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(sin(z));

%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(log(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(acosh(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(asinh(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(acos(z));
%!demo z = ccmeshgrid(linspace(-2*pi,2*pi)); ccmesh(asin(z));
