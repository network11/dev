%% p = besselpoly(n)
%%	Compute the coefficients of the reverse Bessel polynomial.

function p = besselpoly(n)
	k = 0:n;
	p = factorial(n+k)./factorial(n-k)./factorial(k)./2.^k;
end

%!test
%!	for n = 0:7
%!		p = besselpoly(n);
%!		assert(p(1), 1);
%!		assert(p(end), p(max(1,end-1)));
%!		assert(p, abs(round(p)));
%!	end
%!test
%!	for n = 1:6
%!		p = besselpoly(n) .* 1j.^(n:-1:0); a = real(p); b = imag(p);
%!		den = conv(a,a) + conv(b,b);
%!		num = conv(polyder(b),a) - conv(polyder(a),b);
%!		assert(den, [1 num], eps^.5);
%!	end

%!demo
%!	for n = 1:6 subplot(2,3,n);
%!		p = besselpoly(n); r = roots(p);
%!		polar(arg(r),abs(r),'x'); rticks(0:2:6);
%!		title(n); disp(polyout(p));
%!	end

%!demo
%!	w = logspace(-2,2); s = 1j*w;
%!	for n = 1:4 p = besselpoly(n); h(n,:) = p(end)./polyval(p,s); end
%!	p = unwrap(arg(h),[],2);
%!	subplot(221); semilogx(w,abs(h)); grid on;
%!	subplot(222); semilogx(w,rad2deg(p)); grid on; yticks(-360:45:0);
%!	subplot(223); semilogx(w,p./w); grid on;
%!	subplot(224); semilogx(w,gradient(p,w)); grid on;
