%% phi = totient(n)
%%	Compute the Euler's totient function.

function phi = totient(n)
	p = factor(n);
	p = setdiff(p, 0:1);
	phi = n * prod(1 - 1 ./ p);
	phi = round(phi);
end

%!shared x,y,tol
%!	x = 1:100;
%!	y = arrayfun(@totient, x);
%!	tol = eps^.5;
%!assert(y, [
%!	 1  1  2  2  4  2  6  4  6  4 ...
%!	10  4 12  6  8  8 16  6 18  8 ...
%!	12 10 22  8 20 12 18 12 28  8 ...
%!	30 16 20 16 24 12 36 18 24 16 ...
%!	40 12 42 20 24 22 46 16 42 20 ...
%!	32 24 52 18 40 24 36 28 58 16 ...
%!	60 30 36 32 48 20 66 32 44 24 ...
%!	70 24 72 36 40 36 60 24 78 32 ...
%!	54 40 82 24 64 42 56 40 88 24 ...
%!	72 44 60 46 72 32 96 42 60 40 ...
%!	]);
%!assert(y, arrayfun(@(n)nnz(gcd(1:n,n)==1), x));
%!assert(y, arrayfun(@(n)sum(gcd(1:n,n).*cos(2*pi/n*(1:n))), x), tol);
%!test
%!	[m,n] = ndgrid(1:13);
%!	assert(mod(m.^y(n)-1, n)(gcd(m,n)==1) == 0);
%!test
%!	[m,n] = ndgrid(1:10);
%!	d = gcd(m,n);
%!	assert(y(m.*n), y(m).*y(n).*d./y(d));
