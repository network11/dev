%% cas(x)
%%	Compute the cosine-and-sine function.
%%	Return cos(x) + sin(x).

function y = cas(x)
	y = cos(x) + sin(x);
end

%!test
%!	x = randn(1,5) + 1j*randn(1,5);
%!	assert(cas(x), sqrt(2)*cos(x-pi/4), eps^.5);
