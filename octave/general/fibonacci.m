%% F = fibonacci(N)
%%	Return the first N Fibonacci numbers.

function f = fibonacci(n)
	f = filter(1,[1 -1 -1], resize(1,1,n));
end

%!test
%!	z = roots([1 -1 -1]);
%!	f = @(n) (z(2).^n - z(1).^n) ./ (z(2) - z(1));
%!	for n = 0:9
%!		assert(fibonacci(n), f(1:n), -eps^.5);
%!	end
