%% view3()
%% view3(hax)
%% h = view3(...)
%%	Create three-view drawing for the specified or current axes.
%%	Return the axis handles for the four subplots.

function h = view3(hax=gca)
	s = hdl2struct(hax);
	h = arrayfun(@(i)[subplot(2,2,i)], 1:4);
%	n = '__subplotposition__'; d = arrayfun(@(h){getappdata(h,n)}, h);
	p = {'position' 'units'}; v = get(h, p);
	h = clf;
	h = arrayfun(@(i)struct2hdl(s,h), 1:4);
%	cellfun(@(h,d)setappdata(h,n,d), num2cell(h), d);
	set(h, p, v);
	set(h, {'view'}, {[0,0];[-90,0];[0 90];[-37.5,30]});
end

%!demo
%!	a = [cellfun(@(x){x(:)},nthargout(1:3,@ndgrid,-10:10)){:}]';
%!	a += (rand(size(a))-.5)*.2;
%!	b = a - circshift(a,-1);
%!	figure(1); plot3(num2cell(a,2){:},'.');
%!	grid on; xlabel x; ylabel y; zlabel z; view3;
%!	figure(2); plot3(num2cell(b,2){:},'.');
%!	grid on; xlabel x; ylabel y; zlabel z; view3;
