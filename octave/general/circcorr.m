%% circcorr(x)
%% circcorr(x, y)
%%	Compute circular correlation.

function z = circcorr(x, y=x)
	assert(numel(x), numel(y));
	z = conv(x([1:end 1:end-1]), conj(y(end:-1:1)), 'valid');
end

%!test
%!	for n = 1:9
%!		x = randn(1,n) + 1j*randn(1,n);
%!		y = randn(1,n) + 1j*randn(1,n);
%!		assert(circcorr(x,y), ifft(fft(x).*conj(fft(y))), eps^.5);
%!	end
