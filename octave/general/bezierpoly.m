%% p = bezierpoly(n)
%%	Compute the coefficients of the Bernstein polynomial.

function p = bezierpoly(n)
	k = 0:n;
	p = bincoeff(n,k').*bsxfun(@bincoeff,n-k',k).*(-1).^(n-k-k');
end

%!test
%!	for n = 0:9
%!		p = bezierpoly(n);
%!		assert(istril(flip(p)));
%!		assert(issymmetric(p));
%!		assert(p, round(p));
%!	end
%!test
%!	for m = 0:5 for n = 0:5
%!		assert(bezierpoly(m+n), conv2(bezierpoly(m),bezierpoly(n)));
%!	end end
%!test
%!	for n = 1:9
%!		p = bezierpoly(n);
%!		assert(polyreduce(sum(p,1)), 1);
%!		assert(polyreduce((0:n)*p), [n 0]);
%!	end

%!demo
%!	f = gcf; h = plot(nan,'+',nan,'-');
%!	axis([-2 2 -2 2],'equal'); grid on;
%!	p = []; t = linspace(0,1);
%!	while (isfigure(f))
%!		c = polyval(p*bezierpoly(numel(p)-1),t);
%!		set(h,{'xdata' 'ydata'},{real(p) imag(p);real(c) imag(c)});
%!		[x,y,b] = ginput(1);
%!		if (b == 1) p = [p complex(x,y)]; else p = []; end
%!	end

%!demo
%!	f = @(x)sinc(x*2); x = linspace(0,1)'; n = 2.^(0:5);
%!	y = [arrayfun(@(n){polyval(f((0:n)/n)*bezierpoly(n),x)},n){:}];
%!	plot(x,y,x,f(x),'k-.'); grid on; legend(num2str([n inf]'));
