%% ifht(x)
%% ifht(x, n)
%% ifht(x, n, dim)
%%	Compute the inverse discrete Hartley transform

function y = ifht(x, varargin)
	y = irfht(real(x), varargin{:}) + 1j * irfht(imag(x), varargin{:});
end

function y = irfht(x, varargin)
	y = real((1-1j) * ifft(x, varargin{:}));
end
