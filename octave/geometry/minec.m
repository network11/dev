%% minec(m, n, d)

function minec(m, n, d, dbg=0)
	[x,y] = meshgrid(1:n,1:m); x = x(:); y = y(:);
	xy = [x y]; a = max(abs(x-x'),abs(y-y')) == 1;
	axis(.5+[0 n 0 m],'equal','nolabel'); grid on;
	xticks(.5+(0:n)); yticks(.5+(0:m)); mineg(a,xy,d,dbg);
end

%!demo minec( 9, 9,10);
%!demo minec(16,16,40);
%!demo minec(16,30,99);
