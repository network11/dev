%% a = tri2g(tri)
%%	convert from polygon mesh to adjacency matrix
%%	see also: triplot, gplot

function a = tri2g(tri)
	e = cat(3, tri, circshift(tri,-1,2));
	e = reshape(e, [],2);
	a = accumarray(e, 1);
end

%!assert(tri2g(p=randi(5,7,3)), tri2g(fliplr(p))');
%!assert(diag(tri2g(delaunay(rand(9,2)))) == 0);
%!demo
%!	x = rand(9,1); y = rand(9,1); t = delaunay(x,y); a = tri2g(t);
%!	subplot(121); triplot(t,x,y); subplot(122); gplot(a,[x,y]);
