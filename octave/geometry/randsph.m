%% xyz = randsph(n)

function r = randsph(n)
	d = 4/sqrt(n);
	r = randn(n,3);
	r ./= sqrt(sumsq(r,2));
	for i = 1:100
		v = r - permute(r,3:-1:1);
		v = sum(v .* max(0, 1./max(eps,sqrt(sumsq(v,2))) - 1/d), 3);
		r += .1 * v;
		r ./= sqrt(sumsq(r,2));
	end
end

%!demo plot3(num2cell(randsph(200),1){:},'o'); grid on; axis square;
%!demo plot3(num2cell(randsph(400),1){:},'o'); grid on; axis square;
