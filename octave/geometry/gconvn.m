%% c = gconvn(a, b)
%%	convolve two set of points
%%	see also: convhulln

function c = gconvn(a, b)
	assert(ismatrix(a));
	assert(ismatrix(b));
	dim = columns(a);
	assert(dim, columns(b));
	a = reshape(a, [],1,dim);
	b = reshape(b, 1,[],dim);
	c = reshape(a+b, [],dim);
end

%!test
%!	for dim = 1:3 for na = 1:3 for nb = 1:3
%!		a = randn(na,dim);
%!		b = randn(nb,dim);
%!		c = gconvn(a,b);
%!		assert(size(c), [na*nb dim]);
%!	end end end
%!test
%!	ac = @(p) accumarray(p+1,1);
%!	for dim = 1:4
%!		a = randi([0 9],randi(9),dim);
%!		b = randi([0 9],randi(9),dim);
%!		c = gconvn(a,b);
%!		assert(logical(convn(ac(a),ac(b))), logical(ac(c)));
%!	end
%!test
%!	ch = @(p) p(unique(convhulln(p)),:);
%!	for dim = 2:4
%!		a = randn(randi([dim+1 9]),dim);
%!		b = randn(randi([dim+1 9]),dim);
%!		c = sortrows(ch(gconvn(a,b)));
%!		d = sortrows(ch(gconvn(ch(a),ch(b))));
%!		assert(c, d, eps^.5);
%!	end

%!demo
%!	[x,y] = ndgrid(20:100,-20:20); r = [x(:) y(:)];
%!	ip = @(p) r(inpolygon(x,y,num2cell(p,1){:}), :);
%!	ch = @(p) p(convhull(num2cell(p,1){:}), :);
%!	a1 = ch(randi([0 20],8,2)([1:end 1],:)+[20 -10]);
%!	a2 = ch(randi([0 20],6,2)([1:end 1],:)+[40 -10]);
%!	a3 = ch(gconvn(a1,a2));
%!	b1 = ip(a1);
%!	b2 = ip(a2);
%!	b3 = gconvn(b1,b2);
%!	c = @(p) num2cell(p,1);
%!	plot(c(a1){:},c(a2){:},c(a3){:},c(b1){:},'.',c(b2){:},'.',c(b3){:},'.');
%!	grid on;
%!	axis([20 100 -20 20],'equal'); yticks(-20:10:20); xticks(20:10:100);

%!demo
%!	n = 20; [x,y] = ndgrid(-n:n); r = [x(:) y(:)];
%!	a = randi([-n n],8,2)([1:end 1],:);
%!	a = complex(x,y)(inpolygon(x,y,num2cell(a,1){:}));
%!	h = plot(a,'+'); grid on; axis([-n n -n n]);
%!	t = title(1); drawnow; pause(.5);
%!	for i = 2.^(1:16)
%!		a = unique(roundb(gconvn(a,a)/2));
%!		set(h,{'xdata' 'ydata'},{real(a) imag(a)});
%!		set(t,'string',i); drawnow;
%!	end
