%% a = urquhart(xy)
%%	compute the Urquhart graph for a 2-D set of points
%%	see also: delaunay, gplot

function a = urquhart(xy, tol=eps^.5)
	[x,y] = deal(num2cell(xy,1){:});
	tri = delaunay(x,y);
	a = tri2g(tri);

	e = cat(3, tri, circshift(tri,-1,2));
	d = hypot(x-x', y-y');
	d = d(sub2ind(size(d), e(:,:,1), e(:,:,2)));
	d = d > median(d,2) + tol;
	e = reshape(e, [],2)(d,:);
	b = accumarray(e, 1, size(a));

	a = (a+a') &~ (b+b');
end

%!test
%!	xy = randn(9,2); a = tri2g(delaunay(xy)); u = urquhart(xy);
%!	assert(issymmetric(u)); a = a+a'~=0; assert(u <= a);

%!demo
%!	xy = rand(90,2); a = tri2g(delaunay(xy)); u = urquhart(xy);
%!	gplot(a,xy); hold on; gplot(u,xy,'-or'); hold off; axis equal;
%!demo
%!	for i = 1:3 for j = 1:4 subplot(3,4,(i-1)*4+j);
%!		n = [4 6 9](i); p = {[] .1j .1 0}{j};
%!		xy = [exp(2j*pi/n*(1:n)) p].'; xy = [real(xy) imag(xy)];
%!		a = tri2g(delaunay(xy)); u = urquhart(xy);
%!		gplot(a,xy); hold on; gplot(u,xy,'-or'); hold off; axis equal;
%!	end end
%!demo
%!	for i = 1:3 for j = 1:4 subplot(3,4,(i-1)*4+j);
%!		[x,y] = ndgrid(1:3,1:4); xy = [x(:) y(:)]*[i/2 j/4;0 1];
%!		a = tri2g(delaunay(xy)); u = urquhart(xy);
%!		gplot(a,xy); hold on; gplot(u,xy,'-or'); hold off; axis equal;
%!	end end
