%% [c,f,i,st] = mine(c,f,b,a,p,ev)
%%	size(c) == [n,1]
%%	size(f) == [n,1]
%%	size(b) == [n,1]
%%	size(a) == [n,n]
%%	size(p) == [n,1]
%%	ismember(ev, 1:3)
%%	islogical([c,f,b,a,p])

function [c,f,i,st] = mine(c,f,b,a,p,ev)
	assert(cellfun(@iscolumn,{c,f,b,p}));
	assert(islogical([c,f,b,a,p]));
	assert(issymmetric(a));
	assert(~(c & f));
	[i,st] = stat(c,f,b,a);
	if (ischar(st)) return end
	[c,f] = {@fcn1 @fcn2 @fcn3}{ev}(c,f,b,a,p);
	assert(~(c & f));
	[i,st] = stat(c,f,b,a);
end

function [c,f] = fcn1(c,f,b,a,p)
	p = p & ~c & ~f;
	c(p) = true;
	p = p & ~b & a*b==0;
	n = a*p>0 & ~p;
	if (any(n)) [c,f] = fcn1(c,f,b,a,n); end
end

function [c,f] = fcn2(c,f,b,a,p)
	p = p & c & a*b==a*f;
	n = a*p>0 & ~p;
	[c,f] = fcn1(c,f,b,a,n);
end

function [c,f] = fcn3(c,f,b,a,p)
	p = p & ~c;
	f(p) = ~f(p);
end

function [i,st] = stat(c,f,b,a)
	i = a*b;
	i(~c) = -1;
	i( f) = -2;
	if (any(b & c))
		i(b & ~f) = -3;
		i(f & ~b) = -4;
		st = 'FAIL';
	elseif (b ~= c)
		st = 'PASS';
	else
		st = nnz(b) - nnz(f);
	end
end
