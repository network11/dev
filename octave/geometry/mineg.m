%% mineg(a, xy, d)

function mineg(a, xy, d, dbg=0)
	a = a+a'~=0; n = length(a); [x,y] = deal(num2cell(xy,1){:});
	c = f = false(n,1);
	b = full(logical(sprand(n,1,d/n)));
	hi = text(x,y,' ', 'horizontalalignment','center','fontsize',20);
	ht = title(d, 'userdata',{c,f,b,a,x,y});
	if (dbg); hold on; plot(x(b),y(b),'*k'); hold off; end
	refresh;
	set(gcf,'windowbuttondownfcn',{@fcn,hi,ht});
	waitfor(gcf);
end

function fcn(h,ev, hi,ht)
	[c,f,b,a,x,y] = get(ht,'userdata'){:};
	xy = get(gca,'currentpoint')(1,1:2);
	d = hypot(xy(1)-x, xy(2)-y); p = d == min(d);
	[c,f,i,st] = mine(c,f,b,a,p,ev);
	im(:,1) = cellstr(['EBF '.';num2str((0:max(i))')]);
	im(:,2) = cellstr(['rkgk' 'b'(ones(1,1+max(i)))]');
	set(hi,{'string','color'},im(i+5,:));
	set(ht,'string',st, 'userdata',{c,f,b,a,x,y});
	refresh;
end

%!demo
%!	m = 9; n = 16; d = 20;
%!	[x,y] = meshgrid(1:n,1:m); x = x(:); y = y(:);
%!	xy = [x y];
%!	a = urquhart(xy);
%!	gplot(a,xy); axis equal off; mineg(a,xy,d);
%!demo
%!	n = 9; d = 30;
%!	[x,y] = meshgrid(1-n:n-1); x = x(:); y = y(:);
%!	xy = [x y](abs(x-y)<n,:) * [cosd(30) .5;0 -1];
%!	a = tri2g(delaunay(xy));
%!	gplot(a,xy); axis equal off; mineg(a,xy,d);

%!demo
%!	m = 9; n = 16; d = 20;
%!	xy = randrect(m,n);
%!	a = tri2g(delaunay(xy));
%!	gplot(a,xy,'o-'); axis equal off; mineg(a,xy,d);
%!demo
%!	n = 9; d = 10;
%!	xy = randcirc(n^2);
%!	a = urquhart(xy);
%!	gplot(a,xy,'o-'); axis equal off; mineg(a,xy,d);
