%% xy = randcirc(n)

function xy = randcirc(n)
	z = [1 1j]/2*randn(2,n); d = 2/sqrt(n);
	for i = 1:100
		v = z-z.'; v = sum(sign(v).*max(0,1-abs(v)/d));
		z += .1 * v;
		z = ifelse(abs(z)>1, sign(z), z);
	end
	xy = [real(z);imag(z)]';
end

%!demo plot(num2cell(randcirc(100),1){:},'o'); axis square;
%!demo plot(num2cell(randcirc(200),1){:},'o'); axis square;
