%% xy = randrect(n)
%% xy = randrect(m, n)

function xy = randrect(m, n=m)
	z = [n m*1j]*rand(2,n*m); d = 1.5;
	for i = 1:100
		v = z-z.'; v = sum(sign(v).*max(0,1-abs(v)/d));
		z += .1 * v;
		z = max(0,min(n,real(z))) + max(0,min(m,imag(z)))*1j;
	end
	xy = [real(z);imag(z)]';
end

%!demo plot(num2cell(randrect(10,10),1){:},'o'); axis equal;
%!demo plot(num2cell(randrect(10,20),1){:},'o'); axis equal;
