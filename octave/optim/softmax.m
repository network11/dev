%% y = softmax(x)
%%	softmax function for multinomial over k possible outcomes
%%	size(x) == [m,k]
%%	size(y) == [m,k]

function y = softmax(x)
	y = exp(x);
	y ./= sum(y, 2);
end

%!shared m,x
%!	m = 30; k = 5; x = randn(m,k);
%!assert(sum(softmax(x),2), ones(m,1), eps^.5);
%!assert(softmax(x+randn(m,1)), softmax(x), eps^.5);
%!assert(softmax([x(:,1) zeros(m,1)])(:,1), 1./(1+exp(-x(:,1))), eps^.5);

%!demo
%!	r = 3; k = 4;
%!	w = rand(2,k)*(r*2)-r;
%!	b = sumsq(w)/-2;
%!	[x1,x2] = meshgrid(linspace(-r,r)); x = [x1(:) x2(:)];
%!	y = softmax(x*w+b);
%!	for w = w plot3(w([1 1]),w([2 2]),0:1,'x-','linewidth',2); hold on; end
%!	for y = y mesh(x1,x2,reshape(y,size(x1))); end; hold off;
%!	axis([-r r -r r 0 1], 'equal');
