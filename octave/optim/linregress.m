%% w = linregress(x,y)
%% w = linregress(x,y, c)
%% Y = X*w
%%	linear regression
%%	size(x) == [m,n]
%%	size(y) == [m,k]
%%	size(w) == [n,k]
%%	size(X) == [M,n]
%%	size(Y) == [M,k]
%%	c > 0
%%
%%	Solve the optimization problem defined by
%%		min{w} 0.5*trace((x*w-y)'*(x*w-y)) + 0.5*c*trace(w'*w)

function [w, info] = linregress(x,y, c=0)
	if (nargout > 1)
		[m,n] = size(x); [m,k] = size(y);
		f = @(w) 0.5*c*sumsq(w(:)) + 0.5*sumsq((x*w-y)(:));
		w0 = zeros(n,k);
		[w, v, info] = fminunc(f, w0);
	else
		[m,n] = size(x);
		w = (x'*x + c*eye(n)) \ (x'*y);
	end
end

function [x, fval, info] = fminunc(fcn, x0)
	sz = size(x0);
	[x, fval, info] = builtin('fminunc', @(x)fcn(reshape(x,sz)), x0(:));
	x = reshape(x,sz);
end

%!xtest
%!	m = 50; n = 5; k = 4; c = 1;
%!	x = randn(m,n);
%!	y = randn(m,k);
%!	[w,~] = linregress(x,y); assert(linregress(x,y), w, 1e-3);
%!	[w,~] = linregress(x,y,c); assert(linregress(x,y,c), w, 1e-3);
%!test
%!	m = 50; n = 5; k = 4;
%!	x = randn(m,n);
%!	y = randn(m,k);
%!	assert(linregress(x,y), x\y, eps^.5);
%!	assert(linregress(x,y), ols(y,x), eps^.5);
%!test
%!	m = 50; n = 5;
%!	x = randn(m,1);
%!	y = randn(m,1);
%!	[l{1:n}] = ndgrid(logical(0:1));
%!	l = [cellfun(@vec,l,'UniformOutput',0){:}];
%!	for l = l'
%!		p = nonzeros(polyfit(x, y, l));
%!		w = linregress(x.^(n-1:-1:0)(l), y);
%!		assert(p, w, eps^.5);
%!	end

%!demo
%!	x = linspace(-3,3,50)'; p = randn(3,1);
%!	y = polyval(p,x) + randn(size(x));
%!	for c = [zeros(1,5) logspace(-3,3,61)]
%!		w = linregress(x.^(2:-1:0), y, c);
%!		plot(x,y,'o', x,polyval(p,x),'--', x,polyval(w,x));
%!		title(num2str(c,'c=%.2e')); drawnow;
%!	end
