%% w = logregress(x,y)
%% w = logregress(x,y, c)
%% [~,Y] = max(X*w,[],2)
%%	logistic regression
%%	size(x) == [m,n]
%%	size(y) == [m,1]
%%	size(w) == [n,k]
%%	size(X) == [M,n]
%%	size(Y) == [M,1]
%%	ismember(y, 1:k)
%%	c > 0
%%
%%	Solve the optimization problem defined by
%%		min{w} -sum(log(softmax(x*w)(y)) + 0.5*c*trace(w'*w)

function [w, info] = logregress(x,y, c=0)
	[m,n] = size(x); k = max(y);
	i = sub2ind([m,k], 1:m, y');
	f = @(w) 0.5*c*sumsq(w(:)) - sum(log(softmax(x*w)(i)));
	w0 = zeros(n,k);
	[w, v, info] = fminunc(f, w0);
end

function [x, fval, info] = fminunc(fcn, x0)
	sz = size(x0);
	[x, fval, info] = builtin('fminunc', @(x)fcn(reshape(x,sz)), x0(:));
	x = reshape(x,sz);
end

%!xtest
%!	if (compare_versions(version,'4.4','>=')) return end
%!	m = 50; n = 5;
%!	x = randn(m,n);
%!	y = randi(2,m,1);
%!	w = logregress(ones(m,1),y);
%!	t = logistic_regression(y);
%!	assert(diff(w), -t, 1e-3);
%!	w = logregress([x ones(m,1)],y);
%!	[t,b] = logistic_regression(y,x);
%!	assert(diff(w,[],2), [b;-t], 1e-3);
%!test
%!	m = 50; n = 5; k = 4;
%!	x = randn(m,n); c = randn(1,n,k);
%!	[~,y] = min(sumsq(x-c,2),[],3);
%!	x(:,end+1) = 1;
%!	w = logregress(x,y);
%!	assert(y, nthargout(2,@max,x*w,[],2));
%!test
%!	m = 50; n = 5; k = 4; c = 1;
%!	x = randn(m,n);
%!	y = randi(k,m,1);
%!	w = logregress(x,y, c);

%!demo
%!	x = randn(50,2); c = randn(1,2,3);
%!	[~,y] = min(sumsq(x-c,2),[],3);
%!	x(:,3) = 1;
%!	for c = [zeros(1,3) logspace(-3,3,37)]
%!		plot(x(y==1,1),x(y==1,2),'ro','linewidth',2); hold all;
%!		plot(x(y==2,1),x(y==2,2),'go','linewidth',2);
%!		plot(x(y==3,1),x(y==3,2),'bo','linewidth',2); l = axis;
%!		w = logregress(x,y, c);
%!		[x1,x2] = meshgrid(linspace(l(1),l(2)), linspace(l(3),l(4)));
%!		X = [x1(:) x2(:)]; X(:,3) = 1; Y = nthargout(2,@max,X*w,[],2);
%!		plot(X(Y==1,1),X(Y==1,2),'r+');
%!		plot(X(Y==2,1),X(Y==2,2),'g+');
%!		plot(X(Y==3,1),X(Y==3,2),'b+'); hold off; axis(l,'equal');
%!		title(num2str(c,'c=%.2e')); drawnow;
%!	end
%!demo
%!	x = randn(50,2); c = randn(1,2,3);
%!	[~,y] = min(sumsq(x-c,2),[],3);
%!	y(randperm(end,5)) = randi(3,5,1);
%!	x(:,3) = 1;
%!	for c = [zeros(1,3) logspace(-3,3,37)]
%!		plot(x(y==1,1),x(y==1,2),'ro','linewidth',2); hold all;
%!		plot(x(y==2,1),x(y==2,2),'go','linewidth',2);
%!		plot(x(y==3,1),x(y==3,2),'bo','linewidth',2); l = axis;
%!		w = logregress(x,y, c);
%!		[x1,x2] = meshgrid(linspace(l(1),l(2)), linspace(l(3),l(4)));
%!		X = [x1(:) x2(:)]; X(:,3) = 1; Y = nthargout(2,@max,X*w,[],2);
%!		plot(X(Y==1,1),X(Y==1,2),'r+');
%!		plot(X(Y==2,1),X(Y==2,2),'g+');
%!		plot(X(Y==3,1),X(Y==3,2),'b+'); hold off; axis(l,'equal');
%!		title(num2str(c,'c=%.2e')); drawnow;
%!	end
