%% ttimes(x, y)
%%	Return the tensor product and contraction with summation convention.

function z = ttimes(x,y)
	z = x .* y;
	for i = intersect(find(size(x)~=1), find(size(y)~=1))
		z = sum(z, i);
	end
end

%!test
%!	a = randn(3,1);
%!	b = randn(3,1);
%!	assert(cross(a,b), ttimes(epsilon(3),ttimes(vec(a,2),vec(b,3))), eps^.5);
%!test
%!	for n = 1:5
%!		a = randn(n,1);
%!		b = randn(n,1);
%!		assert(dot(a,b), ttimes(a,b), eps^.5);
%!	end
%!test
%!	for n = 1:5
%!	for p = [cellfun(@(x){x(:)},nthargout(1:3,@ndgrid,[1 n])){:}]';
%!		a = randn(p(1:2));
%!		b = randn(p(2:3));
%!		assert(a*b, permute(ttimes(a,shiftdim(b,-1)),[1 3 2]), eps^.5);
%!	end
%!	end

%!test
%!	for n = 1:5
%!		p = [cellfun(@(x){x(:)},nthargout(1:5,@ndgrid,[1 n])){:}];
%!	for i = 1:rows(p)
%!	for j = 1:i
%!		a = randn(p(i,:));
%!		b = randn(p(j,:));
%!		q = ifelse(p(i,:)==p(j,:),1,n);
%!		assert(size_equal(ttimes(a,b), zeros(q)));
%!	end
%!	end
%!	end
