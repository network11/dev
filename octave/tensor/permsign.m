%% permsign(p)
%%	Compute the signum of permutations with one row per permutation.

function s = permsign(p)
	s = cellfun(@(p) det(eye(numel(p))(:,p)), num2cell(p,2));
end

%!test
%!	for n = 1:5
%!		p = perms(1:n);
%!		s = permsign(p);
%!		assert(ismember(s, [-1 1]));
%!		a = randn(n);
%!		d = sum(prod([s a((0:n-1)*n+p)],2));	% Leibniz formula
%!		assert(d, det(a), eps^.5);
%!	end
