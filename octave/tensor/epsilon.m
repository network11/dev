%% epsilon(n)
%%	Return a Levi-Civita symbol.

function x = epsilon(n=1)
	p = perms(1:n);
	s = permsign(p);
	x = accumarray(p, s);
end

%!function p = grids(v)
%!	[p{1:numel(v)}] = ndgrid(v); p = [cellfun(@(x){x(:)},p){:}];
%!endfunction
%!test
%!	for n = 1:5
%!		x = epsilon(n);
%!		assert(ismember(x, -1:1));
%!		assert(ttimes(x,x), factorial(n));
%!		a = randn(n);
%!		p = grids(1:n);
%!		d = sum(prod([x(:) a((0:n-1)*n+p)],2));
%!		assert(d, det(a), eps^.5);
%!	end

%!test
%!	tol = eps^.5;
%!	e = randn(1,3); b = randn(1,3);
%!	ee = dot(e,e); bb = dot(b,b); eb = dot(e,b);
%!	e3 = epsilon(3); e4 = epsilon(4);
%!	g3 = eye(3); g4 = diag([-1 1 1 1]);
%!	fu = [0 e;-e' ttimes(e3,vec(b,3))];
%!	fd = g4*fu*g4;
%!	gd = ttimes(+e4,shiftdim(fu,-2));
%!	gu = ttimes(-e4,shiftdim(fd,-2));
%!	assert(gd, [0 b;-b' ttimes(e3,vec(e,3))]*2);
%!	assert(gd, g4*gu*g4);
%!	assert(fu, -fu');
%!	assert(fd, -fd');
%!	assert(gu, -gu');
%!	assert(gd, -gd');
%!	assert(ttimes(fu,fd), 2*(bb-ee), tol);
%!	assert(ttimes(gu,gd), 8*(ee-bb), tol);
%!	assert(ttimes(fu,gd), 8*eb, tol);
%!	assert(ttimes(fd,gu), 8*eb, tol);
%!	assert(det(fu*g4), -eb^2, tol);
%!	w = (ee+bb)/2; s = cross(e,b); t = w*g3-e'*e-b'*b;
%!	tu = squeeze(ttimes(ipermute(g4,[2 4 1 3]),ttimes(fu,shiftdim(fu,-2)))) - ttimes(fu,fd)/4*g4;
%!	td = g4*tu*g4;
%!	assert(tu, [w s;s' t], tol);
%!	assert(tu, tu');
%!	assert(td, td');
%!	assert(ttimes(tu,td), (bb-ee)^2+4*eb^2, tol);
