%% perminv(p)
%%	Compute the inverse of permutations with one row per permutation.

function i = perminv(p)
	[~,i] = sort(p,2);
end

%!function m = permmat(p)
%!	for k = 1:rows(p) m(:,:,k) = eye(columns(p))(:,p(k,:)); end
%!endfunction
%!test
%!	for n = 1:5
%!		p = perms(1:n);
%!		q = perminv(p);
%!		assert(permtimes(q,p) == 1:n);
%!		assert(permtimes(p,q) == 1:n);
%!		assert(permsign(q), permsign(p));
%!		assert(permmat(q), permute(permmat(p),[2 1 3]));
%!	end
