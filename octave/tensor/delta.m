%% delta(n, m)
%%	Return a generalized Kronecker delta.

function x = delta(n=1, m=n)
	p = perms(1:m);
	s = permsign(p);
	c = nchoosek(1:n,m);
	p = [repmat(p,rows(p),1) repelem(p,rows(p),1)];
	p = cell2mat(cellfun(@(c){c(p)},num2cell(c,2)));
	s = repmat((s*s')(:),rows(c),1);
	x = accumarray(p, s);
end

%!test
%!	for n = 1:5
%!		y = epsilon(n);
%!		a = randn(n);
%!		p = poly(a)(2:end);
%!		aa = xx = 1;
%!	for m = 1:n
%!		x = delta(n,m);
%!		assert(ismember(x, -1:1));
%!		if (m==1) assert(x, eye(n)); end
%!		if (m==n) assert(x, ttimes(y,shiftdim(y,-n))); end
%!		assert(squeeze(ttimes(x,shiftdim(y,-m))), y*factorial(m));
%!		x = permute(x, (1:m)+[0;m]);
%!		assert(squeeze(ttimes(eye(n),x)), xx*(n-m+1));
%!		aa = ttimes(a,shiftdim(aa,-2)); xx = x;
%!		assert(ttimes(x,aa), p(m)*(-1)^m*factorial(m), eps^.5);
%!	end
%!	end
