%% permtimes(p, q)
%%	Compute the product of permutations with one row per permutation.

function p = permtimes(p,q)
	p = cell2mat(cellfun(@(p,q){q(p)}, num2cell(p,2), num2cell(q,2)));
end

%!function m = permmat(p)
%!	for k = 1:rows(p) m(:,:,k) = eye(columns(p))(:,p(k,:)); end
%!endfunction
%!test
%!	for n = 1:5
%!		p = perms(1:n);
%!		a = p(randperm(end),:);
%!		b = p(randperm(end),:);
%!		c = permtimes(a,b);
%!		assert(permsign(c), permsign(b).*permsign(a));
%!		assert(permmat(c), blkmm(permmat(b),permmat(a)));
%!	end
