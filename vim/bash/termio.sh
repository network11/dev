#!/bin/sh

topen () {
	tput clear; tput civis; tbuf=""
	tsav="$(stty -g)"; stty -icanon -echo min 0 time 0
}
tclose () {
	tput ed; tput cnorm; tbuf=""
	stty "$tsav"
}
tflush () {
	printf %s "$(tput ed)$tbuf$(tput home)"; tbuf=""
}
twrite () {
	tbuf="$tbuf$(tput cup "$3" "$2")$1"
}
tread () {
	sleep "$1"; cat /dev/tty
}

if [ "$(basename "$0")" = termio.sh ]; then
	x=0
	y=0
	topen
	while :; do
		twrite "<" $((33+$x*3-3)) $((11+$y))
		twrite ">" $((33+$x*3+3)) $((11+$y))
		twrite "^" $((33+$x*3)) $((11+$y-1))
		twrite "v" $((33+$x*3)) $((11+$y+1))
		tflush
		case "$(tread .05)" in
		'')	break;;
		h)	x=$(($x-4));;
		j)	y=$(($y+4));;
		k)	y=$(($y-4));;
		l)	x=$(($x+4));;
		esac
		[ $x -gt 0 ] && x=$(($x-1))
		[ $x -lt 0 ] && x=$(($x+1))
		[ $y -gt 0 ] && y=$(($y-1))
		[ $y -lt 0 ] && y=$(($y+1))
	done
	tclose
fi
