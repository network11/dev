#!/bin/bash -e

exec 3<> "/dev/tcp/${1:-localhost}/${2:-http}"
printf "GET %s HTTP/1.0\r\n" "${3:-/}" >&3
#printf "Accept: text/html, text/plain\r\n" >&3
#printf "Accept-Language: en\r\n" >&3
#printf "User-Agent: GNU bash %s\r\n" "${BASH_VERSION}" >&3
printf "\r\n" >&3
cat <&3
