#!/bin/bash

topen () {
	tput clear; tput civis; tbuf=""
}
tclose () {
	tput ed; tput cnorm; tbuf=""
}
tflush () {
	printf %s "$(tput ed)$tbuf$(tput home)"; tbuf=""
}
twrite () {
	tbuf="$tbuf$(tput cup "$3" "$2")$1"
}
tread () {
	read -rs -N1 -t"$1"; printf %s "$REPLY"
}

if [ "$(basename "$0")" = termio.bash ]; then
	x=0
	y=0
	topen
	while :; do
		twrite "<" $((33+$x*3-3)) $((11+$y))
		twrite ">" $((33+$x*3+3)) $((11+$y))
		twrite "^" $((33+$x*3)) $((11+$y-1))
		twrite "v" $((33+$x*3)) $((11+$y+1))
		tflush
		case "$(tread .05)" in
		$'\e')	break;;
		h)	x=$(($x-4));;
		j)	y=$(($y+4));;
		k)	y=$(($y-4));;
		l)	x=$(($x+4));;
		esac
		[ $x -gt 0 ] && x=$(($x-1))
		[ $x -lt 0 ] && x=$(($x+1))
		[ $y -gt 0 ] && y=$(($y-1))
		[ $y -lt 0 ] && y=$(($y+1))
	done
	tclose
fi
